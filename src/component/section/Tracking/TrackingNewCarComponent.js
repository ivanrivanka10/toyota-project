import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  FlatList,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import TrackingComponent from '../Riwayat/RiwayatComponent';
import moment from 'moment';

const NewCarComponent = ({navigation, dataNewCar}) => {
  const [detail, setDetail] = useState('Request Diskon Sudah Terupdate');
  const functionStatus = (status, statusLabel) => {
    switch (status) {
      case 'Approved':
        return `Request ${statusLabel} Sudah Terupdate`;
      case 'Rejected':
        return `Request ${statusLabel} Sudah Terupdate`;
      case 'Cancel':
        return `Request ${statusLabel} Sudah Terupdate`;
      case 'Dalam Proses':
        return `Sedang Dalam Request`;
      default:
        return '-';
    }
  };
  const functionColor = (status, statusLabel, tabStatus) => {
    if (status === 'Dalam Proses') {
      return Colors.DEEPBLUE;
    }
    if (tabStatus === 'New Car') {
      return Colors.ORANGE;
    }

    switch (statusLabel) {
      case 'Diskon':
        return Colors.PRIMARY;
      case 'Subsidi':
        return Colors.GREEN;
      case 'MRP':
        return Colors.YELLOW;

      default:
        return Colors.PRIMARY;
    }
  };
  const RenderItem = ({item}) => (
    <TouchableOpacity 
    onPress={() => navigation.navigate('TrackingBeliMobil', item={item})}
    >
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '100%',
          backgroundColor: Colors.WHITE,
          borderWidth: 0.6,
          borderColor: Colors.BORDERGREY,
          borderRadius: 8,
          paddingHorizontal: 12,
          paddingTop: 10,
          paddingBottom: 8,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            // text={'TR-092018-246'}
            text={item.ApprovalNewCar.noNewCar}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextRegular
            text={moment(item.updatedAt).format('YYYY-MM-DD HH:mm')}
            // text={moment(item.createdAt).format('')
            size={12}
            color={Colors.DEEPBLUE}
          />
        </View>

        <TextBold
          text={item.ApprovalNewCar.NewCar.carName}
          size={14}
          color={Colors.DEEPBLUE}
          style={{marginTop: 8}}
        />

        <View
          style={{
            borderStyle: 'dashed',
            width: '100%',
            borderWidth: 0.6,
            borderColor: Colors.BORDERGREY,
            marginTop: 12,
          }}></View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 8,
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <View>
            <TextBold
              // text={'Handoko'}
              text={item.ApprovalNewCar.SalesBranch.branch}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextRegular text={'Salesman'} size={10} color={Colors.BLACK} />
          </View>
          <View
            style={{
              backgroundColor: functionColor(
                item.status,
                item.status_label,
                item.status_type,
              ),
              paddingHorizontal: 6,
              paddingVertical: 4,
              borderRadius: 4,
            }}>
            <TextRegular
              text={functionStatus(item.status, item.status_label)}
              size={10}
              color={Colors.WHITE}
            />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
  return (
    <View style={{paddingBottom: 60, backgroundColor: Colors.GREY}}>
      {dataNewCar.length < 1 ? (
        <TextRegular
          text={'No Data'}
          color={Colors.BORDERGREY}
          size={18}
          style={{alignSelf: 'center', marginTop: 20}}
        />
      ) : null}
      <FlatList
        data={dataNewCar}
        renderItem={({item}) => <RenderItem item={item} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({});

export default NewCarComponent;
