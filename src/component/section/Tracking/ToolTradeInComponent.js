import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium, InputText} from '../../global';
import ModalAlasanPenolakan from '../../modal/Tracking/TradeIn/ModalAlasanPenolakan';
import ModalKonfirmasiDeal from '../../modal/Tracking/TradeIn/ModalKonfirmasiDeal';
import ModalRequestDiscountBOD from '../../modal/Tracking/TradeIn/ModalRequestDiscountBOD';
import ModalChangeDiscount from '../../modal/Tracking/TradeIn/ModalChangeDiscount';
import ModalRequestMRP from '../../modal/Tracking/TradeIn/ModalRequestMRP';
import {Header} from '../../global';
import moment from 'moment';
import {convertCurrency} from '../../../utils/helpers/ConvertCurrentcy';
import ModalChangeCar from '../../modal/Home/ModalChangeCar';
//
const ToolTradeInComponent = ({
  navigation,
  routing,
  item,
  onChange,
  setIdNewCar,
  listCar,
}) => {
  const data = item;

  const [ModalChangeCarVisible, setModalChangeCarVisible] = useState(false);
  const [modalBottomNoDeal, setModalBottomNodeal] = useState(false);
  const [modalBottomKonfirmasi, setModalBottomKonfirmasi] = useState(false);
  const [modalBottomBOD, setModalBOD] = useState(false);
  const [modalChageDiscount, setModalChangeDiscount] = useState(false);
  const [modalChangeCar, setModalChangeCar] = useState(false);
  const [modalRequestMRP, setModalRequestMRP] = useState(false);
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );
  const dataPenawaran = data.calculation[1].price;
  const dataNego = data.calculation[1].price;
  const dataPenawaran1 = dataPenawaran !== 'N/A' ? parseFloat(dataPenawaran) : 0;
  const dataNego1 = dataNego !== 'N/A' ? parseFloat(dataNego) : 0;
  const MRP = convertCurrency(dataPenawaran1 + dataNego1);
  return (
    // All
    <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
      {/* Header Dimulai Di sini  */}
      <Header
        title="Tool Trade In"
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />
      {/* Kode TR & Hari, Tanggal */}
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          backgroundColor: 'white',
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
          justifyContent: 'space-between',
        }}>
        <TextRegular
          // text={'TR-092018-246'}
          text={data.approvalTradeIn.Appraisal.Booking.noBooking}
          size={12}
        />

        <TextRegular
          // text={'coba'}
          text={moment(data.approvalTradeIn.updatedAt).format(
            'YYYY-MM-DD HH:mm',
          )}
          size={12}
          color={Colors.DARKBLUETOYOTA}
        />
      </View>
      {/* Kode TR & Hari, Tanggal Selesai di Sini*/}
      {/* Used Car Information Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Used Car Information'}
          style={{marginVertical: 5, fontSize: 14}}
          color={Colors.DARKBLUETOYOTA}
        />

        <Separator />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Mobil'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
          <TextBold
            // text={'TR-092018-246'}
            text={
              item?.approvalTradeIn?.NewCar?.carName
                ? item?.approvalTradeIn?.NewCar?.carName
                : 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Plat Nomor'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            // text={'B 1234 TES'}
            text={
              item?.approvalTradeIn?.Appraisal?.carDetail[7]?.value ?? 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>

        <TouchableOpacity
          style={{
            marginTop: 20,
            width: '100%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: '#287AE5',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
          onPress={() =>
            navigation.navigate('DetailAppraisalTracking', (item = {item}))
          }>
          <TextRegular
            text={'Lihat Detail Appraisal'}
            size={14}
            color={Colors.WHITE}
            style={{
              marginHorizontal: 10,
            }}
          />

          <Image
            style={{
              width: 20,
              height: 20,
              tintColor: 'white',
              marginHorizontal: 10,
            }}
            source={require('../../../asset/icon-next.png')}
          />
        </TouchableOpacity>
      </View>
      {/* Used Car Information Selesai di Sini  */}

      {/* New Car Information Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'New Car Information'}
          style={{marginVertical: 5, fontSize: 14}}
          color={Colors.DARKBLUETOYOTA}
        />

        <Separator />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Mobil'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <View>
            <TextBold
              // text={'TR-092018-246'}
              text={item?.approvalTradeIn?.NewCar?.carName ?? 'N/A'}
              size={14}
              color={Colors.DARKBLUETOYOTA}
              style={{
                marginVertical: 5,
              }}
            />

            <TouchableOpacity
              style={{
                alignSelf: 'flex-end',
              }}
              onPress={() => setModalChangeCarVisible(true)}>
              <TextRegular
                text={'Change Car'}
                size={14}
                color={Colors.PRIMARY}
                style={{
                  fontStyle: 'italic',
                }}
              />
            </TouchableOpacity>
            <Modal
              animationType="fade"
              transparent={true}
              visible={ModalChangeCarVisible}
              onRequestClose={() => {
                setModalChangeCarVisible(false);
              }}>
              <ModalChangeCar
                onRequestClose={() => {
                  setModalChangeCarVisible(false);
                }}
                listCar={listCar}
                onChange={onChange}
                setIdNewCar={setIdNewCar}
              />
            </Modal>
          </View>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'OTR'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={
              convertCurrency(item?.approvalTradeIn?.NewCar?.otr)
                ? 'Rp ' + convertCurrency(item?.approvalTradeIn?.NewCar?.otr)
                : 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>
      </View>
      {/* New Car Information Selesai di Sini  */}

      {/* Ruang Nego Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Ruang Nego'}
          style={{marginVertical: 5, fontSize: 14}}
          color={Colors.DARKBLUETOYOTA}
        />

        <Separator />

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Harga Penawaran'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={
              convertCurrency(data?.calculation[1]?.price)
                ? 'Rp. ' + convertCurrency(data?.calculation[1]?.price)
                : 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Ruang Nego Harga'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
             text={
              convertCurrency(data?.calculation[6]?.price)
                ? 'Rp. ' + convertCurrency(data?.calculation[6]?.price)
                : 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Harga Beli MAX'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={'Rp. ' +MRP}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>
      </View>
      {/* Ruang Nego Selesai di Sini  */}

      {/* Diskon Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Diskon New Car'}
          style={{marginVertical: 5, fontSize: 14}}
          color={Colors.DARKBLUETOYOTA}
        />

        <Separator />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Diskon Kacab'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
          <View>
            <TextBold
               text={
                'Rp. ' + convertCurrency(data?.calculation[6]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DARKBLUETOYOTA}
              style={{
                marginVertical: 5,
                alignSelf: 'flex-end',
              }}
            />
            <TouchableOpacity
              style={{
                alignSelf: 'flex-end',
              }}
              onPress={() => setModalChangeDiscount(true)}>
              <TextRegular
                text={'Change Discount'}
                size={14}
                color={Colors.PRIMARY}
                style={{
                  fontStyle: 'italic',
                }}
              />
            </TouchableOpacity>
            <ModalChangeDiscount
              show={modalChageDiscount}
              onClose={() => setModalChangeDiscount(false)}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Pengajuan Diskon BOD '}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={
              'Rp. ' + convertCurrency(data?.calculation[7]?.price) ?? 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
              alignSelf: 'flex-end',
            }}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'accessories1'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={'Rp. 1000.000'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
              alignSelf: 'flex-end',
            }}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'GP AUTO2000 After Disc'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
             text={
              'Rp. ' + convertCurrency(data?.calculation[9]?.price) ?? 'N/A'
            }
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
              alignSelf: 'flex-end',
            }}
          />
        </View>
      </View>
      {/* Diskon Selesai di Sini  */}

      {/* Gross Profit Trust Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Gross Profit Trust'}
          style={{marginVertical: 5, fontSize: 14}}
          color={Colors.DARKBLUETOYOTA}
        />

        <Separator />

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Proyeksi Harga Jual'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={convertCurrency('Rp. '+ data?.calculation[2]?.price ?? 'N/A' )}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'COGS (harga beli + biaya rekondisi) + OPEX'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
              width: 200,
            }}
          />

          <TextBold
            text={convertCurrency(data?.calculation[3]?.price, 'Rp. ')}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Proyeksi NPBT Trust'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
            text={ 'Rp. '+convertCurrency(data?.calculation[4]?.price ?? 'N/A')}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Total Benefit TI'}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />

          <TextBold
             text={ 'Rp. '+convertCurrency(data?.calculation[8]?.price ?? 'N/A')}
            size={14}
            color={Colors.DARKBLUETOYOTA}
            style={{
              marginVertical: 5,
            }}
          />
        </View>
      </View>
      {/* Gross Profit Trust Selesai di Sini  */}

      {/* Request Section Dimulai dari Sini  */}
      <TextBold
        text={'Request Section'}
        style={{
          textAlign: 'center',
          fontWeight: 'bold',
          fontSize: 16,
          marginVertical: 20,
        }}
      />

      {/* Separator Tengah Dimulai Di Sini*/}
      <View
        style={{
          width: '90%',
          height: 1,
          backgroundColor: '#cecece',
          marginVertical: 5,
          alignSelf: 'center',
        }}
      />
      {/* Separator Tengah Selesai di Sini */}
      {/* Request Section Selesai dari Sini  */}

      {/* Request Discount BOD Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Request Diskon OM'}
          style={{marginVertical: 5, fontSize: 16}}
          color={Colors.DARKBLUETOYOTA}
        />

        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: Colors.GREY,
              marginTop: 20,
              width: '100%',
              paddingVertical: 5,
              borderRadius: 6,
              backgroundColor: '#cecece',
              alignItems: 'center',
              paddingHorizontal: 10,
            }}>
            <TextRegular text={'Sedang dalam Request'} />
            {/* <Image
            source={{uri: 'https://w7.pngwing.com/pngs/922/489/png-transparent-whatsapp-icon-logo-whatsapp-logo-whatsapp-logo-text-trademark-grass-thumbnail.png'}}
            style={{
              width:50,
              height:50,
            }}
            /> */}
          </View>
        </View>
      </View>
      {/* Request Discount BOD Selesai di sini*/}

      {/* Request MRP Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: '#cecece',
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Request MRP'}
          style={{marginVertical: 5, fontSize: 16}}
          color={Colors.DEEPBLUE}
        />

        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => setModalRequestMRP(true)}
            style={{
              marginTop: 20,
              width: '100%',
              paddingVertical: 10,
              borderRadius: 6,
              backgroundColor: Colors.PRIMARY,
              alignItems: 'center',
            }}>
            <TextRegular text={'REQUEST NOW'} color={Colors.WHITE} />
          </TouchableOpacity>
          <ModalRequestMRP
            show={modalRequestMRP}
            onClose={() => setModalRequestMRP(false)}
          />
        </View>
      </View>
      {/* Request Discount MRP Selesai di sini*/}

      {/* Harga Final Dimulai Di Sini */}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <TextRegular text={'Harga Final'} size={12} color={Colors.DEEPBLUE} />
        <View>
          <InputText
            style={{
              width: '100%',
              height: 32,
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 4,
              paddingVertical: 6,
              paddingHorizontal: 12,
              marginTop: 4,
            }}
            placeholderText={'raa'}
            // value={item?.item?.profile?.noKTP ?? '-'}
            onChangeText={text => setNotes(text)}
            multiline>
           
          </InputText>
        </View>
        <View style={{marginTop: 12}}>
          <TextRegular
            text={'Total Benefit TI (Final) : '}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <View>
          <InputText
            placeholderText="1231321313"
            value={'masukan harga final'}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          </View>
        </View>

        
        
      </View>
      {/* Harga Final Selesai Di Sini */}

      {/* No Deal dan Deal */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '90%',
          alignSelf: 'center',
        }}>
        <TouchableOpacity
          onPress={() => setModalBottomNodeal(true)}
          style={{
            marginVertical: 10,
            paddingVertical: 10,
            width: '42%',
            backgroundColor: Colors.BORDERGREY,
            alignItems: 'center',
            borderRadius: 10,
            marginRight: 10,
          }}>
          <TextRegular text={'NO DEAL'} color={Colors.BLACK} />
        </TouchableOpacity>
        <ModalAlasanPenolakan
          show={modalBottomNoDeal}
          onClose={() => setModalBottomNodeal(false)}
          title="Alasan Penolakan"
        />

        <TouchableOpacity
          onPress={() => setModalBottomKonfirmasi(true)}
          style={{
            marginVertical: 10,
            paddingVertical: 10,
            width: '48%',
            backgroundColor: '#287AE5',
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <TextRegular text={'DEAL'} color={Colors.WHITE} />
        </TouchableOpacity>
        <ModalKonfirmasiDeal
          show={modalBottomKonfirmasi}
          onClose={() => setModalBottomKonfirmasi(false)}
          title="Konfrimasi Data"
        />
      </View>

      {/* Isian View, biar scrollView/flatList bisa kelihatan semuanya */}
      <View style={{width: '100%', height: 50}} />
    </ScrollView>
  );
};

export default ToolTradeInComponent;
