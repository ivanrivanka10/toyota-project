import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import ModalAlasanPenolakan from '../../modal/Tracking/NewCar/ModalAlasanPenolakan';
import ModalHargaFinal from '../../modal/Tracking/NewCar/ModalHargaFinal';
import ModalRequestDiscount from '../../modal/Tracking/NewCar/ModalRequestDiscount';
import {Header} from '../../global';
import moment from 'moment';
import {convertCurrency} from '../../../utils/helpers/ConvertCurrentcy';
import Icon from 'react-native-vector-icons/FontAwesome';
import { BLACK } from '../../../styles/Colors';

const BeliMobilComponent = ({navigation, item}) => {
  console.log(item);
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );
  const [modalBottom, setModalBottom] = useState(false);
  const [modalBottomFinal, setModalBottomFinal] = useState(false);
  const [modalRequestDiscount, setModalRequestDicount] = useState(false);
  const DiskonDireksi = item.approvalRequestStatus[0].requestPriceEdited;
  const accessories1 = item.approvalRequestStatus[0].productBonuses[0].price;
  const accessories2 = item.approvalRequestStatus[0].productBonuses[0].price;
  const acc1 = accessories1 !== 'N/A' ? parseFloat(accessories1) : 0;
  const acc2 = accessories2 !== 'N/A' ? parseFloat(accessories2) : 0;
  const DiskonDireksi1 =
    DiskonDireksi !== 'N/A' ? parseFloat(DiskonDireksi) : 0;
  const Jumlah = convertCurrency(acc1 + acc2 + DiskonDireksi);
  return (
    // All

    <View style={{flex: 1}}>
      <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
        {/* Header */}
        <Header
          title="Beli Mobil"
          titleCenter={true}
          onPressBack={() => navigation.pop()}
          titleSize={16}
          isBoldTitle={true}
        />
        {/* Header Selesai Di Sini  */}

        {/* Kode TR & Hari, Tanggal */}
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            backgroundColor: Colors.WHITE,
            marginTop: 5,
            padding: 10,
            alignContent: 'center',
            justifyContent: 'space-between',
          }}>
          <TextRegular
            // text={'TR-092018-246'}
            text={item.approvalNewCar.noNewCar}
          />

          <TextRegular
            // text={'Sen, 17 Sep 2018 - 10:30'}
            color={Colors.DEEPBLUE}
            text={moment(item.updatedAt).format('YYYY-MM-DD HH:mm')}
          />
        </View>
        {/* Kode TR & Hari, Tanggal Selesai di Sini*/}

        {/* Nama Customer di dari sini */}
        <View
          style={{
            marginTop: 1,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: 'white',
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Nama Customer'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={item.approvalNewCar.customerName}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'No. HP'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={item.approvalNewCar.phone}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
        </View>
        {/* Nama Customer Selesai di Sini  */}

        {/* Detail Mobil di dari sini */}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: Colors.WHITE,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <TextBold
            text={'Detail Mobil'}
            color={Colors.BLACK}
            style={{
              marginVertical: 5,
            }}
          />

          <Separator />

          <View style={{marginVertical: 10}}>
            <TextRegular
              text={'New Car'}
              style={{
                marginVertical: 10,
              }}
            />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextBold
                // text={'Rush S AT TRD'}
                text={item.approvalNewCar.NewCar.carName}
                color={Colors.DEEPBLUE}
                style={{
                  marginVertical: 5,
                }}
              />

              <TextBold
                text={convertCurrency(item.approvalNewCar.NewCar.otr, 'Rp. ')}
                color={Colors.DEEPBLUE}
                style={{
                  marginVertical: 5,
                }}
              />
            </View>
          </View>
        </View>
        {/*Detail Mobil Selesai di Sini  */}

        {/* Request Discount di dari sini */}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: Colors.WHITE,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <TextBold
            text={'Diskon'}
            color={Colors.DEEPBLUE}
            style={{
              marginVertical: 10,
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Harga Mobil'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(item.approvalNewCar.NewCar.otr, 'Rp. ')}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Request Diskon'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(
                item.approvalRequestStatus[0].requestPrice,
                'Rp. ',
              )}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Keputusan Direksi  '}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(
                item.approvalRequestStatus[0].requestPriceEdited,
                'Rp. ',
              )}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextBold
              text={'Bonus Produk'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Accessories3'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(
                item.approvalRequestStatus[0].productBonuses[0].price,
                'Rp. ',
              )}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Accessories4'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(
                item.approvalRequestStatus[0].productBonuses[1].price,
                'Rp. ',
              )}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                width: '96%',
                height: 1,
                backgroundColor: Colors.BLACK,
                marginVertical: 5,
              }}></View>
            <Icon name="plus" size={12} color={Colors.BLACK} />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Total Discount'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={Jumlah}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
        </View>
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: Colors.WHITE,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <TextRegular text={'Approve By OM'} color={Colors.GREEN} />
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              
              // justifyContent: 'space-between',
            }}>
            <TextRegular text={moment(item?.approvalRequestStatus?.updatedAt).format('dddd, DD MMMM YYYY [-] HH.mm')} size={12} style={{marginRight:10}} />
            <View style={{width:1, marginVertical:2, backgroundColor:Colors.BLACK,}}/>
            <TextRegular text={item?.approvalRequestStatus[0]?.Director?.name}  size={12}  style={{marginLeft:10}}/>
          </View>
          <View style={{
                width: '96%',
                height: 1,
                backgroundColor: Colors.BORDERGREY,
                marginVertical: 5,
              }}>

          </View>
          <TextRegular
          size={12}
          text={'Notes : '} style={{marginTop:10, marginBottom:20}}/>
        </View>
        {/*Request Discount Selesai di Sini  */}
      </ScrollView>

      {/* No Deal dan Deal */}
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: Colors.WHITE,
          width: '100%',
          justifyContent: 'space-between',
          bottom: 0,
          alignSelf: 'center',
          paddingHorizontal: 15,
        }}>
        <TouchableOpacity
          onPress={() => setModalBottomFinal(true)}
          style={{
            marginVertical: 10,
            paddingVertical: 10,
            width: '42%',
            backgroundColor: Colors.BORDERGREY,
            alignItems: 'center',
            borderRadius: 10,
            marginRight: 10,
          }}>
          <TextRegular text={'NO DEAL'} color={Colors.BLACK} />
        </TouchableOpacity>
        <ModalAlasanPenolakan
          show={modalBottomFinal}
          onClose={() => setModalBottomFinal(false)}
          title="Alasan Penolakan"
        />

        <TouchableOpacity
          onPress={() => setModalBottom(true)}
          style={{
            marginVertical: 10,
            paddingVertical: 10,
            width: '42%',
            backgroundColor: Colors.PRIMARY,
            alignItems: 'center',
            borderRadius: 10,
            marginRight: 10,
          }}>
          <TextBold text={'DEAL'} color={Colors.WHITE} />
        </TouchableOpacity>
        <ModalHargaFinal
          show={modalBottom}
          onClose={() => setModalBottom(false)}
          title="Harga Final"
        />
      </View>
      {/* No Deal dan Deal Selesai di sini*/}
    </View>
  );
};

export default BeliMobilComponent;
