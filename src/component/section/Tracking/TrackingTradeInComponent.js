import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  FlatList,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import TrackingComponent from '../Riwayat/RiwayatComponent';
import moment from 'moment';
const TradeInComponent = ({navigation, dataTradein}) => {
  const [detail, setDetail] = useState('Sedang Dalam Request');

  const functionStatus = (status, statusLabel) => {
    switch (status) {
      case 'Approved':
        return `Request ${statusLabel} Sudah Terupdate`;
      case 'Rejected':
        return `Request ${statusLabel} Sudah Terupdate`;
      case 'Cancel':
        return `Request ${statusLabel} Sudah Terupdate`;
      case 'Dalam Proses':
        return `Sedang Dalam Request`;

      default:
        return '-';
    }
  };
  const functionColor = (status, statusLabel) => {
    if (status === 'Dalam Proses') {
      return Colors.DEEPBLUE;
    }

    switch (statusLabel) {
      case 'Diskon':
        return Colors.PRIMARY;
      case 'Subsidi':
        return Colors.GREEN;
      case 'MRP':
        return Colors.YELLOW;

      default:
        return Colors.PRIMARY;
    }
  };

  const RenderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('TrackingToolTradeIn', item={item})}>
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '100%',
          backgroundColor: Colors.WHITE,
          borderWidth: 0.6,
          borderColor: Colors.BORDERGREY,
          borderRadius: 8,
          paddingHorizontal: 12,
          paddingTop: 10,
          paddingBottom: 8,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            // text={'TR-092018-246'}
            text={item.ApprovalTradeIn.Appraisal.Booking.noBooking}
            size={12}
          />
          <TextRegular
            // text={'Sen, 17 Sep 2018 - 10:30'}
            text={moment(item.ApprovalTradeIn.updatedAt).format('YYYY-MM-DD HH:mm')}
            size={12}
            color={Colors.DEEPBLUE}
          />
        </View>

        <View style={{flexDirection: 'row'}}>
          {/* AVANZA VELOZ A/T 2020 */}
          {[3, 5, 6].map(index => (
            <React.Fragment key={index}>
              <TextBold
                text={item.ApprovalTradeIn.Appraisal.carDetail[index].value}
                color={Colors.DARKBLUETOYOTA}
                style={{marginVertical: 5}}
              />
              <TextBold text={' '} />
            </React.Fragment>
          ))}
        </View>

        <View
          style={{
            borderStyle: 'dashed',
            width: '100%',
            borderWidth: 0.6,
            borderColor: Colors.BORDERGREY,
            marginTop: 12,
          }}></View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 8,
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <View>
            <TextBold
              text={item.ApprovalTradeIn.Appraisal.Booking.SalesProfile.name}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextRegular text={'Salesman'} size={10} color={Colors.BLACK} />
          </View>
          <View
            style={{
              backgroundColor: functionColor(item.status, item.status_label),
              paddingHorizontal: 6,
              paddingVertical: 4,
              borderRadius: 4,
            }}>
            <TextRegular
              text={functionStatus(item.status, item.status_label)}
              size={10}
              color={Colors.WHITE}
            />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={{paddingBottom: 60}}>
      {dataTradein.length < 1 ? (
        <TextRegular
          text={'No Data'}
          color={Colors.BORDERGREY}
          size={18}
          style={{alignSelf: 'center', marginTop: 20}}
        />
      ) : null}
      <FlatList
        data={dataTradein}
        renderItem={({item}) => <RenderItem item={item} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({});

export default TradeInComponent;
