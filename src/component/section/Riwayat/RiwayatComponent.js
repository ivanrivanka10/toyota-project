import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import TradeInComponent from './RiwayatTradeInComponent';
import NewCarComponent from './RiwayatNewCarComponent';
import ModalFilter from '../../modal/Riwayat/ModalFilter';
import RiwayatTradeInComponent from './RiwayatTradeInComponent';
import RiwayatNewCarComponent from './RiwayatNewCarComponent';
const RiwayatComponent = ({
  navigation,
  dataTradein,
  dataNewCar,
  onSearch,
  keyword,
  filter,
  setFilter,
  onSaveFilter,
}) => {
  const [modalBottom, setModalBottom] = useState(false);

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );
  const [isOpenTradeIn, setisOpenTradeIn] = useState(true);
  const inverseTradeIn = () => setisOpenTradeIn(true);
  const inverseNewCar = () => setisOpenTradeIn(false);

  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      <View style={{flex: 1, backgroundColor: Colors.GREY}}>
        <View
          style={{
            width: '100%',
            backgroundColor: Colors.PRIMARY,
            paddingHorizontal: 16,
            paddingTop: 16,
            paddingBottom: 30,
          }}>
          <TextBold text={'Riwayat'} color={Colors.WHITE} size={20} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 16,
              marginBottom: 14,
            }}>
            <View
              style={{
                width: '85%',
                padding: 14,
                backgroundColor: Colors.WHITE,
                borderRadius: 8,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={{width: '80%'}}>
                <TextInput
                  style={{height: 16, padding: 0}}
                  onChangeText={text => onSearch(text)}
                  placeholder="Telusuri..."
                  value={keyword}
                />
              </View>
              <Icon name="search1" size={16} color={Colors.DEEPBLUE} />
            </View>

            <TouchableOpacity
              style={{
                padding: 14,
                backgroundColor: Colors.WHITE,
                borderRadius: 8,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => setModalBottom(true)}>
              <Image
                source={require('../../../asset/icon-filter.png')}
                style={{
                  height: 18,
                  width: 18,
                }}
              />
            </TouchableOpacity>
            <ModalFilter
              show={modalBottom}
              onClose={() => setModalBottom(false)}
              filter={filter}
              setFilter={setFilter}
              onSaveFilter={onSaveFilter}></ModalFilter>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            width: '100%',
            paddingHorizontal: 16,
            marginTop: -20,
            borderRadius: 8,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              backgroundColor: Colors.WHITE,
              borderRadius: 8,
              borderWidth: 0.5,
              borderColor: Colors.BORDERGREY,
            }}>
            {/* Trade In  */}
            <TouchableOpacity onPress={inverseTradeIn} style={styles.button}>
              {isOpenTradeIn ? (
                <TextBold text={'Trade In'} size={16} color={Colors.PRIMARY} />
              ) : (
                <TextRegular
                  text={'Trade In'}
                  size={14}
                  color={Colors.DEEPBLUE}
                />
              )}
            </TouchableOpacity>
            <View
              style={{
                width: 1,
                height: 40,
                backgroundColor: Colors.BORDERGREY,
              }}></View>

            {/* New Car  */}
            <TouchableOpacity onPress={inverseNewCar} style={[styles.button]}>
              {isOpenTradeIn ? (
                <TextRegular
                  text={'New Car'}
                  size={14}
                  color={Colors.DEEPBLUE}
                />
              ) : (
                <TextBold text={'New Car'} size={16} color={Colors.PRIMARY} />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View style={{paddingHorizontal: 16}}>
          {isOpenTradeIn ? (
            <RiwayatTradeInComponent
              navigation={navigation}
              dataTradein={dataTradein}
            />
          ) : (
            <RiwayatNewCarComponent
              navigation={navigation}
              dataNewCar={dataNewCar}
            />
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  button: {
    height: 40,
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default RiwayatComponent;
