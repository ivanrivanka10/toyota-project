import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
} from 'react-native';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {Header} from '../../../global';

const TotalHargadanDetailCustomer = ({navigation, routing, item}) => {
    const datas = item;
    const Separator = () => (
        <View
          style={{
            width: '100%',
            height: 1,
            backgroundColor: '#d3e1ce',
            marginVertical: 5,
          }}
        />
      );
    return(
        <View>
           
          <View
            style={{
              marginTop: 10,
              alignSelf: 'center',
              width: '90%',
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 8,
              padding: 10,
              marginBottom: 19,
              flexDirection: 'column',
            }}>
            <TextBold
              text={'Data Customer'}
              size={16}
              style={{alignSelf: 'center'}}
              color={Colors.DEEPBLUE}
            />
            <Separator />
            <View style={styles.dataDetailCustomer}>
              <TextRegular
                text={'Pemilik Kendaraan'}
                size={12}
                color={Colors.PRIMARY}
              />
              <TextBold
                // text={datas.approvalTradeIn.Appraisal.carDetail[0].value}
                color={Colors.DEEPBLUE}
                size={12}
                style={{marginLeft: 12}}
              />
            </View>
            <Separator />
            <View style={styles.dataDetailCustomer}>
              <TextRegular text={'No hp'} size={12} color={Colors.PRIMARY} />

              <TextBold
                // text={datas.approvalTradeIn.Appraisal.carDetail[1].value}
                color={Colors.DEEPBLUE}
                size={12}
                style={{marginLeft: 12}}
              />
            </View>
            <Separator />
            <View style={styles.dataDetailCustomer}>
              <TextRegular
                text={'Email Customer'}
                size={12}
                color={Colors.PRIMARY}
              />

              <TextBold
                // text={datas.approvalTradeIn.Appraisal.carDetail[2].value}
                color={Colors.DEEPBLUE}
                size={12}
                style={{marginLeft: 12}}
              />
            </View>
          </View>
        </View>
    );
        

};

const styles = StyleSheet.create({
    cardprofile: {
      backgroundColor: Colors.WHITE,
      padding: 12,
      width: '100%',
      marginTop: 12,
      borderRadius: 5,
    },
    inputText: {
      marginTop: 20,
      width: '100%',
      alignSelf: 'center',
    },
    btnLogin: {
      width: '100%',
      alignSelf: 'center',
      height: 48,
      borderRadius: 6,
      paddingHorizontal: 16,
      backgroundColor: Colors.PRIMARY,
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
    },
    detail: {
      flexDirection: 'row',
      marginHorizontal: 12,
      justifyContent: 'space-between',
      marginBottom: 8,
    },
    dataDetailCustomer: {
      flexDirection: 'row',
      marginHorizontal: 12,
      justifyContent: 'space-between',
      marginTop: 12,
      marginBottom: 12,
    },
    detailpenguranganharga: {
      marginHorizontal: 12,
      alignSelf: 'flex-end',
      marginBottom: 8,
    },
    detailAndIcon: {
      flexDirection: 'row',
      marginLeft: 12,
      marginVertical: 12,
    },
  });
  
  export default TotalHargadanDetailCustomer;
  

