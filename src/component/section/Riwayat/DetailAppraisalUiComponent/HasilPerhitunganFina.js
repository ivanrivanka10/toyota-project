import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
} from 'react-native';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {Header} from '../../../global';

const HasilPerhitunganFinal = ({navigation, routing, item}) => {
  const datas = item;
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );

  return (
    <View>
      {/* Hasil Perhitungan Final start */}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 10,
          flexDirection: 'column',
        }}>
        <TextBold
          text={'Hasil Perhitungan Final'}
          size={16}
          style={{alignSelf: 'center'}}
          color={Colors.DEEPBLUE}
        />
        <Separator />
        <View style={styles.detailAndIcon}>
          <Image
            source={require('../../../../asset/icon-overview.png')}
            style={{width: 25, height: 25}}
          />
          <TextBold
            text={'Overview'}
            color={Colors.DEEPBLUE}
            size={16}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'No Polisi'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            // text={'B 1234 XYZ'}
            // text={datas.approvalTradeIn.Appraisal.carDetail[7].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'No Mesin'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            // text={datas.approvalTradeIn.Appraisal.carDetail[8].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'No Rangka'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            // text={'7149GHG68'}
            // text={datas.approvalTradeIn.Appraisal.carDetail[9].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Warna Eksterior'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            // text={datas.approvalTradeIn.Appraisal.carDetail[10].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Jarak tempuh'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            // text={datas.approvalTradeIn.Appraisal.carDetail[11].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'Tahun STNK'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'2017 (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>

        <Separator />
        <View style={styles.detailAndIcon}>
          <Image
            source={require('../../../../asset/icon-kendaraan.png')}
            style={{width: 25, height: 25}}
          />
          <TextBold
            text={'Kendaran'}
            color={Colors.DEEPBLUE}
            size={16}
            style={{marginLeft: 10}}
          />
        </View>

        <View
          style={{
            height: 40,
            flexDirection: 'row',
            marginLeft: 24,
            marginRight: 12,
            backgroundColor: '#EBFDF5',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 12,
          }}>
          <TextBold
            text={'Frame Dalam Kondisi Baik'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <Separator />
        <View style={styles.detailAndIcon}>
          <Image
            source={require('../../../../asset/icon-frame.png')}
            style={{width: 25, height: 25}}
          />
          <TextBold
            text={'Frame'}
            color={Colors.DEEPBLUE}
            size={16}
            style={{marginLeft: 10}}
          />
        </View>
        <View
          style={{
            height: 40,
            flexDirection: 'row',
            marginLeft: 24,
            marginRight: 12,
            backgroundColor: '#EBFDF5',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 12,
          }}>
          <TextBold
            text={'Frame Dalam Kondisi Baik'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 10}}
          />
        </View>
        <Separator />
        <View style={styles.detailAndIcon}>
          <Image
            source={require('../../../../asset/icon-engine.png')}
            style={{width: 25, height: 25}}
          />
          <TextBold
            text={'Engine'}
            color={Colors.DEEPBLUE}
            size={16}
            style={{marginLeft: 10}}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 12,
            justifyContent: 'space-between',
            marginBottom: 8,
          }}>
          <TextRegular
            text={'Engine Defect'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'Perlu perbaikan Engine Oil Leak (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000 (B)'} color={Colors.RED} size={12} />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Engine Defect'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'Perlu perbaikan Engine White (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000 (B)'} color={Colors.RED} size={12} />
        </View>
        <Separator />
        <View style={styles.detailAndIcon}>
          <Image
            source={require('../../../../asset/icon-interior.png')}
            style={{width: 25, height: 25}}
          />
          <TextBold
            text={'Interior'}
            color={Colors.DEEPBLUE}
            size={16}
            style={{marginLeft: 10}}
          />
        </View>

        <View style={styles.detail}>
          <TextRegular text={'Front'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            // text={item.item.item.Appraisal.overView[1].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000 (B)'} color={Colors.RED} size={12} />
        </View>
        <Separator />
        <View style={styles.detailAndIcon}>
          <Image
            source={require('../../../../asset/icon-eksterior.png')}
            style={{width: 25, height: 25}}
          />
          <TextBold
            text={'Eksterior'}
            color={Colors.DEEPBLUE}
            size={16}
            style={{marginLeft: 10}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'Front'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            // text={item.item.item.Appraisal.overView[0].value}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000'} color={Colors.RED} size={12} />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'Front'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'Perlu perbaikan Front Bumper (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000'} color={Colors.RED} size={12} />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'Back'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'Perlu perbaikan Front Rear Bumper (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000'} color={Colors.RED} size={12} />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'BPKB'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'ADA'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>

        <View style={styles.detail}>
          <TextRegular text={'STNK '} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'TIDAK ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detailpenguranganharga}>
          <TextBold text={'-800.000'} color={Colors.RED} size={12} />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Fatkur Pajak'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'NIK'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Buku Manual (B)'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Buku Service'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'Toolkit'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Kunci Serep '}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular
            text={'Dongkrak (B)'}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.detail}>
          <TextRegular text={'Spare tyre'} size={12} color={Colors.DEEPBLUE} />
          <TextBold
            text={'ADA (B)'}
            color={Colors.DEEPBLUE}
            size={12}
            style={{marginLeft: 12}}
          />
        </View>
        <View
          style={{
            alignSelf: 'center',
            width: '100%',

            flexDirection: 'column',
          }}>
          <Separator />
          <View style={styles.detail}>
            <TextBold
              text={'Total Pengurangan '}
              size={14}
              color={Colors.DEEPBLUE}
            />
            <TextBold text={'-RP. 6.400.000'} size={14} color={Colors.RED} />
          </View>

          <View style={styles.detail}>
            <TextBold
              text={'Harga Penawaran'}
              size={16}
              color={Colors.DEEPBLUE}
            />
            <TextBold
              text={'RP. 76.500.000'}
              size={16}
              color={Colors.DEEPBLUE}
            />
          </View>
        </View>
      </View>
      {/* Hasil Perhitungan Final End */}
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  detail: {
    flexDirection: 'row',
    marginHorizontal: 12,
    justifyContent: 'space-between',
    marginBottom: 8,
  },
  dataDetailCustomer: {
    flexDirection: 'row',
    marginHorizontal: 12,
    justifyContent: 'space-between',
    marginTop: 12,
    marginBottom: 12,
  },
  detailpenguranganharga: {
    marginHorizontal: 12,
    alignSelf: 'flex-end',
    marginBottom: 8,
  },
  detailAndIcon: {
    flexDirection: 'row',
    marginLeft: 12,
    marginVertical: 12,
  },
});

export default HasilPerhitunganFinal;
