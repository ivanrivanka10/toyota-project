import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  FlatList,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

const RiwayatNewCarComponent = ({navigation, dataNewCar}) => {
  const [detail, setDetail] = useState('No Deal');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );
  const LabelColors = {
    'No Deal': Colors.ORANGE,
    Deal: Colors.LIGHTGREEN,
  };
  const RenderItem = ({item}) => (
    <TouchableOpacity onPress={() => navigation.navigate('SumaryNewCar', item)}>
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '100%',
          backgroundColor: Colors.WHITE,
          borderWidth: 0.6,
          borderColor: Colors.BORDERGREY,
          borderRadius: 8,
          paddingHorizontal: 12,
          paddingTop: 10,
          paddingBottom: 8,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            // text={'TR-092018-246'}
            text={item.noNewCar}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <TextRegular
            // text={'Sen, 17 Sep 2018 - 10:30'}

            text={moment(item.updatedAt).format('YYYY-MM-DD HH:mm')}
            size={12}
            color={Colors.DEEPBLUE}
          />
        </View>

        <TextBold
          text={item.NewCar.carName}
          size={14}
          color={Colors.DEEPBLUE}
          style={{marginTop: 8}}
        />
        <View
          style={{
            borderStyle: 'dashed',
            width: '100%',
            borderWidth: 0.6,
            borderColor: Colors.BORDERGREY,
            marginTop: 12,
          }}></View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 8,
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <View>
            <TextBold
              // text={'Handoko'}
              text={item.SalesBranch.branch}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextRegular text={'Salesman'} size={10} color={Colors.BLACK} />
          </View>
          <View
            style={{
              backgroundColor: LabelColors[item.approvalStatus],
              paddingHorizontal: 6,
              paddingVertical: 4,
              borderRadius: 4,
            }}>
            <TextRegular
              text={item.approvalStatus}
              size={10}
              color={Colors.WHITE}
            />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={{paddingBottom: 60}}>
      {dataNewCar.length < 1 ? (
        <TextRegular
          text={'No Data'}
          color={Colors.BORDERGREY}
          size={18}
          style={{alignSelf: 'center', marginTop: 20}}
        />
      ) : null}
      <FlatList
        data={dataNewCar}
        renderItem={({item}) => <RenderItem item={item} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default RiwayatNewCarComponent;
