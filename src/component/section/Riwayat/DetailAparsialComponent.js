import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import RiwayatTradeInComponent from './RiwayatTradeInComponent';
import RiwayatNewCarComponent from './RiwayatNewCarComponent';
import {Header} from '../../global';
import HasilPerhitunganFinal from './DetailAppraisalUiComponent/HasilPerhitunganFina';
import DetailMobil from './DetailAppraisalUiComponent/DetailMobil';
import TotalHargadanDetailCustomer from './DetailAppraisalUiComponent/TotalHargadanDataCustomer';
import {convertCurrency} from '../../../utils/helpers/ConvertCurrentcy';
const DetailAparsialComponent = ({navigation, routing, item}) => {
  const data = item;
  const datas = item.approvalTradeIn.Appraisal;
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );
  const chooseKilometer = {
    1: 'ada',
    2: 'tidak ada',
  };

  const RenderItemKendaraan = ({item}) => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        paddingHorizontal: 10,
      }}>
      <TextRegular
        text={item.group}
        size={12}
        style={{marginVertical: 5, textAlign: 'justify'}}
        color={Colors.PRIMARY}
      />
      <View style={{width: '40%', flexDirection: 'column'}}>
        <TextBold
          text={item.name}
          size={12}
          style={{marginTop: 10, textAlign: 'justify'}}
          color={Colors.DEEPBLUE}
        />

        <TextRegular
          text={'- Rp.' + item.value}
          size={12}
          style={{marginTop: 5, textAlign: 'justify'}}
          color={Colors.RED}
        />
      </View>
    </View>
  );

  return (
    <ScrollView style={{flex: 1, backgroundColor: Colors.GREY, width: '100%'}}>
      <View style={{flex: 1, backgroundColor: Colors.GREY, width: '100%'}}>
        {/* Sisi Biru Atas */}
        <ImageBackground
          source={require('../../../asset/backgroundprofil.png')}
          style={{
            width: '100%',
            height: 112,
          }}>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              paddingHorizontal: 16,
              paddingTop: 16,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity onPress={() => navigation.pop()}>
              <Icon name="arrowleft" size={16} color={Colors.WHITE} />
            </TouchableOpacity>

            <TextBold text={'Summary'} color={Colors.WHITE} size={16} />

            <View style={{width: 16, height: 16}}></View>
          </View>
        </ImageBackground>

        <View style={{width: '100%'}}>
          <View
            style={{
              marginTop: -40,
              alignSelf: 'center',
              width: '90%',
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 10,
              flexDirection: 'column',
              marginHorizontal: 12,
            }}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                paddingVertical: 14,
                paddingHorizontal: 10,

                alignItems: 'center',
              }}>
              <Image
                source={{
                  uri: 'https://3.bp.blogspot.com/-JxFGHibMwy0/Vv-9X5sqlGI/AAAAAAAAAO0/t1N-oiW069UJIAXZ0dt5g9ORu9O-ry_GA/s640/Warna%2BGrand%2BNew%2BAvanza-Black%2BMetallic.png',
                }}
                style={{
                  width: 140,
                  height: 100,
                }}
              />
              <View style={{width: '50%', marginLeft: 20}}>
                <View style={{flexDirection: 'row'}}>
                  {/* AVANZA VELOZ A/T 2020 */}
                  <TextBold
                    text={item?.approvalTradeIn.Appraisal.carName}
                    color={Colors.DARKBLUETOYOTA}
                    style={{marginVertical: 5}}
                    size={16}
                  />
                </View>
                <Separator />
                <TextRegular
                  text={'Harga Penawaran'}
                  size={12}
                  style={{marginVertical: 8}}
                />
                <TextBold
                  text={
                    item?.approvalTradeIn?.Appraisal?.finalPrice
                      ?.hargaFinalAfterNego
                      ? convertCurrency(
                          item?.approvalTradeIn?.Appraisal?.finalPrice
                            ?.hargaFinalAfterNego,
                        )
                      : '-Rp.-'
                  }
                  size={14}
                  color={Colors.PRIMARY}
                />
              </View>
            </View>
            <Separator />
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../../asset/icon-bebasbanjir.png')}
                style={{width: 25, height: 25}}
              />
              <TextRegular
                text={'Bebas Banjir'}
                size={12}
                color={Colors.DEEPBLUE}
                style={{marginLeft: 8}}
              />
              <Image
                source={require('../../../asset/icon-bebaskecelakaan.png')}
                style={{width: 25, height: 25, marginLeft: 5}}
              />
              <TextRegular
                text={'Bebas Kecelakaan'}
                size={12}
                color={Colors.DEEPBLUE}
                style={{marginLeft: 8}}
              />
            </View>
          </View>
          {/* <DetailMobil navigation={navigation} item={item} /> */}
          {/* <HasilPerhitunganFinal navigation={navigation} item={item} /> */}
          <View
            style={{
              marginTop: 10,
              alignSelf: 'center',
              width: '90%',
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 10,
              padding: 10,
              flexDirection: 'column',
            }}>
            <TextBold
              text={'Hasil Perhitungan Final'}
              size={16}
              style={{alignSelf: 'center'}}
              color={Colors.DEEPBLUE}
            />
            <Separator />
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 12,
                marginVertical: 12,
              }}>
              <Image
                source={require('../../../asset/icon-overview.png')}
                style={{width: 25, height: 25}}
              />
              <TextBold
                text={'Overview'}
                color={Colors.DEEPBLUE}
                size={16}
                style={{marginLeft: 10}}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 12,
                justifyContent: 'space-between',
                marginBottom: 8,
              }}>
              <TextRegular
                text={'No Polisi'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={datas?.carDetail[7]?.value ?? 'N/A'}
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 12,
                justifyContent: 'space-between',
                marginBottom: 8,
              }}>
              <TextRegular
                text={'No Mesin'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={
                  datas?.carDetail[8]?.value
                    ? datas?.carDetail[8]?.value
                    : 'N/A'
                }
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 12,
                justifyContent: 'space-between',
                marginBottom: 8,
              }}>
              <TextRegular
                text={'No Rangka'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={
                  datas?.carDetail[9]?.value
                    ? datas?.carDetail[9]?.value
                    : 'N/A'
                }
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 12,
                justifyContent: 'space-between',
                marginBottom: 8,
              }}>
              <TextRegular
                text={'Warna Eksterior'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={
                  datas?.carDetail[10]?.value
                    ? datas?.carDetail[10]?.value
                    : 'N/A'
                }
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 12,
                justifyContent: 'space-between',
                marginBottom: 8,
              }}>
              <TextRegular
                text={'Jarak tempuh'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={
                  datas?.carDetail[11]?.value + 'Km'
                    ? datas?.carDetail[11]?.value + ' Km'
                    : 'N/A'
                }
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 12,
                justifyContent: 'space-between',
                marginBottom: 8,
              }}>
              <TextRegular
                text={'Tahun STNK'}
                size={12}
                color={Colors.DEEPBLUE}
              />

              <TextBold
                // text={'2017'}
                text={
                  datas?.carDetail[6]?.value
                    ? datas?.carDetail[6]?.value
                    : 'N/A'
                }
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>

            <Separator />
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 12,
                marginVertical: 12,
              }}>
              <Image
                source={require('../../../asset/icon-kendaraan.png')}
                style={{width: 25, height: 25}}
              />
              <TextBold
                text={'Kendaraan'}
                color={Colors.DEEPBLUE}
                size={16}
                style={{marginLeft: 10}}
              />
            </View>
            {item?.approvalTradeIn?.Appraisal?.finalPrice?.kendaraan ? (
              <View>
                <FlatList
                  data={item.approvalTradeIn.Appraisal.finalPrice.kendaraan[0]}
                  renderItem={({item}) => <RenderItemKendaraan item={item} />}
                />
              </View>
            ) : (
              <View
                style={{
                  height: 40,
                  flexDirection: 'row',
                  marginLeft: 24,
                  marginRight: 12,
                  backgroundColor: '#EBFDF5',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 12,
                }}>
                <TextBold
                  text={'Frame Dalam Kondisi Baik'}
                  color={Colors.DEEPBLUE}
                  size={12}
                  style={{marginLeft: 10}}
                />
              </View>
            )}

            <Separator />
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 12,
                marginVertical: 12,
              }}>
              <Image
                source={require('../../../asset/icon-frame.png')}
                style={{width: 25, height: 25}}
              />
              <TextBold
                text={'Frame'}
                color={Colors.DEEPBLUE}
                size={16}
                style={{marginLeft: 10}}
              />
            </View>
            <View
              style={{
                height: 40,
                flexDirection: 'row',
                marginLeft: 24,
                marginRight: 12,
                backgroundColor: '#EBFDF5',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 12,
              }}>
              <TextBold
                text={'Kendaraan Dalam Kondisi Baik'}
                color={Colors.DEEPBLUE}
                size={12}
                style={{marginLeft: 10}}
              />
            </View>
            <Separator />
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 12,
                marginVertical: 12,
              }}>
              <Image
                source={require('../../../asset/icon-engine.png')}
                style={{width: 25, height: 25}}
              />
              <TextBold
                text={'Engine'}
                color={Colors.DEEPBLUE}
                size={16}
                style={{marginLeft: 10}}
              />
            </View>
            {item?.approvalTradeIn?.Appraisal?.finalPrice?.photoEngine ? (
              <View>
                <FlatList
                  data={
                    item.approvalTradeIn.Appraisal.finalPrice.photoEngine[0]
                  }
                  renderItem={({item}) => <RenderItemKendaraan item={item} />}
                />
              </View>
            ) : (
              <View
                style={{
                  height: 40,
                  flexDirection: 'row',
                  marginLeft: 24,
                  marginRight: 12,
                  backgroundColor: '#EBFDF5',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 12,
                }}>
                <TextBold
                  text={'Frame Dalam Kondisi Baik'}
                  color={Colors.DEEPBLUE}
                  size={12}
                  style={{marginLeft: 10}}
                />
              </View>
            )}

            <Separator />
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 12,
                marginVertical: 12,
              }}>
              <Image
                source={require('../../../asset/icon-interior.png')}
                style={{width: 25, height: 25}}
              />
              <TextBold
                text={'Interior'}
                color={Colors.DEEPBLUE}
                size={16}
                style={{marginLeft: 10}}
              />
            </View>
            {item?.approvalTradeIn?.Appraisal?.finalPrice?.photoInterior ? (
              <View>
                <FlatList
                  data={
                    item.approvalTradeIn.Appraisal.finalPrice.photoInterior[0]
                  }
                  renderItem={({item}) => <RenderItemKendaraan item={item} />}
                />
              </View>
            ) : (
              <View
                style={{
                  height: 40,
                  flexDirection: 'row',
                  marginLeft: 24,
                  marginRight: 12,
                  backgroundColor: '#EBFDF5',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 12,
                }}>
                <TextBold
                  text={'Interior Dalam Kondisi Baik'}
                  color={Colors.DEEPBLUE}
                  size={12}
                  style={{marginLeft: 10}}
                />
              </View>
            )}

            <Separator />
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 12,
                marginVertical: 12,
              }}>
              <Image
                source={require('../../../asset/icon-eksterior.png')}
                style={{width: 25, height: 25}}
              />
              <TextBold
                text={'Eksterior'}
                color={Colors.DEEPBLUE}
                size={16}
                style={{marginLeft: 10}}
              />
            </View>
            {item?.approvalTradeIn?.Appraisal?.finalPrice?.photoExterior ? (
              <View>
                <FlatList
                  data={
                    item.approvalTradeIn.Appraisal.finalPrice.photoExterior[0]
                  }
                  renderItem={({item}) => <RenderItemKendaraan item={item} />}
                />
              </View>
            ) : (
              <View
                style={{
                  height: 40,
                  flexDirection: 'row',
                  marginLeft: 24,
                  marginRight: 12,
                  backgroundColor: '#EBFDF5',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 12,
                }}>
                <TextBold
                  text={'Frame Dalam Kondisi Baik'}
                  color={Colors.DEEPBLUE}
                  size={12}
                  style={{marginLeft: 10}}
                />
              </View>
            )}
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 10,
                marginTop: 12,
                justifyContent: 'space-between',
              }}>
              <TextBold
                text={'Total Pengurangan '}
                size={14}
                color={Colors.DEEPBLUE}
              />

              <TextBold
                text={
                  'Rp. ' +
                  item?.approvalTradeIn?.Appraisal?.finalPrice?.hargaPotong
                    ? 'Rp. ' +
                      convertCurrency(
                        item?.approvalTradeIn?.Appraisal?.finalPrice
                          ?.hargaPotong,
                      )
                    : '-Rp.-'
                }
                size={14}
                color={Colors.RED}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 12,
                justifyContent: 'space-between',
                marginBottom: 12,
                paddingHorizontal: 10,
              }}>
              <TextBold
                text={'Harga Penawaran'}
                size={16}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={
                  'Rp. ' +
                  item?.approvalTradeIn?.Appraisal?.finalPrice
                    ?.hargaFinalAfterNego
                    ? 'Rp. ' +
                      convertCurrency(
                        item?.approvalTradeIn?.Appraisal?.finalPrice
                          ?.hargaFinalAfterNego,
                      )
                    : '-Rp.-'
                }
                size={14}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>
          </View>
          <View
            style={{
              marginTop: 10,
              alignSelf: 'center',
              width: '90%',
              backgroundColor: Colors.WHITE,
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 10,
              padding: 15,
            }}>
            <TextBold
              text={'Data Customer'}
              color={Colors.DEEPBLUE}
              size={16}
              style={{
                marginVertical: 10,

                textAlign: 'center',
              }}
            />

            <Separator />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextRegular
                text={'Pemilik Kendaraan'}
                size={12}
                color={Colors.BLUETOYOTADETAIL}
                style={{marginVertical: 5}}
              />

              <TextBold
                // text={'Budi Suradi'}
                // text={item.item.Appraisal.carDetail[0].value}
                text={datas?.carDetail[0]?.value ?? 'N/A'}
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>

            <Separator />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextRegular
                text={'No. HP Customer'}
                size={12}
                color={Colors.BLUETOYOTADETAIL}
                style={{marginVertical: 5}}
              />

              <TextBold
                // text={'085212457896'}
                // text={item.item.Appraisal.carDetail[1].value}
                text={datas?.carDetail[1]?.value ?? 'N/A'}
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>

            <Separator />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextRegular
                text={'E-mail Customer'}
                size={12}
                color={Colors.BLUETOYOTADETAIL}
                style={{marginVertical: 5}}
              />

              <TextBold
                // text={'budi@gmail.com'}
                text={datas?.carDetail[2]?.value ?? 'N/A'}
                // text={item.item.Appraisal.carDetail[2].value}
                size={12}
                style={{marginVertical: 5}}
                color={Colors.DARKBLUETOYOTA}
              />
            </View>

            <Separator />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  detail: {
    flexDirection: 'row',
    marginHorizontal: 12,
    justifyContent: 'space-between',
    marginBottom: 8,
  },
  dataDetailCustomer: {
    flexDirection: 'row',
    marginHorizontal: 12,
    justifyContent: 'space-between',
    marginTop: 12,
    marginBottom: 12,
  },
  detailpenguranganharga: {
    marginHorizontal: 12,
    alignSelf: 'flex-end',
    marginBottom: 8,
  },
  detailAndIcon: {
    flexDirection: 'row',
    marginLeft: 12,
    marginVertical: 12,
  },
});

export default DetailAparsialComponent;
