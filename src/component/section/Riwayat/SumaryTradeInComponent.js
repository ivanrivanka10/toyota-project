import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {InputText, TextBold, TextRegular} from '../../global';
import {Colors} from '../../../styles';
import {Header} from '../../global';
import ModalAlasanPenolakan from '../../modal/Riwayat/TradeIn/ModalAlasanPenolakan';
import ModalKonfirmasiDeal from '../../modal/Riwayat/TradeIn/ModalKonfirmasiDeal';
import moment from 'moment';
import {convertCurrency} from '../../../utils/helpers/ConvertCurrentcy';

const SumaryTradeInComponent = ({navigation, routing, item}) => {
  const [notes, setNotes] = useState('');
  const [modalBottomNoDeal, setModalBottomNodeal] = useState(false);
  const [modalBottomKonfirmasi, setModalBottomKonfirmasi] = useState(false);
  const data = item;
  console.log(data);
  const dataPenawaran = data.calculation[1].price;
  const dataNego = data.calculation[1].price;
  const dataPenawaran1 =
    dataPenawaran !== 'N/A' ? parseFloat(dataPenawaran) : 0;
  const dataNego1 = dataNego !== 'N/A' ? parseFloat(dataNego) : 0;
  const MRP = convertCurrency(dataPenawaran1 + dataNego1);
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );

  return (
    // All
    <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
      <Header
        title="Sumary Trade In"
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />

      {/* Kode TR & Hari, Tanggal */}
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          height: 48,
          paddingVertical: 16,
          backgroundColor: 'white',
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
          justifyContent: 'space-between',
        }}>
        <TextRegular
          // text={'TR-092018-246'}
          text={data?.approvalTradeIn?.Appraisal?.Booking?.noBooking}
          size={12}
          color={'#6A6E7C'}
        />
        <TextRegular
          // text={'Sen, 17 Sep 2018 - 10:30'}
          text={moment(data?.approvalTradeIn?.updatedAt).format(
            'YYYY-MM-DD HH:mm',
          )}
          size={12}
          color={'#6A6E7C'}
        />
      </View>
      {/* Kode TR & Hari, Tanggal Selesai di Sini*/}

      {/* Used Car Information Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'Used Car Information'}
          size={14}
          color={Colors.DEEPBLUE}
          style={{marginVertical: 5}}
        />
        <Separator />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular text={'Mobil'} size={14} color={Colors.DEEPBLUE} />
          <TextBold
            // text={'Avanza G AT 2016'}
            text={
              item?.approvalTradeIn?.NewCar?.carName
                ? item?.approvalTradeIn?.NewCar?.carName
                : 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular text={'Plat Nomor'} size={14} color={Colors.DEEPBLUE} />
          <TextBold
            // text={'coba'}
            text={
              item?.approvalTradeIn?.Appraisal?.carDetail[7]?.value ?? 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity
            style={{
              width: '100%',
              paddingVertical: 10,
              borderRadius: 6,
              backgroundColor: Colors.PRIMARY,
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingRight: 12,
            }}
            onPress={() => navigation.navigate('DetailAparsial', item)}>
            <TextRegular
              text={'Liat Detail Appraisal'}
              color={Colors.WHITE}
              style={{paddingLeft: 12}}
            />

            <Image
              style={{
                width: 12,
                height: 12,
                tintColor: 'white',
                paddingVertical: 10,
              }}
              source={require('../../../asset/icon-next.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
      {/* Used Car Information Selesai di Sini  */}

      {/* New Car Information Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <TextBold
          text={'New Car Information'}
          size={14}
          color={Colors.DEEPBLUE}
          style={{marginVertical: 5}}
        />

        <Separator />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular text={'Mobil'} size={14} color={Colors.DEEPBLUE} />
          <TextBold
            text={item?.approvalTradeIn?.NewCar?.carName ?? 'N/A'}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular text={'OTR'} size={14} color={Colors.DEEPBLUE} />
          <TextBold
            // text={'Rp. 276.600.000 (B)'}
            text={
              convertCurrency(item?.approvalTradeIn?.NewCar?.otr)
                ? 'Rp ' + convertCurrency(item?.approvalTradeIn?.NewCar?.otr)
                : 'N/A'
            }
            color={Colors.DEEPBLUE}
          />
        </View>
      </View>
      {/* Used Car Information Selesai di Sini  */}
      {/*Ruang Nego Harga Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextBold
            text={'Ruang Nego Harga'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 5}}
          />
        </View>

        <Separator />

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'Harga Penawaran'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            // text={'Rp. 78.500.000'}
            text={
              convertCurrency(data?.calculation[1]?.price)
                ? 'Rp.' + convertCurrency(data?.calculation[1]?.price)
                : 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'Harga Beli Max'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={
              convertCurrency(data?.calculation[0]?.price)
                ? 'Rp.' + convertCurrency(data?.calculation[0]?.price)
                : 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular text={'Harga Final'} size={14} color={Colors.DEEPBLUE} />
          <TextBold
            text={
              convertCurrency(data?.calculation[8]?.price)
                ? 'Rp.' + convertCurrency(data?.calculation[8]?.price)
                : 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
      </View>
      {/* Ruang Nego harga end */}
      {/*Diskon New Car Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextBold
            text={'Diskon New Car'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 5}}
          />
        </View>

        <Separator />

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular text={'Dikon Kacab'} size={14} color={Colors.DEEPBLUE} />
          <TextBold
            text={
              'Rp. ' + convertCurrency(data?.calculation[6]?.price) ?? 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'Pengajuan Diskon BOD'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={
              'Rp. ' + convertCurrency(data?.calculation[7]?.price) ?? 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'GP AUTO2000 After disc'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            // text={' Rp. 12.168.000 (B)'}
            text={convertCurrency(
              data?.approvalTradeIn?.calculationDetail[7]?.price,
              'Rp. ',
            )}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
      </View>
      {/* Diskon New Car  harga end */}
      {/*Gross Profit Trust Dimulai di sini*/}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextBold
            text={'Gross Profit Trust'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 5}}
          />
        </View>

        <Separator />

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'Proyeksi Harga Jual'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={convertCurrency(
              data.approvalTradeIn.calculationDetail[2].price,
              'Rp. ',
            )}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'COGS (Harga Beli + biaya Rekondisi)+ OPEX'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{width: '50%'}}
          />
          <TextBold
            text={convertCurrency(
              data.approvalTradeIn.calculationDetail[3].price,
              'Rp. ',
            )}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <TextRegular
            text={'Proyeksi NPBT Trust'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={
              'Rp. ' + convertCurrency(data?.calculation[4]?.price) ?? 'N/A'
            }
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
      </View>
      {/* Gross Profit Trust end */}
      {/* Request Section Start */}
 {/* Harga Final start */}
      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <TextRegular text={'Harga Final'} size={12} color={Colors.DEEPBLUE} />
        <View>
          <InputText
            style={{
              width: '100%',
              height: 32,
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 4,
              paddingVertical: 6,
              paddingHorizontal: 12,
              marginTop: 4,
            }}
            placeholderText={'raa'}
            // value={item?.item?.profile?.noKTP ?? '-'}
            onChangeText={text => setNotes(text)}
            multiline>
           
          </InputText>
        </View>
        <View style={{marginTop: 12}}>
          <TextRegular
            text={'Total Benefit TI (Final) : '}
            size={12}
            color={Colors.DEEPBLUE}
          />
          <View>
          <InputText
            placeholderText="1231321313"
            value={'masukan harga final'}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          </View>
        </View>

        <TextRegular
          text={'Last Status'}
          size={12}
          color={Colors.DEEPBLUE}
          style={{marginTop: 14}}
        />
        <View style={{flexDirection: 'row', marginTop: 16}}>
          <TextBold
            text={
              item?.approvalTradeIn?.approvalStatus
                ? item?.approvalTradeIn?.approvalStatus.toUpperCase()
                : 'N/A'
            }
            size={14}
            color={Colors.RED}
          />
          <View
            style={{
              width: 1,
              height: 19,
              backgroundColor: '#d3e1ce',
              marginHorizontal: 12,
            }}
          />
          <TextRegular
            text={moment(data.approvalHistory[0].createdAt).format(
              'YYYY-MM-DD HH:mm',
            )}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
      </View>
      {/* Harga Final End */}

      <View
        style={{
          flexDirection: 'row',

          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
        }}>
        <TouchableOpacity
          style={{
            backgroundColor: '#E5E8EE',
            width: 156,
            height: 36,
            alignItems: 'center',
            paddingVertical: 8,
            borderRadius: 4,
          }}
          onPress={() => setModalBottomNodeal(true)}>
          <TextBold text={'NO DEAL'} color={Colors.DEEPBLUE} />
        </TouchableOpacity>
        <ModalAlasanPenolakan
          show={modalBottomNoDeal}
          onClose={() => setModalBottomNodeal(false)}
        />
        <TouchableOpacity
          style={{
            backgroundColor: Colors.PRIMARY,
            width: 156,
            height: 36,
            alignItems: 'center',
            paddingVertical: 8,
            borderRadius: 4,
            position: 'absolute',
            right: 0,
          }}
          onPress={() => setModalBottomKonfirmasi(true)}>
          <TextBold text={'DEAL'} color={Colors.WHITE} />
        </TouchableOpacity>
        <ModalKonfirmasiDeal
          show={modalBottomKonfirmasi}
          onClose={() => setModalBottomKonfirmasi(false)}
        />
      </View>

      {/* Isian View, biar scrollView/flatList bisa kelihatan semuanya */}
      <View style={{width: '100%', height: 50}} />
    </ScrollView>
  );
};

export default SumaryTradeInComponent;
