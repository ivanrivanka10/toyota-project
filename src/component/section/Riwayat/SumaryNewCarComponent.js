import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {TextBold, TextRegular, TextMedium} from '../../global';
import {Colors} from '../../../styles';
import {Header} from '../../global';
import Icon from 'react-native-vector-icons/FontAwesome';
import { convertCurrency } from '../../../utils/helpers/ConvertCurrentcy';
import moment from 'moment';

const SumaryNewCarComponent = ({navigation, item}) => {
  console.log(item);
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );
  const LabelColors = {
    'No Deal': Colors.ORANGE,
    Deal: Colors.LIGHTGREEN,
  };
  return (
    // All
    <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
      <Header
        title="Summary New Car"
        titleColor={Colors.DEEPBLUE}
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />
      {/* Deal or No deal start*/}
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          height: 48,
          backgroundColor: LabelColors[item?.approvalNewCar?.approvalStatus],
          marginVertical: 5,
          padding: 10,

          justifyContent: 'space-between',
        }}>
        <TextBold size={14} text={item?.approvalNewCar?.approvalStatus} color={Colors.WHITE} />
        <TextBold
          size={14}
          text={moment(item?.approvalNewCar?.updatedAt).format('YYYY-MM-DD HH:mm')}
          color={Colors.WHITE}
        />
      </View>
      {/* Deal or No deal End */}
      {/* Kode TR & Hari, Tanggal */}
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          height: 48,
          paddingVertical: 16,
          backgroundColor: 'white',
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
          justifyContent: 'space-between',
        }}>
        <TextRegular
          // text={'TR-092018-246'}
          text={item.approvalNewCar.noNewCar}
          size={12}
          color={Colors.DEEPBLUE}
        />

        <TextRegular
          // text={'Sen, 17 Sep 2018 - 10:30'}
          text={item?.approvalNewCar?.SalesBranch?.BranchHead?.name}
          size={12}
          color={Colors.DEEPBLUE}
        />
      </View>
      {/* Kode TR & Hari, Tanggal Selesai di Sini*/}
      {/*Ruang Nego Harga Dimulai di sini*/}
      {/* Nama customer dan NO hp */}
      <View
        style={{
          width: '100%',
          paddingVertical: 16,
          backgroundColor: 'white',
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'Nama Customer'}
            size={14}
            color={Colors.DEEPBLUE}
          />

          <TextBold
            // text={'coba'}
            text={item?.approvalNewCar?.customerName}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 24,
            justifyContent: 'space-between',
          }}>
          <TextRegular text={'No Hp'} size={14} color={Colors.DEEPBLUE} />

          <TextRegular
            // text={'coba'}
            text={item?.approvalNewCar?.phone}
            size={14}
            color={Colors.DEEPBLUE}
          />
        </View>
      </View>
      {/* Nama cust dan no HP end*/}
      {/* Detail Mobil start */}
      <View
        style={{
          width: '100%',
          paddingVertical: 16,
          backgroundColor: 'white',
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
        }}>
        <TextBold text={'Detail Mobil'} color={Colors.DEEPBLUE} size={14} />

        <Separator />
        <TextRegular text={'New Car'} size={12} style={{marginTop: 12}} />
        <View>
          <TextBold
            // text={'Rush S AT TRD'}
            text={item?.approvalNewCar?.NewCar?.carName}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />

          <TextBold
            // text={'RP. 27.600.000 (B)'}
            text={convertCurrency(item?.approvalNewCar?.NewCar?.otr) ? 'Rp. '+ convertCurrency(item?.approvalNewCar?.NewCar?.otr) : 'N/A'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{position: 'absolute', right: 0, marginTop: 4}}
          />
        </View>
      </View>
      {/* Detail Mobil end*/}
      {/* Request diskon start */}
      <View
        style={{
          width: '100%',
          paddingVertical: 16,
          backgroundColor: 'white',
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            justifyContent: 'space-between',
          }}>
          <TextBold text={'Request Diskon'} color={Colors.DEEPBLUE} size={14} />
          <Icon name="minus" size={9} color={Colors.BLACK} />
        </View>

        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              width: '96%',
              height: 1,
              backgroundColor: Colors.BLACK,
              marginVertical: 5,
            }}></View>
          <Icon name="plus" size={9} color={Colors.BLACK} />
        </View>

        <View style={{marginTop: 12}}>
          <TextRegular
            text={'Diskon'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextBold
            // text={'RP. 15.600.000 (B)'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{position: 'absolute', right: 0, marginTop: 4}}
          />
        </View>
        <View style={{marginTop: 24}}>
          <TextRegular
            text={'Harga Mobil'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />

          <TextRegular
            // text={'RP. 235.000.000 (B)'}
            text={item?.approvalNewCar?.finalPrice ? 'Rp. '+ convertCurrency(item?.approvalNewCar?.finalPrice) : 'N/A'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{position: 'absolute', right: 0, marginTop: 4}}
          />
        </View>
      </View>
      {/* Request diskon end*/}

      {/* Isian View, biar scrollView/flatList bisa kelihatan semuanya */}
      <View style={{width: '100%', height: 50}} />
    </ScrollView>
  );
};

export default SumaryNewCarComponent;
