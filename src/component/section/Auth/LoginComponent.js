import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Modal,
} from 'react-native';
import {InputText, TextBold, TextRegular} from '../../global';
import {Colors} from '../../../styles';
import ModalLevelUser from '../../modal/Auth/ModalLevelUser';
import apiProvider from '../../../utils/service/apiProvider';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';

const LoginComponent = ({
  navigation,
  data,
  email,
  setEmail,
  password,
  setPassword,
  hidePassword,
  setShowPassword,
  onPressLogin,
}) => {
  const [modalBottom, setModalBottom] = useState(false);
  const [dataLogin, setDataLogin] = useState('');
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
        paddingHorizontal: 20,
        justifyContent: 'center',
      }}>
      <View
        style={{
          backgroundColor: Colors.WHITE,
          padding: 16,
          alignItems: 'center',
        }}>
        <View
          style={{
            width: 170,
            height: 36,
            backgroundColor: Colors.DEEPBLUE,
            borderRadius: 100,
            marginTop: -34,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TextBold text={'DIGITAL APPROVAL'} size={16} color={Colors.WHITE} />
        </View>

        <Image
          source={require('../../../asset/logo.png')}
          style={{width: 127, height: 55, marginTop: 27}}
        />

        <InputText
          placeholderText="Masukkan Email"
          style={styles.inputText}
          onChangeText={text => setEmail(text)}
          value={email}
          typeOfInput="email"
        />
        <InputText
          placeholderText="Masukkan Password"
          style={styles.inputText}
          onChangeText={text => setPassword(text)}
          value={password}
          isPassword={true}
          showPassword={hidePassword}
          setShowPassword={setShowPassword}
          typeOfInput="password"
        />
        <View
          style={{
            alignItems: 'flex-end',
            marginTop: 16,
            width: '100%',
            marginBottom: 24,
          }}>
          <TouchableOpacity>
            <TextRegular text={'Reset Password'} color={Colors.PRIMARY} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.btnLogin}
          onPress={async () => {
            const params = {
              email: email,
              password: password,
            };
            if (!email && !password) {
              return alert('Harap Isi Email & Password Anda');
            }
            if (!email) {
              return alert('Harap Isi Email Anda');
            }
            if (!password) {
              return alert('Harap Isi Password Anda');
            }
            const update = await apiProvider.PostDataLogin(params);
            setDataLogin(update);

            if (update.code === 200) {
              setModalBottom(true);
            } else {
              return alert('Login Gagal');
            }
          }}>
          <TextBold text="Log In" color={Colors.WHITE} size={18} />
        </TouchableOpacity>
        <ModalLevelUser
          show={modalBottom}
          onClose={() => setModalBottom(false)}
          title="Pilih Cabang"
          data={dataLogin}
          navigation={navigation}>
          <View style={{paddingLeft: 16, paddingVertical: 18}}>
            <TextBold text="Deal" />
            <TextRegular text="No Deal" style={{marginVertical: 18}} />
          </View>
        </ModalLevelUser>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    marginTop: 20,
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoginComponent;
