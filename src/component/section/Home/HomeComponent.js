import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Modal,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import ModalFilter from '../../modal/Home/ModalFilter';
import HomeTradeInComponent from './HomeTradeInComponent';
import HomeNewCarComponent from './HomeNewCarComponent';
import ModalTambahDataCustomer from '../../modal/Home/ModalTambahDataCustomer';

const HomeComponent = ({
  navigation,
  dataTotalDashboard,
  dataTradein,
  dataNewCar,
  onSearch,
  keyword,
  filter,
  setFilter,
  onSaveFilter,
  dataListCar,
  modelCar,
  setModelCar,
  phone,
  name,
  setPhone,
  setName,
  addNewCar,
}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  const [isOpenTradeIn, setisOpenTradeIn] = useState(true);
  const inverseTradeIn = () => setisOpenTradeIn(true);
  const inverseNewCar = () => setisOpenTradeIn(false);
  const [modalBottom, setModalBottom] = useState(false);
  const [modalFilterVisible, setModalFilterVisible] = useState(false);
  const [ModalTambahDataCustomerVisible, setModalTambahDataCustomerVisible] =
    useState(false);

  // On Refresh
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 5000);
  }, []);

  return (
    // All
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {/* Sisi Biru Atas */}
        <Image
          style={{height: 380, width: '100%'}}
          source={require('../../../asset/img-header.jpg')}
        />
        <View
          style={{
            flexDirection: 'row',
            marginTop: -350,
            justifyContent: 'space-between',
            width: '90%',
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'column'}}>
            <TextRegular
              text={'Kepala Cabang'}
              size={12}
              color={Colors.WHITE}
            />

            <TextRegular
              text={'Cabang Bintaro'}
              size={16}
              color={Colors.WHITE}
            />
          </View>

          {/* Lonceng Notifikasi Dimulai Dari Sini  */}
          <TouchableOpacity style={{flexDirection: 'row'}}>
            <Image
              style={{
                height: 25,
                width: 22,
                alignSelf: 'center',
              }}
              source={require('../../../asset/icon-lonceng-3.png')}
            />
            <View
              style={{
                width: 23,
                height: 23,
                backgroundColor: '#FFCD27',
                borderRadius: 20,
                borderWidth: 3,
                marginTop: -8,
                marginLeft: -10,
                borderColor: Colors.PRIMARY,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {/* Loading Notification dimulai dari Sini */}
              {dataTotalDashboard?.appraisalTotal ? (
                <TextBold
                  text={dataTotalDashboard.appraisalTotal}
                  size={10}
                  color={Colors.WHITE}
                />
              ) : (
                <ActivityIndicator size={'large'} />
              )}
              {/* Loading Notification selesai dari Sini */}
            </View>
          </TouchableOpacity>
        </View>
        {/* Lonceng Notifikasi Selesai Di Sini  */}

        {/* Kotak Putih Atas  */}
        <View
          style={{
            backgroundColor: Colors.WHITE,
            width: '90%',
            height: 250,
            alignSelf: 'center',
            marginTop: 15,
            borderRadius: 10,
          }}>
          <TextRegular
            // '1 Sep - 12 September 2020'
            text={dataTotalDashboard.mountToDate}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 10, marginHorizontal: 10}}
          />
          <Separator />

          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
              alignSelf: 'center',
            }}>
            {/* Kotak Abu 1 dari 4 */}
            <TouchableOpacity
              onPress={() => navigation.navigate('CekHarga')}
              style={{
                margin: 10,
                width: '45%',
                borderRadius: 5,
                backgroundColor: Colors.GREY,
              }}>
              <View style={{flexDirection: 'row', marginVertical: 5}}>
                <View
                  style={{
                    backgroundColor: Colors.PRIMARY,
                    width: 3,
                    height: 20,
                  }}
                />
                <TextRegular
                  text={'Cek Harga'}
                  style={{marginHorizontal: 5}}
                  size={12}
                />
              </View>
              {/* Loading Cek Harga dimulai dari Sini */}
              {dataTotalDashboard?.code ? (
                <TextBold
                  text={dataTotalDashboard.priceCheckTotal}
                  color={Colors.BLACK}
                  style={{
                    marginHorizontal: 10,
                    marginVertical: 5,
                    fontSize: 20,
                  }}
                />
              ) : (
                <ActivityIndicator color={Colors.PRIMARY} />
              )}
              {/* Loading Cek Harga selesai di Sini */}
            </TouchableOpacity>

            {/* Kotak Abu 2 dari 4 */}
            <TouchableOpacity
              onPress={() => navigation.navigate('TotalAppraisal')}
              style={{
                margin: 10,
                width: '45%',
                borderRadius: 5,
                backgroundColor: Colors.GREY,
              }}>
              <View style={{flexDirection: 'row', marginVertical: 5}}>
                <View
                  style={{backgroundColor: Colors.GREEN, width: 3, height: 20}}
                />
                <TextRegular
                  text={'Total Appraisal'}
                  style={{marginHorizontal: 5}}
                  size={12}
                />
              </View>
              {/* Loading Total Appraisal dimulai dari Sini */}
              {dataTotalDashboard?.code ? (
                <TextBold
                  text={dataTotalDashboard.appraisalTotal}
                  color={Colors.BLACK}
                  // text={'2'}
                  style={{
                    marginHorizontal: 10,
                    marginVertical: 5,
                    fontSize: 20,
                  }}
                />
              ) : (
                <ActivityIndicator color={Colors.GREEN} />
              )}
              {/* Loading Total Appraisal Selesai di Sini */}
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
              alignSelf: 'center',
            }}>
            {/* Kotak Abu 3 dari 4 */}
            <TouchableOpacity
              onPress={() => navigation.navigate('PO')}
              style={{
                margin: 10,
                width: '45%',
                borderRadius: 5,
                backgroundColor: Colors.GREY,
              }}>
              <View style={{flexDirection: 'row', marginVertical: 5}}>
                <View
                  style={{
                    backgroundColor: Colors.DEEPORANGE,
                    width: 3,
                    height: 20,
                  }}
                />
                <TextRegular
                  text={'PO'}
                  style={{marginHorizontal: 5}}
                  size={12}
                />
              </View>

              {/* Loading PO Dimulai dari di Sini */}
              {dataTotalDashboard?.code ? (
                <TextBold
                  text={dataTotalDashboard.POTotal}
                  color={Colors.BLACK}
                  // text={'2'}
                  style={{
                    marginHorizontal: 10,
                    marginVertical: 5,
                    fontSize: 20,
                  }}
                />
              ) : (
                <ActivityIndicator color={Colors.DEEPORANGE} />
              )}
              {/* Loading PO selesai di Sini */}
            </TouchableOpacity>

            {/* Kotak Abu 4 dari 4 */}
            <TouchableOpacity
              onPress={() => navigation.navigate('POValid')}
              style={{
                margin: 10,
                width: '45%',
                borderRadius: 5,
                backgroundColor: Colors.GREY,
              }}>
              <View style={{flexDirection: 'row', marginVertical: 5}}>
                <View
                  style={{backgroundColor: Colors.RED, width: 3, height: 20}}
                />
                <TextRegular
                  text={'PO Valid'}
                  style={{marginHorizontal: 5}}
                  size={12}
                />
              </View>
              {dataTotalDashboard?.code ? (
                <TextBold
                  text={dataTotalDashboard.POValidTotal}
                  color={Colors.BLACK}
                  // text={'2'}
                  style={{
                    marginHorizontal: 10,
                    marginVertical: 5,
                    fontSize: 20,
                  }}
                />
              ) : (
                <ActivityIndicator color={Colors.RED} />
              )}
            </TouchableOpacity>
          </View>
        </View>

        {/* Cari dan Filter */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 16,
            marginBottom: 14,
            width: '90%',
            alignSelf: 'center',
          }}>
          <View
            style={{
              width: '85%',
              padding: 14,
              backgroundColor: Colors.WHITE,
              borderRadius: 8,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{width: '80%'}}>
              <TextInput
                style={{height: 16, padding: 0}}
                onChangeText={text => onSearch(text)}
                placeholder="Telusuri..."
                value={keyword}
              />
            </View>
            <Image
              source={require('../../../asset/icon-cari.png')}
              style={{
                height: 18,
                width: 18,
              }}
            />
          </View>

          <TouchableOpacity
            onPress={() => setModalBottom(true)}
            style={{
              padding: 14,
              backgroundColor: Colors.WHITE,
              borderRadius: 8,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../asset/icon-filter.png')}
              style={{
                height: 18,
                width: 18,
              }}
            />
            <ModalFilter
              show={modalBottom}
              onClose={() => setModalBottom(false)}
              filter={filter}
              setFilter={setFilter}
              onSaveFilter={onSaveFilter}></ModalFilter>
          </TouchableOpacity>
        </View>

        {/* Trade In dan New Car Slide dimulai di sini*/}
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 40,
            alignSelf: 'center',
            width: '90%',
          }}>
          <TouchableOpacity
            onPress={inverseTradeIn}
            style={{
              width: '50%',
              height: 40,
              backgroundColor: Colors.WHITE,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              borderrightWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
              borderRightWidth: 1,
              borderRightColor: Colors.GREY,
            }}>
            {isOpenTradeIn ? (
              <TextBold text={'Trade In'} color={Colors.PRIMARY} />
            ) : (
              <TextRegular text={'Trade In'} />
            )}
          </TouchableOpacity>

          <TouchableOpacity
            onPress={inverseNewCar}
            style={{
              width: '50%',
              height: 40,
              backgroundColor: Colors.WHITE,
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
              borderrightWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
              borderLeftWidth: 1,
              borderLeftColor: Colors.GREY,
            }}>
            {isOpenTradeIn ? (
              <TextRegular text={'New Car'} />
            ) : (
              <TextBold text={'New Car'} color={Colors.PRIMARY} />
            )}
          </TouchableOpacity>
        </View>
        {/* Trade In dan New Car Slide selesai  di sini*/}

        {/* Materi Flat List Trade In dan New Car Dimulai Dari Sini  */}
        <View>
          {isOpenTradeIn ? (
            <HomeTradeInComponent
              navigation={navigation}
              dataTradein={dataTradein}
            />
          ) : (
            <HomeNewCarComponent
              navigation={navigation}
              dataNewCar={dataNewCar}
            />
          )}
          <View style={{width: '100%', height: 100}} />
        </View>

        {/* Materi Flat List Trade In dan New Car Selesai Dari Sini  */}
      </ScrollView>

      {/* Tombol Tambah Data Pembeli Mobil  */}
      <TouchableOpacity
        onPress={() => setModalTambahDataCustomerVisible(true)}
        // Lokasi di '../component/modal/ModalTambahDataCustomer.js'

        style={{
          position: 'absolute',
          bottom: '5%',
          right: '5%',
        }}>
        <Image
          source={require('../../../asset/img-tambah-data.png')}
          style={{width: 50, height: 51}}
        />
      </TouchableOpacity>

      {/* Modal Filter Selesai Di sini  */}
      {/* Modal Tambah Data Customer Dimulai Di sini  */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={ModalTambahDataCustomerVisible}
        onRequestClose={() => {
          setModalTambahDataCustomerVisible(false);
        }}>
        <ModalTambahDataCustomer
          dataListCar={dataListCar}
          onRequestClose={() => {
            setModalTambahDataCustomerVisible(false);
          }}
          modelCar={modelCar}
          setModelCar={setModelCar}
          phone={phone}
          name={name}
          setPhone={setPhone}
          setName={setName}
          addNewCar={addNewCar}
        />
      </Modal>

      {/* Modal Tambah Data Customer Selesai Di sini  */}
      {/* Modal  Selesai Di sini  */}
    </View>
  );
};

export default HomeComponent;
