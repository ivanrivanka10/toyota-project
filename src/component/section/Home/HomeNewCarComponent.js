import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  FlatList,
  ActivityIndicator,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import moment from 'moment';
import 'moment/locale/id';

const HomeNewCarComponent = ({navigation, dataNewCar}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        marginVertical: 5,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderColor: Colors.BORDERGREY,
      }}
    />
  );

  return (
    <View style={{paddingBottom: 60}}>
      {dataNewCar ? (
        <FlatList
          data={dataNewCar}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => navigation.navigate('BeliMobil', (item = {item}))}
              style={{
                marginTop: 10,
                alignSelf: 'center',
                width: '90%',
                backgroundColor: Colors.WHITE,
                borderWidth: 1,
                borderColor: Colors.BORDERGREY,
                borderRadius: 10,
                padding: 15,
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <TextRegular
                  // 'TR-092018-246'
                  text={item.noNewCar}
                  style={{fontWeight: '300', fontSize: 12}}
                />

                <TextRegular
                  // 'Sen, 17 Sep 2018 - 10:30'
                  text={moment(item.createdAt).format('ddd, DD-MM-YYYY HH:mm')}
                  style={{fontSize: 12}}
                  color={Colors.DEEPBLUE}
                />
              </View>
              <TextBold
                // AVANZA VELOZ A/T 2020
                text={item.NewCar.carName}
                style={{marginVertical: 5}}
                color={Colors.DEEPBLUE}
              />

              <Separator />
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View>
                  <TextBold
                    // 'Handoko'
                    text={item.SalesBranch.branch}
                    size={12}
                    color={Colors.DARKBLUETOYOTA}
                  />
                  <View style={{flexDirection: 'row'}}>
                    <TextRegular
                      // 'Salesman'
                      text={'Kepala Cabang | '}
                      // text={item.SalesBranch.branch}
                      size={10}
                      style={{fontWeight: '300'}}
                    />

                    <TextRegular
                      text={item.SalesBranch.BranchHead.name}
                      size={10}
                      style={{fontWeight: '300'}}
                    />
                  </View>
                </View>

                <View
                  style={{
                    backgroundColor: Colors.PRIMARY,
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 5,
                    alignSelf: 'center',
                  }}>
                  <TextRegular
                    text={item.approvalStatus}
                    size={10}
                    color={Colors.WHITE}
                  />
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      ) : (
        <View style={{marginTop: 50}}>
          <ActivityIndicator size={'large'} color={Colors.PRIMARY} />
        </View>
      )}
    </View>
  );
};

export default HomeNewCarComponent;
