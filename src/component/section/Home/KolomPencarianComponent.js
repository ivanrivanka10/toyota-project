import React from 'react';
import {View, Text, TextInput, Image} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';

const KolomPencarianComponent = ({}) => {
  return (
    <View>
      {/* Kolom pencarian dimulai dari sini  */}
      <View
        style={{
          flexDirection: 'row',
          width: '90%',
          alignSelf: 'center',
          margin: 15,
          backgroundColor: Colors.WHITE,
          borderRadius: 15,
        }}>
        <TextInput
          style={{
            backgroundColor: Colors.WHITE,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
            width: '85%',
          }}
          placeholder="   Telusuri..."
          keyboardType="default"
        />
        <View
          style={{
            width: 50,
            height: 50,
            backgroundColor: Colors.WHITE,
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}>
          <Image
            source={require('../../../asset/icon-cari.png')}
            style={{
              height: 20,
              marginVertical: 15,
              width: 20,
              marginLeft: 15,
              backgroundColor: '#F6F8FF',
            }}
          />
        </View>
      </View>
      {/* Kolom pencarian dan iconnya selesai di sini */}
    </View>
  );
};

export default KolomPencarianComponent;
