import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  FlatList,
  ActivityIndicator,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import moment from 'moment';
import 'moment/locale/id';

const HomeTradeInComponent = ({navigation, dataTradein}) => {
  const [detail, setDetail] = useState('Deal');
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        marginVertical: 5,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderColor: Colors.BORDERGREY,
      }}
    />
  );

  return (
    <View style={{paddingBottom: 60}}>
      {dataTradein ? (
        <FlatList
          data={dataTradein}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('ToolTradeIn', (item = {item}))
              }
              style={{
                marginTop: 10,
                alignSelf: 'center',
                width: '90%',
                backgroundColor: Colors.WHITE,
                borderWidth: 1,
                borderColor: Colors.BORDERGREY,
                borderRadius: 10,
                padding: 15,
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <TextRegular
                  // 'TR-092018-246'
                  text={item.Appraisal.Booking.noBooking}
                  style={{fontWeight: '300', fontSize: 12}}
                />
                <TextRegular
                  // 'Sen, 17 Sep 2018 - 10:30'
                  text={moment(item.createdAt).format('ddd, DD-MM-YYYY HH:mm')}
                  style={{fontSize: 12}}
                  color={Colors.DEEPBLUE}
                />
              </View>
              <View style={{flexDirection: 'row'}}>
                {/* AVANZA VELOZ A/T 2020 */}
                {[3, 4, 6].map(index => (
                  <React.Fragment key={index}>
                    <TextBold
                      text={item.Appraisal.carDetail[index].value}
                      color={Colors.DARKBLUETOYOTA}
                      style={{marginVertical: 5}}
                    />
                    <TextBold text={' '} />
                  </React.Fragment>
                ))}
              </View>

              <Separator />
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View>
                  <TextBold
                    // 'Handoko'
                    text={item.Appraisal.Booking.SalesProfile.name}
                    size={12}
                    color={Colors.DARKBLUETOYOTA}
                  />
                  <View style={{flexDirection: 'row'}}>
                    <TextRegular
                      // 'Salesman'
                      text={'Salesman | '}
                      size={10}
                      style={{fontWeight: '300'}}
                    />

                    <TextRegular
                      text={
                        item.Appraisal.Booking.SalesProfile.SalesBranch.branch
                      }
                      size={10}
                      style={{fontWeight: '300'}}
                    />
                  </View>
                </View>

                <View
                  style={{
                    backgroundColor: Colors.PRIMARY,
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 5,
                    alignSelf: 'center',
                  }}>
                  <TextRegular
                    // New Approval
                    text={item.approvalStatus}
                    size={10}
                    color={Colors.WHITE}
                  />
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      ) : (
        <View style={{marginTop: 50}}>
          <ActivityIndicator size={'large'} color={Colors.PRIMARY} />
        </View>
      )}
    </View>
  );
};

export default HomeTradeInComponent;
