import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import KolomPencarianComponent from './KolomPencarianComponent';

const CekHargaComponent = ({navigation}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        marginVertical: 5,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderColor: Colors.BORDERGREY,
      }}
    />
  );

  return (
    <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
      {/* Header Dimulai Di sini  */}
      <View
        style={{backgroundColor: Colors.PRIMARY, width: '100%', height: 160}}>
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{
            width: '100%',
            flexDirection: 'row',
            borderColor: Colors.BORDERGREY,
            paddingHorizontal: 15,
            paddingVertical: 15,
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 10,
              tintColor: Colors.WHITE,
            }}
            source={require('../../../asset/icon-back.png')}
          />
          <TextBold
            text={'Cek Harga'}
            color={Colors.WHITE}
            style={{fontSize: 20, marginHorizontal: 5}}
          />
        </TouchableOpacity>
        {/* Header Selesai Di sini  */}

        {/* Kolom Pencarian dimulai di sini */}
        <KolomPencarianComponent />
        {/* Kolom Pencarian selesai di sini */}
      </View>

      {/* Flat List Cek Harga  */}

      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: Colors.WHITE,
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row'}}>
          <TextRegular
            text={'Tanggal Cek harga:'}
            style={{fontWeight: '300', fontSize: 12}}
          />
          <TextRegular
            text={'    Sen, 17 Sep 2018 - 10:30'}
            style={{fontSize: 12}}
            color={Colors.DEEPBLUE}
          />
        </View>

        <TextBold
          text={'Avanza G AT 2016'}
          style={{marginTop: 5}}
          color={Colors.DEEPBLUE}
        />

        <Separator />

        <TextBold
          text={'Handoko'}
          size={12}
          style={{marginTop: 10}}
          color={Colors.DEEPBLUE}
        />
        <TextRegular
          text={'Cabang Kelapa Gading'}
          size={10}
          style={{fontWeight: '300'}}
        />
      </View>

      {/* Flat List Cek Harga  Selesai Di Sni */}

      <View style={{width: '100%', height: 50}} />
    </ScrollView>
  );
};

export default CekHargaComponent;
