import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import KolomPencarianComponent from './KolomPencarianComponent';

const TotalAppraisalComponent = ({navigation}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        marginVertical: 5,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderColor: Colors.BORDERGREY,
      }}
    />
  );

  return (
    // All
    <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
      {/* Header Dimulai Di sini  */}
      <View
        style={{backgroundColor: Colors.PRIMARY, width: '100%', height: 160}}>
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{
            width: '100%',
            flexDirection: 'row',
            borderColor: Colors.BORDERGREY,
            paddingHorizontal: 15,
            paddingVertical: 15,
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 10,
              tintColor: Colors.WHITE,
            }}
            source={require('../../../asset/icon-back.png')}
          />
          <TextBold
            text={'Total Appraisal'}
            color={Colors.WHITE}
            style={{fontSize: 20, marginHorizontal: 5}}
          />
        </TouchableOpacity>
        {/* Header Selesai Di sini  */}

        {/* Kolom Pencarian dimulai di sini */}
        <KolomPencarianComponent />
        {/* Kolom Pencarian selesai di sini */}
      </View>

      {/* Flat List Total Appraisal 1 Dimulai Dari Sini */}

      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: Colors.WHITE,
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'TR - 092018 - 246'}
            size={12}
            style={{fontWeight: '300', fontSize: 12}}
          />
          <TextRegular
            text={'Sen, 17 Sep 2018 - 10:30'}
            size={12}
            color={Colors.DARKBLUETOYOTA}
          />
        </View>

        <TextBold
          text={'Avanza G AT 2016'}
          size={14}
          color={Colors.DARKBLUETOYOTA}
          style={{marginTop: 5}}
        />

        <Separator />

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View>
            <TextBold
              text={'Handoko'}
              style={{marginTop: 10}}
              size={12}
              color={Colors.DARKBLUETOYOTA}
            />
            <TextRegular
              text={'Cabang Kelapa Gading'}
              size={10}
              style={{fontWeight: '300'}}
            />
          </View>

          <View
            style={{
              backgroundColor: Colors.PRIMARY,

              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 5,
              alignSelf: 'center',
            }}>
            <TextRegular text={'New Approval'} size={10} color={Colors.WHITE} />
          </View>
        </View>
      </View>

      {/* Flat List Total Appraisal 1 Selesai Dari Sini */}

      {/* Flat List Total Appraisal 2 Dimulai Dari Sini */}

      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: Colors.WHITE,
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'TR - 092018 - 246'}
            size={12}
            style={{fontWeight: '300', fontSize: 12}}
          />
          <TextRegular
            text={'Sen, 17 Sep 2018 - 10:30'}
            size={12}
            color={Colors.DARKBLUETOYOTA}
          />
        </View>

        <TextBold
          text={'Avanza G AT 2016'}
          size={14}
          color={Colors.DARKBLUETOYOTA}
          style={{marginTop: 5}}
        />

        <Separator />

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View>
            <TextBold
              text={'Burhan'}
              size={12}
              style={{marginTop: 10}}
              color={Colors.DARKBLUETOYOTA}
            />
            <TextRegular
              text={'Cabang Kelapa Gading'}
              size={10}
              style={{fontWeight: '300'}}
            />
          </View>

          <View
            style={{
              backgroundColor: Colors.PRIMARY,
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 5,
              alignSelf: 'center',
            }}>
            <TextRegular text={'New Approval'} size={10} color={Colors.WHITE} />
          </View>
        </View>
      </View>
      {/* Flat List Total Appraisal 2 Selesai Dari Sini */}

      {/* Flat List Total Appraisal 3 Dimulai Dari Sini */}

      <View
        style={{
          marginTop: 10,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: Colors.WHITE,
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
          borderRadius: 10,
          padding: 15,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextRegular
            text={'TR - 092018 - 246'}
            size={12}
            style={{fontWeight: '300', fontSize: 12}}
          />
          <TextRegular
            text={'Sen, 17 Sep 2018 - 10:30'}
            size={12}
            color={Colors.DARKBLUETOYOTA}
          />
        </View>

        <TextBold
          text={'Avanza G AT 2016'}
          size={14}
          color={Colors.DARKBLUETOYOTA}
          style={{marginTop: 5}}
        />

        <Separator />

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View>
            <TextBold
              text={'Reyfan'}
              size={12}
              style={{marginTop: 10}}
              color={Colors.DARKBLUETOYOTA}
            />
            <TextRegular
              text={'Cabang Kelapa Gading'}
              size={10}
              style={{fontWeight: '300'}}
            />
          </View>

          <View
            style={{
              backgroundColor: Colors.PRIMARY,
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 5,
              alignSelf: 'center',
            }}>
            <TextRegular text={'New Approval'} size={10} color={Colors.WHITE} />
          </View>
        </View>
      </View>
      {/* Flat List Total Appraisal 3 Selesai Dari Sini */}

      {/* Isian View, biar scrollView/flatList bisa kelihatan semuanya */}
      <View style={{width: '100%', height: 50}} />
    </ScrollView>
  );
};

export default TotalAppraisalComponent;
