import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium, InputText} from '../../global';

import ModalChangeCar from '../../modal/Home/ModalChangeCar';
import ModalChangeDiscount from '../../modal/Home/ModalChangeDiscount';
import ModalRequestDiscountBOD from '../../modal/Home/ModalRequestDiscountBOD';
import ModalNoDeal from '../../modal/Home/ModalNoDeal';
import ModalKonfirmasiDeal from '../../modal/Home/ModalKonfirmasiDeal';
import ModalRequestMRP from '../../modal/Home/ModalRequestMRP';
import moment from 'moment';
import 'moment/locale/id';
import {convertCurrency} from '../../../utils/helpers/ConvertCurrentcy';
const ToolTradeInComponent = ({
  navigation,
  item,
  listCar,
  onChange,
  setIdNewCar,
}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  const [ModalChangeCarVisible, setModalChangeCarVisible] = useState(false);
  const [ModalChangeDiscountVisible, setModalChangeDiscountVisible] =
    useState(false);
  const [modalRequestDiscountBODVisible, setModalRequestDiscountBODVisible] =
    useState(false);
  const [modalRequestMRP, setModalRequestMRP] = useState(false);
  const [modalNoDealVisible, setModalNoDealVisible] = useState(false);
  const [ModalKonfirmasiDealVisible, setModalKonfirmasiDealVisible] =
    useState(false);
  console.log('ini item nya detail', item);
  const data = item;
  const dataPenawaran = data.calculation[1].price;
  const dataNego = data.calculation[1].price;
  const dataPenawaran1 =
    dataPenawaran !== 'N/A' ? parseFloat(dataPenawaran) : 0;
  const dataNego1 = dataNego !== 'N/A' ? parseFloat(dataNego) : 0;
  const MRP = convertCurrency(dataPenawaran1 + dataNego1);
  console.log('ini MRP', dataNego);
  return (
    // All
    <View style={{flex: 1}}>
      {/* Header Dimulai Di sini  */}
      <View style={{width: '100%', height: 60, backgroundColor: Colors.WHITE}}>
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{
            width: '100%',
            flexDirection: 'row',
            borderColor: Colors.BORDERGREY,
            padding: 15,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Image
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 10,
            }}
            source={require('../../../asset/icon-back.png')}
          />
          <TextBold
            text={'Tool Trade In'}
            color={Colors.BLACK}
            style={{fontSize: 20}}
          />
          <Image
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 10,
              tintColor: Colors.WHITE,
            }}
            source={require('../../../asset/icon-back.png')}
          />
        </TouchableOpacity>
        {/* Header Selesai Di sini  */}
      </View>
      {/* Kode TR & Hari, Tanggal */}
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          backgroundColor: Colors.WHITE,
          marginVertical: 5,
          padding: 10,
          alignContent: 'center',
          justifyContent: 'space-between',
        }}>
        <TextRegular
          text={'TR-092018-246'}
          size={12}
          style={{fontWeight: '300'}}
        />

        <TextRegular
          // text={'Sen, 17 Sep 2018 - 10:30'}
          text={moment(item?.approvalTradeIn?.createdAt).format(
            'ddd, DD-MM-YYYY HH:mm',
          )}
          size={12}
          color={Colors.DARKBLUETOYOTA}
        />
      </View>
      {/* Kode TR & Hari, Tanggal Selesai di Sini*/}

      <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
        {/* Used Car Information Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.BORDERGREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'Used Car Information'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <Separator />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Mobil'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                item?.approvalTradeIn?.NewCar?.carName
                  ? item?.approvalTradeIn?.NewCar?.carName
                  : 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Plat Nomor'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                item?.approvalTradeIn?.Appraisal?.carDetail[7]?.value ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <TouchableOpacity
            style={{
              marginTop: 20,
              width: '100%',
              paddingVertical: 10,
              borderRadius: 6,
              backgroundColor: Colors.PRIMARY,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
            onPress={() => navigation.navigate('Summary', (item = {item}))}>
            <TextRegular
              text={'Lihat Detail Appraisal'}
              size={14}
              color={Colors.WHITE}
              style={{
                marginHorizontal: 10,
              }}
            />

            <Image
              style={{
                width: 20,
                height: 20,
                tintColor: Colors.WHITE,
                marginHorizontal: 10,
              }}
              source={require('../../../asset/icon-next.png')}
            />
          </TouchableOpacity>
        </View>
        {/* Used Car Information Selesai di Sini  */}

        {/* New Car Information Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.BORDERGREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'New Car Information'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <Separator />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Mobil'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <View>
              <TextBold
                text={item?.approvalTradeIn?.NewCar?.carName ?? 'N/A'}
                size={14}
                color={Colors.DEEPBLUE}
                style={{
                  marginVertical: 5,
                }}
              />

              <TouchableOpacity
                onPress={() => setModalChangeCarVisible(true)}
                style={{
                  alignSelf: 'flex-end',
                }}>
                <TextRegular
                  text={'Change Car'}
                  size={14}
                  color={Colors.PRIMARY}
                  style={{
                    fontStyle: 'italic',
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'OTR'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              // text={'Rp 276.600.000'}
              text={
                'Rp ' + convertCurrency(item?.approvalTradeIn?.NewCar?.otr)
                  ? 'Rp ' + convertCurrency(item?.approvalTradeIn?.NewCar?.otr)
                  : 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
        </View>
        {/* New Car Information Selesai di Sini  */}

        {/* Ruang Nego Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.BORDERGREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'Ruang Nego Harga'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <Separator />

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Harga Penawaran'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(data?.calculation[1]?.price) ?? 'N/A'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Ruang Nego'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={convertCurrency(data?.calculation[6]?.price) ?? 'N/A'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'MRP (Harga Max)'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={'Rp. ' + MRP}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
        </View>
        {/* Ruang Nego Selesai di Sini  */}

        {/* Diskon Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.BORDERGREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'Diskon'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <Separator />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Diskon Kacab'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
            <View>
              <TextBold
                text={
                  'Rp. ' + convertCurrency(data?.calculation[6]?.price) ?? 'N/A'
                }
                size={14}
                color={Colors.DEEPBLUE}
                style={{
                  marginVertical: 5,
                  alignSelf: 'flex-end',
                }}
              />
              <TouchableOpacity
                onPress={() => setModalChangeDiscountVisible(true)}
                style={{
                  alignSelf: 'flex-end',
                }}>
                <TextRegular
                  text={'Change Discount'}
                  size={14}
                  color={Colors.PRIMARY}
                  style={{
                    fontStyle: 'italic',
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Pengajuan Diskon BOD'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                'Rp. ' + convertCurrency(data?.calculation[7]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
                alignSelf: 'flex-end',
              }}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Proyeksi Gross Profit'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                'Rp. ' + convertCurrency(data?.calculation[10]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
                alignSelf: 'flex-end',
              }}
            />
          </View>
        </View>
        {/* Diskon Selesai di Sini  */}

        {/* Gross Profit Trust Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.BORDERGREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'Gross Profit Trust'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <Separator />

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Proyeksi Harga Jual'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                'Rp. ' + convertCurrency(data?.calculation[2]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'COGS (harga beli + biaya rekondisi) + OPEX'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
                width: 200,
              }}
            />

            <TextBold
              text={
                'Rp. ' + convertCurrency(data?.calculation[3]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Proyeksi NPBT Trust'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                'Rp. ' + convertCurrency(data?.calculation[4]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Total Benefit TI'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              text={
                'Rp. ' + convertCurrency(data?.calculation[12]?.price) ?? 'N/A'
              }
              size={14}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
        </View>
        {/* Gross Profit Trust Selesai di Sini  */}

        {/* Request Section Dimulai dari Sini  */}
        <TextBold
          text={'Request Section'}
          style={{
            textAlign: 'center',
            fontWeight: 'bold',
            fontSize: 16,
            marginVertical: 20,
          }}
        />

        {/* Separator Tengah Dimulai Di Sini*/}
        <View
          style={{
            width: '90%',
            height: 1,
            backgroundColor: Colors.BORDERGREY,
            marginVertical: 5,
            alignSelf: 'center',
          }}
        />
        {/* Separator Tengah Selesai di Sini */}
        {/* Request Section Selesai dari Sini  */}

        {/* Request Discount BOD Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.BORDERGREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'Request Discount BOD'}
            style={{marginVertical: 5, fontSize: 16}}
            color={Colors.DEEPBLUE}
          />

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => setModalRequestDiscountBODVisible(true)}
              style={{
                marginTop: 20,
                width: '100%',
                paddingVertical: 10,
                borderRadius: 6,
                backgroundColor: Colors.PRIMARY,
                alignItems: 'center',
              }}>
              <TextRegular text={'REQUEST NOW'} color={Colors.WHITE} />
            </TouchableOpacity>
          </View>
        </View>
        {/* Request Discount BOD Selesai di sini*/}

        {/* Request MRP Dimulai di sini*/}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',
            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.GREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextBold
            text={'Request MRP'}
            style={{marginVertical: 5, fontSize: 16}}
            color={Colors.DEEPBLUE}
          />

          <TouchableOpacity onPress={() => setModalRequestMRP(true)}>
            <View style={{flexDirection: 'row'}}>
              <View
                style={{
                  marginTop: 20,
                  width: '100%',
                  paddingVertical: 10,
                  borderRadius: 6,
                  backgroundColor: Colors.BORDERGREY,
                  alignItems: 'center',
                }}>
                <TextRegular text={'REQUEST NOW'} color={Colors.DEEPBLUE} />
              </View>
            </View>
          </TouchableOpacity>
        </View>
        {/* Request MRP Selesai di sini*/}

        {/* Harga Final Dimulai Di Sini */}

        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '90%',

            backgroundColor: Colors.WHITE,
            borderWidth: 1,
            borderColor: Colors.GREY,
            borderRadius: 10,
            padding: 15,
          }}>
          <TextRegular
            text={'Harga Final'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            placeholderText="1231321313"
            value={'Rp. Masukan Nominal Harga Final'}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />

          <TextRegular
            text={'Sisa Ruang Nego'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <View
            style={{
              backgroundColor: Colors.GREY,
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 5,
              padding: 10,
              marginVertical: 5,
            }}>
            <TextRegular text={'Rp 2.000.000'} />
          </View>

          <TextRegular
            text={'Total Benefit TI (Final)'}
            style={{marginVertical: 5, fontSize: 14}}
            color={Colors.DEEPBLUE}
          />

          <View
            style={{
              backgroundColor: Colors.GREY,
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              borderRadius: 5,
              padding: 10,
              marginVertical: 5,
            }}>
            <TextRegular text={'Rp 2.000.000'} />
          </View>
        </View>

        {/* Harga Final Selesai Di Sini */}

        {/* No Deal dan Deal */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '90%',
            alignSelf: 'center',
          }}>
          <TouchableOpacity
            onPress={() => setModalNoDealVisible(true)}
            style={{
              marginVertical: 10,
              paddingVertical: 10,
              width: '48%',
              backgroundColor: Colors.BORDERGREY,
              alignItems: 'center',
              borderRadius: 10,
            }}>
            <TextRegular text={'NO DEAL'} color={Colors.DEEPBLUE} />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => setModalKonfirmasiDealVisible(true)}
            style={{
              marginVertical: 10,
              paddingVertical: 10,
              width: '48%',
              backgroundColor: Colors.PRIMARY,
              alignItems: 'center',
              borderRadius: 10,
            }}>
            <TextRegular text={'DEAL'} color={Colors.WHITE} />
          </TouchableOpacity>

          {/* Modal Mulai Dari Sini  */}
          {/* Modal Change Car Mulai Dari Sini  */}
          <Modal
            animationType="fade"
            transparent={true}
            visible={ModalChangeCarVisible}
            onRequestClose={() => {
              setModalChangeCarVisible(false);
            }}>
            <ModalChangeCar
              onRequestClose={() => {
                setModalChangeCarVisible(false);
              }}
              listCar={listCar}
              onChange={onChange}
              setIdNewCar={setIdNewCar}
            />
          </Modal>
          {/* Modal Change Car Selesai di Sini  */}

          {/* Modal Change Discount Mulai Dari Sini  */}
          <Modal
            animationType="fade"
            transparent={true}
            visible={ModalChangeDiscountVisible}
            onRequestClose={() => {
              setModalChangeDiscountVisible(false);
            }}>
            <ModalChangeDiscount
              onRequestClose={() => {
                setModalChangeDiscountVisible(false);
              }}
            />
          </Modal>
          {/* Modal Change Discount Selesai di Sini  */}

          {/* Modal Request Discount BOD Mulai Dari Sini  */}
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalRequestDiscountBODVisible}
            onRequestClose={() => {
              setModalRequestDiscountBODVisible(false);
            }}>
            <ModalRequestDiscountBOD
              onRequestClose={() => {
                setModalRequestDiscountBODVisible(false);
              }}
            />
          </Modal>
          {/* Modal Request Discount BOD Selesai di Sini  */}

          {/* Modal Request MRP Mulai Dari Sini  */}
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalRequestMRP}
            onRequestClose={() => {
              setModalRequestMRP(false);
            }}>
            <ModalRequestMRP
              onRequestClose={() => {
                setModalRequestMRP(false);
              }}
            />
          </Modal>
          {/* Modal Request MRP Selesai di Sini  */}

          {/* Modal Request No Deal Mulai Dari Sini  */}
          <Modal
            animationType='"fade"'
            transparent={true}
            visible={modalNoDealVisible}
            onRequestClose={() => {
              setModalNoDealVisible(false);
            }}>
            <ModalNoDeal
              onRequestClose={() => {
                setModalNoDealVisible(false);
              }}
            />
          </Modal>
          {/* Modal Request No Deal Selesai di Sini  */}

          {/* Modal Request Konfirmasi Deal Mulai Dari Sini  */}
          <Modal
            animationType='"fade"'
            transparent={true}
            visible={ModalKonfirmasiDealVisible}
            onRequestClose={() => {
              setModalKonfirmasiDealVisible(false);
            }}>
            <ModalKonfirmasiDeal
              onRequestClose={() => {
                setModalKonfirmasiDealVisible(false);
              }}
            />
          </Modal>
          {/* Modal Request Konfirmasi Deal Selesai di Sini  */}
          {/* Modal Selesai Dari Sini  */}
        </View>

        {/* Isian View, biar scrollView/flatList bisa kelihatan semuanya */}
        <View style={{width: '100%', height: 50}} />
      </ScrollView>
    </View>
  );
};

export default ToolTradeInComponent;
