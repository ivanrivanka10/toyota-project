import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import ModalRequestDiscount from '../../modal/Home/ModalRequestDiscount';
import ModalNoDeal from '../../modal/Home/ModalNoDeal';
import ModalHargaFinal from '../../modal/Home/ModalHargaFinal';
import moment from 'moment';
import 'moment/locale/id';

const BeliMobilComponent = ({navigation, item}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  const [modalRequestDiscountVisible, setModalRequestDiscountVisible] =
    useState(false);
  const [modalNoDealVisible, setModalNoDealVisible] = useState(false);
  const [modalHargaFinalVisible, setModalHargaFinalVisible] = useState(false);
  // item.item.createdAt = 2021-01-18T09:37:53.207Z
  // const originalDate = item.item.createdAt;
  // const formattedDate = moment(originalDate).format('YYYY-MM-DD HH:mm');

  return (
    // All
    <View style={{flex: 1}}>
      <ScrollView style={{flex: 1, backgroundColor: Colors.GREY}}>
        {/* Header */}
        <View
          style={{
            width: '100%',
            height: 60,
            backgroundColor: Colors.WHITE,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal: 15,
          }}>
          <TouchableOpacity
            onPress={() => navigation.pop()}
            style={{
              flexDirection: 'row',
              borderColor: Colors.BORDERGREY,
              alignItems: 'center',
              paddingVertical: 10,
            }}>
            <Image
              style={{
                width: 25,
                height: 25,
              }}
              source={require('../../../asset/icon-back.png')}
            />
          </TouchableOpacity>

          <TextBold
            text={'Beli Mobil'}
            size={16}
            color={Colors.BLACK}
            style={{paddingVertical: 15}}
          />

          <TouchableOpacity
            style={{
              paddingVertical: 15,
            }}>
            <Image
              style={{
                width: 25,
                height: 25,
                tintColor: Colors.BLACK,
              }}
              source={require('../../../asset/icon-share.png')}
            />
          </TouchableOpacity>
        </View>
        {/* Header Selesai Di Sini  */}

        {/* Kode TR & Hari, Tanggal */}
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            backgroundColor: Colors.WHITE,
            marginTop: 5,
            padding: 10,
            alignContent: 'center',
            justifyContent: 'space-between',
            paddingVertical: 10,
          }}>
          <TextRegular
            // 'TR-092018-246'
            text={item?.approvalNewCar?.noNewCar}
            // text={'TR-092018-246'}
          />

          <TextRegular
            // 'Sen, 17 Sep 2018 - 10:30'
            // text={formattedDate}
            // text={'Sen, 17 Sep 2018 - 10:30'}
            text={moment(item?.approvalNewCar?.createdAt).format(
              'ddd, DD-MM-YYYY HH:mm',
            )}
            color={Colors.DEEPBLUE}
          />
          {/* 
          <TextRegular
            // 'Sen, 17 Sep 2018 - 10:30'
            text={formattedDate}
            color={Colors.DEEPBLUE}
          /> */}
        </View>
        {/* Kode TR & Hari, Tanggal Selesai di Sini*/}

        {/* Nama Customer di dari sini */}
        <View
          style={{
            marginTop: 1,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: Colors.WHITE,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Nama Customer'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              // 'NANDO DWIKI SATRIA'
              // item.item.SalesBranch.branch
              // text={'N/A'}
              text={item?.approvalNewCar?.customerName}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'No. HP'}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />

            <TextBold
              // '082245884655'
              // text={'N/A'}
              text={item?.approvalNewCar?.phone}
              color={Colors.DEEPBLUE}
              style={{
                marginVertical: 5,
              }}
            />
          </View>
        </View>
        {/* Nama Customer Selesai di Sini  */}

        {/* Detail Mobil di dari sini */}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: Colors.WHITE,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <TextBold
            text={'Detail Mobil'}
            color={Colors.BLACK}
            style={{
              marginVertical: 5,
            }}
          />

          <Separator />

          <View style={{marginVertical: 10}}>
            <TextRegular
              text={'New Car'}
              style={{
                marginVertical: 10,
              }}
            />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextBold
                // 'Rush S AT TRD'
                // text={item.item.NewCar.carName}
                // text={'Rush S AT TRD'}
                text={item?.approvalNewCar?.NewCar?.carName}
                color={Colors.DEEPBLUE}
                style={{
                  marginVertical: 5,
                }}
              />

              <TextBold
                // 'Rp. 276.600.000'
                // text={'N/A'}
                text={
                  'Rp ' +
                  Number(item?.approvalNewCar?.NewCar?.otr)
                    .toLocaleString('id-ID')
                    .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
                }
                color={Colors.DEEPBLUE}
                style={{
                  marginVertical: 5,
                }}
              />
            </View>
          </View>
        </View>
        {/*Detail Mobil Selesai di Sini  */}

        {/* Request Discount di dari sini */}
        <View
          style={{
            marginTop: 10,
            alignSelf: 'center',
            width: '100%',
            backgroundColor: Colors.WHITE,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <TextBold
            text={'Diskon'}
            color={Colors.DEEPBLUE}
            style={{
              marginVertical: 10,
            }}
          />

          <Separator />

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                marginVertical: 10,
                width: '100%',
                paddingVertical: 10,
                borderRadius: 6,
                backgroundColor: Colors.PRIMARY,
                flexDirection: 'row',
              }}
              onPress={() => setModalRequestDiscountVisible(true)}>
              <TextRegular
                text={'REQUEST DISCOUNT'}
                color={Colors.WHITE}
                style={{marginHorizontal: 10, marginVertical: 3}}
              />

              <Image
                style={{
                  width: 20,
                  height: 20,
                  position: 'absolute',
                  right: 0,
                  marginVertical: 10,
                  marginHorizontal: 10,
                  tintColor: Colors.WHITE,
                }}
                source={require('../../../asset/icon-next.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        {/*Request Discount Selesai di Sini  */}
      </ScrollView>

      {/* No Deal dan Deal */}
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: Colors.WHITE,
          width: '100%',
          justifyContent: 'space-between',
          bottom: 0,
          alignSelf: 'center',
          paddingHorizontal: 15,
        }}>
        <TouchableOpacity
          onPress={() => setModalNoDealVisible(true)}
          style={{
            marginVertical: 10,
            paddingVertical: 10,
            width: '42%',
            backgroundColor: Colors.BORDERGREY,
            alignItems: 'center',
            borderRadius: 10,
            marginRight: 10,
          }}>
          <TextRegular text={'NO DEAL'} color={Colors.BLACK} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => setModalHargaFinalVisible(true)}
          style={{
            marginVertical: 10,
            paddingVertical: 10,
            width: '42%',
            backgroundColor: Colors.PRIMARY,
            alignItems: 'center',
            borderRadius: 10,
            marginRight: 10,
          }}>
          <TextRegular text={'DEAL'} color={Colors.WHITE} />
        </TouchableOpacity>
      </View>
      {/* No Deal dan Deal Selesai di sini*/}

      {/* Modal Mulai Dari Sini  */}
      {/* Modal Request Discount Mulai Dari Sini  */}

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalRequestDiscountVisible}
        onRequestClose={() => {
          setModalRequestDiscountVisible(false);
        }}>
        <ModalRequestDiscount
          onRequestClose={() => {
            setModalRequestDiscountVisible(false);
          }}
        />
      </Modal>
      {/* Modal Request Discount Selesai Di Sini  */}

      {/* Modal Request No Deal Mulai Dari Sini  */}
      <Modal
        animationType='"fade"'
        transparent={true}
        visible={modalNoDealVisible}
        onRequestClose={() => {
          setModalNoDealVisible(false);
        }}>
        <ModalNoDeal
          onRequestClose={() => {
            setModalNoDealVisible(false);
          }}
        />
      </Modal>

      {/* Modal konfirmasi Deal Selesai di Sini  */}

      {/* Modal Request No Deal Mulai Dari Sini  */}
      <Modal
        animationType='"fade"'
        transparent={true}
        visible={modalHargaFinalVisible}
        onRequestClose={() => {
          setModalHargaFinalVisible(false);
        }}>
        <ModalHargaFinal
          onRequestClose={() => {
            setModalHargaFinalVisible(false);
          }}
        />
      </Modal>
      {/* Modal Konfirmasi Deal Selesai di Sini  */}

      {/* Modal Selesai Di Sini  */}
    </View>
  );
};

export default BeliMobilComponent;
