import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';

import {Colors} from '../../../styles';
import {Header, InputText, TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Entypo';

const EditDataRekeningComponent = ({
  navigation,
  item,
  bank,
  name,
  noRekening,
  setBank,
  setName,
  setNoRekening,
  onSave,
}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
      }}>
      <Header
        title="Edit Data Rekening"
        titleColor={Colors.DEEPBLUE}
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />
      <ScrollView style={{paddingHorizontal: 16}}>
        <View
          style={{
            paddingHorizontal: 12,
            backgroundColor: Colors.WHITE,
            width: '100%',

            marginTop: 16,
            borderRadius: 5,
          }}>
          <TextBold
            size={14}
            text={'Data Rekening'}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 10}}
          />
          <View
            style={{
              height: 3,
              marginHorizontal: -16,
              backgroundColor: Colors.GREY,
            }}></View>
          <TextRegular
            text={'Bank'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            onChangeText={text => setBank(text)}
            value={bank}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          <TextRegular
            text={'Nama Pemilik Rekening'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            onChangeText={text => setName(text)}
            value={name}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          <TextRegular
            text={'No. Rekening'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            onChangeText={text => setNoRekening(text)}
            value={noRekening}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE, marginBottom: 16}}
          />
        </View>
      </ScrollView>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: Colors.WHITE,
          paddingHorizontal: 16,
          alignContent: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            const params = {
              bankName: bank,
              bankBeneficiary: name,
              bankAccNo: noRekening,
            };
            onSave(params);
          }}
          style={{
            width: '100%',
            height: 36,
            backgroundColor: Colors.PRIMARY,
            borderRadius: 5,
          }}>
          <View
            style={{
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              height: '100%',
            }}>
            <TextBold text={'SIMPAN'} color={Colors.WHITE} size={14} />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default EditDataRekeningComponent;
