import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';

const ProfilComponent = ({navigation, item}) => {
  const dispatch = useDispatch();
  const {loginData} = useSelector(state => state.login);
  return (
    <ScrollView
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
      }}>
      <ImageBackground
        source={require('../../../asset/backgroundprofil.png')}
        style={{
          width: '100%',
          height: 142,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            dispatch({type: 'LOGOUT'});
            navigation.replace('AuthNavigation');
          }}
          style={{alignSelf: 'flex-end', marginRight: 24, marginBottom: -22}}>
          <Icon name="logout" size={22} color={Colors.WHITE} />
        </TouchableOpacity>
        <Image
          source={{
            uri: item?.profile?.ktp ?? '-',
          }}
          style={{
            width: 64,
            height: 64,
            borderRadius: 64,
            borderWidth: 1,
            borderColor: Colors.WHITE,
          }}
        />
        <TextRegular
          // text={'Setiawan'}
          text={item?.user?.name ?? '-'}
          color={Colors.WHITE}
          size={20}
          style={{marginTop: 12}}
        />
      </ImageBackground>
      <View
        style={{
          padding: 16,
          backgroundColor: Colors.GREY,
        }}>
        <View style={styles.cardprofile}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextBold text={'Data Diri'} color={Colors.DEEPBLUE} />
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('EditDataDiri', (item = {item}))
              }>
              <TextRegular text={'Edit Data Diri'} color={Colors.PRIMARY} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginVertical: 12,
              height: 3,
              backgroundColor: Colors.GREY,
              marginHorizontal: -12,
            }}></View>
          <TextRegular
            text={'Nama Kepala cabang'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.user?.name ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'Cabang'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.profile?.SalesBranch?.branch ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'NRP'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.user?.nrp ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'No. Telepon'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.user?.phone ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'Tanggal Lahir'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={moment(item.profile.dateOfBirth).format('DD-MM-YYYY')}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
        </View>

        <View style={styles.cardprofile}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextBold text={'Data Rekening'} color={Colors.DEEPBLUE} />
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('EditDataRekening', (item = {item}))
              }>
              <TextRegular text={'Edit Data Rekening'} color={Colors.PRIMARY} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginVertical: 12,
              height: 3,
              backgroundColor: Colors.GREY,
              marginHorizontal: -12,
            }}></View>
          <TextRegular
            text={'Bank'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.profile?.bankName ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'nama Pemilik Rekening'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.profile?.bankBeneficiary ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'No. Rekening'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.profile?.bankAccNo ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
        </View>
        <View style={styles.cardprofile}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextBold text={'Dokumen'} color={Colors.DEEPBLUE} />
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('EditDokumen', (item = {item}))
              }>
              <TextRegular text={'Edit Dokumen'} color={Colors.PRIMARY} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginVertical: 12,
              height: 3,
              backgroundColor: Colors.GREY,
              marginHorizontal: -12,
            }}></View>
          <TextRegular
            text={'No. KTP'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.profile?.noKTP ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'Foto KTP'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <Image
            style={{width: 214, height: 128, borderRadius: 10, marginTop: 4}}
            source={{
              uri: item.profile.ktp,
            }}
          />
          <TextRegular
            text={'No. NPWP'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <TextBold
            text={item?.profile?.noNPWP ?? '-'}
            color={Colors.DEEPBLUE}
            style={{marginTop: 4}}
          />
          <TextRegular
            text={'Foto NPWP'}
            size={12}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10}}
          />
          <Image
            style={{
              width: 214,
              height: 128,
              borderRadius: 10,
              marginTop: 4,
            }}
            source={{
              uri: item.profile.npwp,
            }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default ProfilComponent;
