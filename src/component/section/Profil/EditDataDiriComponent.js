import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';

import {Colors} from '../../../styles';
import {Header, InputText, TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Entypo';
import moment from 'moment';
import 'moment/locale/id';

const EditDataDiriComponent = ({
  navigation,
  item,
  birth,
  nrp,
  namaKacab,
  cabang,
  phone,
  setBirth,
  setPhone,
  setCabang,
  setNamaKacab,
  setNrp,
  onSave,
}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
      }}>
      <Header
        title="Edit Data Diri"
        titleColor={Colors.DEEPBLUE}
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />
      <ScrollView style={{paddingHorizontal: 16}}>
        <View
          style={{
            paddingHorizontal: 12,
            backgroundColor: Colors.WHITE,
            width: '100%',

            marginTop: 16,
            borderRadius: 5,
          }}>
          <TextBold
            size={14}
            text={'Data Diri'}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 10}}
          />

          <View
            style={{
              height: 3,
              marginHorizontal: -16,
              backgroundColor: Colors.GREY,
            }}></View>
          <TextRegular
            text={'Nama Kepala Cabang'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            value={namaKacab}
            onChangeText={text => setNamaKacab(text)}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />

          <TextRegular
            text={'Cabang'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <View style={styles.container}>
            <TextRegular text={cabang} />
          </View>
          <TextRegular
            text={'NRP'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <View style={styles.container}>
            <TextRegular text={nrp} />
          </View>
          <TextRegular
            text={'No. Telepon'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            onChangeText={text => setPhone(text)}
            value={phone}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          <TextRegular
            text={'Tanggal Lahir'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <View
            style={{
              width: '100%',
              height: 50,
              marginTop: 4,
              borderRadius: 6,
              borderWidth: 1,
              borderColor: '#dedede',
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 10,
              marginBottom: 16,
            }}>
            <TextRegular
              text={moment(birth).format('DD-MM-YYYY')}
              color={Colors.DEEPBLUE}
            />
            <TouchableOpacity>
              <Icon name="calendar" color={Colors.DEEPBLUE} size={20} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: Colors.WHITE,
          paddingHorizontal: 16,
          alignContent: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            const params = {
              name: namaKacab,
              phone: phone,
            };
            onSave(params);
          }}
          style={{
            width: '100%',
            height: 36,
            backgroundColor: Colors.PRIMARY,
            borderRadius: 5,
          }}>
          <View
            style={{
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              height: '100%',
            }}>
            <TextBold text={'SIMPAN'} color={Colors.WHITE} size={14} />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  container: {
    width: '100%',
    borderColor: '#dedede',
    borderWidth: 1,
    borderRadius: 6,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 5,
    backgroundColor: '#dedede',
    marginTop: 16,
  },
});

export default EditDataDiriComponent;
