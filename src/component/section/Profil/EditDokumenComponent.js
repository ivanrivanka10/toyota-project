import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';

import {Colors} from '../../../styles';
import {Header, InputText, TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Entypo';
import moment from 'moment';
import 'moment/locale/id';
const EditDokumenComponent = ({
  navigation,
  npwp,
  noNpwp,
  ktp,
  noktp,
  setKtp,
  setNoKtp,
  setNoNpwp,
  setNpwp,
  onSave,
}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
      }}>
      <Header
        title="Edit Dokumen"
        titleColor={Colors.DEEPBLUE}
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />
      <ScrollView style={{paddingHorizontal: 16}}>
        <View
          style={{
            paddingHorizontal: 12,
            backgroundColor: Colors.WHITE,
            width: '100%',

            marginTop: 16,
            borderRadius: 5,
          }}>
          <TextBold
            size={14}
            text={'Data Dokumen'}
            color={Colors.DEEPBLUE}
            style={{marginVertical: 10}}
          />
          <View
            style={{
              height: 3,
              marginHorizontal: -16,
              backgroundColor: Colors.GREY,
            }}></View>
          <TextRegular
            text={'No. KTP'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            onChangeText={text => setNoKtp(text)}
            value={noktp}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          <TextRegular
            text={'Foto KTP'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <View style={styles.inputimage}>
            <Image
              source={{
                uri: ktp,
              }}
              style={styles.image}
            />
            <TouchableOpacity style={styles.button}>
              <TextBold text={'UNGGAH KTP'} color={Colors.WHITE} size={14} />
            </TouchableOpacity>
          </View>
          <TextRegular
            text={'No. NPWP'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <InputText
            onChangeText={text => setNoNpwp(text)}
            value={noNpwp}
            placeholderTextColor={Colors.DEEPBLUE}
            style={{marginTop: 4, color: Colors.DEEPBLUE}}
          />
          <TextRegular
            text={'Foto NPWP'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <View style={styles.inputimage}>
            <Image source={{uri: npwp}} style={styles.image} />
            <TouchableOpacity style={styles.button}>
              <TextBold text={'UNGGAH KTP'} color={Colors.WHITE} size={14} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: Colors.WHITE,
          paddingHorizontal: 16,
          alignContent: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            const params = {
              noKTP: noktp,
              ktp: ktp,
              noNPWP: noNpwp,
              npwp: npwp,
            };
            onSave(params);
          }}>
          <TextBold text={'SIMPAN'} color={Colors.WHITE} size={14} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  button: {
    width: '100%',
    height: 36,
    backgroundColor: Colors.PRIMARY,
    borderRadius: 5,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputimage: {
    width: '100%',
    paddingVertical: 8,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 4,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#dedede',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginBottom: 16,
  },
  image: {
    width: 214,
    height: 128,
    borderRadius: 6,
    marginBottom: 12,
  },
});

export default EditDokumenComponent;
