import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';

import {Colors} from '../../../styles';
import {Header, InputText, TextBold, TextRegular} from '../../global';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';

const DetailInsentifComponent = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
      }}>
      <Header
        title="Detail Insentif"
        titleColor={Colors.DEEPBLUE}
        titleCenter={true}
        onPressBack={() => navigation.pop()}
        titleSize={16}
        isBoldTitle={true}
      />
      <ScrollView style={{paddingHorizontal: 16}}>
        <View style={styles.cardbox}>
          <TextRegular text={'TR-1234534-123'} size={12} />
          <View style={styles.row}>
            <TextRegular
              text={'Sen, 17 Sep 2018 - 10:30'}
              size={12}
              color={Colors.DEEPBLUE}
              style={{marginTop: 8, alignSelf: 'flex-end'}}
            />
            <TouchableOpacity>
              <View
                style={{
                  backgroundColor: Colors.PRIMARY,
                  padding: 4,
                  borderRadius: 4,
                }}>
                <TextRegular text={'CREATE'} size={10} color={Colors.WHITE} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.row, {marginTop: 15}]}>
            <TextRegular
              text={'Nilai Insentif'}
              size={14}
              color={Colors.DEEPBLUE}
            />
            <TextBold
              text={'Rp. 1.0000.000'}
              size={14}
              color={Colors.DEEPBLUE}
            />
          </View>
          <View
            style={{
              borderWidth: 1,
              borderStyle: 'dashed',
              borderColor: '#CCD3DD',
              marginTop: 10,
              marginBottom: 9,
            }}></View>
          <View style={[styles.row]}>
            <TextRegular
              text={'Nama Salesman'}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextBold text={'Kukuh Hadi'} size={12} color={Colors.DEEPBLUE} />
          </View>
          <View style={[styles.row]}>
            <TextRegular
              text={'Cabang Auto2000'}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextBold text={'Ciledug'} size={12} color={Colors.DEEPBLUE} />
          </View>
          <View style={[styles.row]}>
            <TextRegular
              text={'Tipe Mobil'}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextBold text={'Avanza'} size={12} color={Colors.DEEPBLUE} />
          </View>
          <View style={[styles.row]}>
            <TextRegular
              text={'Nomor Polisi'}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextBold text={'AB1234CD'} size={12} color={Colors.DEEPBLUE} />
          </View>
          <View style={[styles.row]}>
            <TextRegular
              text={'Nama Customer'}
              size={12}
              color={Colors.DEEPBLUE}
            />
            <TextBold
              text={'Aji Bayu Wirrotama'}
              size={12}
              color={Colors.DEEPBLUE}
            />
          </View>
        </View>

        <View
          style={{
            paddingHorizontal: 12,
            paddingVertical: 12,
            backgroundColor: Colors.WHITE,
            width: '100%',

            marginTop: 16,
            borderRadius: 5,
          }}>
          <TextBold
            size={14}
            text={'Riwayat'}
            color={Colors.DEEPBLUE}
            style={{marginBottom: 10}}
          />
          <View
            style={{
              height: 3,
              marginHorizontal: -16,
              backgroundColor: Colors.GREY,
            }}></View>

          <TextRegular
            text={'Tanggal Lahir'}
            style={{marginTop: 16}}
            color={Colors.DEEPBLUE}
          />
          <View style={[styles.cardbox, {flexDirection: 'row'}]}>
            <Icon name="checkcircle" color={Colors.DEEPBLUE} size={16} />
            <View style={{marginLeft: 18}}>
              <TextRegular text={'Rabu, 5 Des 2018 Pukul 17:02'} size={12} />
              <TextBold
                text={'PAID'}
                style={{marginTop: 8}}
                color={Colors.DEEPBLUE}
                size={12}
              />
            </View>
          </View>
          <View style={[styles.cardbox, {flexDirection: 'row'}]}>
            <Icon name="checkcircle" color={Colors.DEEPBLUE} size={16} />
            <View style={{marginLeft: 18}}>
              <TextRegular text={'Rabu, 5 Des 2018 Pukul 17:02'} size={12} />
              <TextBold
                text={'VALIDATED'}
                style={{marginTop: 8}}
                color={Colors.DEEPBLUE}
                size={12}
              />
            </View>
          </View>
          <View style={[styles.cardbox, {flexDirection: 'row'}]}>
            <Icon name="checkcircle" color={Colors.DEEPBLUE} size={16} />
            <View style={{marginLeft: 18}}>
              <TextRegular text={'Rabu, 5 Des 2018 Pukul 17:02'} size={12} />
              <TextBold
                text={'CREATE'}
                style={{marginTop: 8}}
                color={Colors.DEEPBLUE}
                size={12}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  cardprofile: {
    backgroundColor: Colors.WHITE,
    padding: 12,
    width: '100%',
    marginTop: 12,
    borderRadius: 5,
  },
  inputText: {
    marginTop: 20,
    width: '100%',
    alignSelf: 'center',
  },
  btnLogin: {
    width: '100%',
    alignSelf: 'center',
    height: 48,
    borderRadius: 6,
    paddingHorizontal: 16,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  cardbox: {
    width: '100%',
    backgroundColor: Colors.WHITE,
    padding: 12,
    borderRadius: 8,
    marginTop: 12,
    borderWidth: 0.6,
    borderColor: '#CCD3DD',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4,
  },
});

export default DetailInsentifComponent;
