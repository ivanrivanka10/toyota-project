import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import ModalTanggal from '../../modal/Insentif/ModalTanggal';

const InsentifComponent = ({navigation}) => {
  const [value, onChangeText] = React.useState('');
  const [modalBottom, setModalBottom] = useState(false);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.GREY,
      }}>
      <View
        style={{
          width: '100%',
          backgroundColor: Colors.PRIMARY,
          paddingTop: 16,
          paddingHorizontal: 16,
          paddingBottom: 30,
        }}>
        <TextBold text={'Insentif'} color={Colors.WHITE} size={20} />
        <View
          style={{
            width: '100%',
            padding: 14,
            backgroundColor: Colors.WHITE,
            borderRadius: 8,
            marginTop: 16,
            marginBottom: 14,
            flexDirection: 'row',

            justifyContent: 'space-between',
          }}>
          <View style={{width: '80%'}}>
            <TextInput
              style={{height: 16, padding: 0}}
              onChangeText={text => onChangeText(text)}
              placeholder="Telusuri..."
              value={value}
            />
          </View>
          <Icon name="search1" size={16} color={Colors.DEEPBLUE} />
        </View>
      </View>
      <ScrollView style={{padding: 16}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TextBold text={'September 2021'} size={20} color={Colors.DEEPBLUE} />
          <TouchableOpacity onPress={() => setModalBottom(true)}>
            <Icon name="calendar" size={21} color={Colors.DEEPBLUE} />
          </TouchableOpacity>

          {/* Modal Untuk Tanggal */}
          <ModalTanggal
            show={modalBottom}
            onClose={() => setModalBottom(false)}
            title="Pilih Bulan"></ModalTanggal>
          {/* Modal Untuk tanggal */}
        
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 4,
          }}>
          <TextRegular
            text={'Total Insentif'}
            size={14}
            color={Colors.DEEPBLUE}
          />
          <TextBold
            text={'Rp. 3.000.000'}
            size={14}
            color={Colors.DEEPBLUE}
            style={{marginLeft: 12}}
          />
        </View>
        <View style={styles.cardbox}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Target Bulanan'}
              size={14}
              color={Colors.DEEPBLUE}
            />
            <TextBold
              text={'80%'}
              size={14}
              color={Colors.DEEPBLUE}
              style={{marginLeft: 12}}
            />
          </View>
          <View
            style={{
              width: '100%',
              backgroundColor: '#F7F7F7',
              height: 12,
              marginTop: 8,
              borderRadius: 6,
            }}>
            <View
              style={{
                width: '80%',
                height: '100%',
                backgroundColor: Colors.PRIMARY,
                borderRadius: 6,
              }}></View>
          </View>
          <TextRegular
            text={'8/10 Unit PO Valid'}
            size={12}
            style={{marginTop: 8, alignSelf: 'flex-end'}}
          />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('DetailInsentif')}>
          <View style={styles.cardbox}>
            <TextRegular text={'TR-1234534-123'} size={12} />
            <View style={styles.row}>
              <TextRegular
                text={'Sen, 17 Sep 2018 - 10:30'}
                size={12}
                color={Colors.DEEPBLUE}
                style={{marginTop: 8, alignSelf: 'flex-end'}}
              />
              <TouchableOpacity>
                <View
                  style={{
                    backgroundColor: Colors.PRIMARY,
                    padding: 4,
                    borderRadius: 4,
                  }}>
                  <TextRegular text={'CREATE'} size={10} color={Colors.WHITE} />
                </View>
              </TouchableOpacity>
            </View>
            <View style={[styles.row, {marginTop: 15}]}>
              <TextRegular
                text={'Nilai Insentif'}
                size={14}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={'Rp. 1.0000.000'}
                size={14}
                color={Colors.DEEPBLUE}
              />
            </View>
            <View
              style={{
                borderWidth: 1,
                borderStyle: 'dashed',
                borderColor: '#CCD3DD',
                marginTop: 10,
                marginBottom: 9,
              }}></View>
            <View style={[styles.row]}>
              <TextRegular
                text={'Nama Salesman'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold text={'Kukuh Hadi'} size={12} color={Colors.DEEPBLUE} />
            </View>
            <View style={[styles.row]}>
              <TextRegular
                text={'Cabang Auto2000'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold text={'Ciledug'} size={12} color={Colors.DEEPBLUE} />
            </View>
            <View style={[styles.row]}>
              <TextRegular
                text={'Tipe Mobil'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold text={'Avanza'} size={12} color={Colors.DEEPBLUE} />
            </View>
            <View style={[styles.row]}>
              <TextRegular
                text={'Nomor Polisi'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold text={'AB1234CD'} size={12} color={Colors.DEEPBLUE} />
            </View>
            <View style={[styles.row]}>
              <TextRegular
                text={'Nama Customer'}
                size={12}
                color={Colors.DEEPBLUE}
              />
              <TextBold
                text={'Aji Bayu Wirrotama'}
                size={12}
                color={Colors.DEEPBLUE}
              />
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  cardbox: {
    width: '100%',
    backgroundColor: Colors.WHITE,
    padding: 12,
    borderRadius: 8,
    marginTop: 12,
    borderWidth: 0.6,
    borderColor: '#CCD3DD',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4,
  },
});

export default InsentifComponent;
