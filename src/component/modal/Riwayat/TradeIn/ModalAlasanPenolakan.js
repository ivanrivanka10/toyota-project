import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Modal,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';

const ModalAlasanPenolakan = ({show,title,onClose,data,navigation, route}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');
  const [notes, setNotes] = useState('');
  const [selectedOption, setSelectedOption] = useState(null);
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: '#eee',
        marginVertical: 5,
      }}
    />
  );

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingTop: 15,
          backgroundColor: 'white',
          width: '90%',
          borderRadius: 5,
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'Alasan Penolakan'} color={Colors.BLACK} size={16} />

          <TouchableOpacity
            style={{position: 'absolute', right: 0, marginRight: 20}} onPress={onClose}>
             
            <Image
              
              source={require('../../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/* Request Pengajuan Discount dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <TextRegular
            text={'* Request Pengajuan Discount'}
            size={14}
            style={{
              marginVertical: 10,
            }}
          />

          {/* Apakah Customer .. dimulai di sini*/}
          <TextRegular
            text={'Apakah customer tetap membeli mobil di sini?'}
            size={14}
            style={{
              marginVertical: 10,
            }}
          />

          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              justifyContent: 'space-between',
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              {/* Buletan Biru Kosong */}
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  width: 20,
                  height: 20,
                  borderWidth: 1,
                  borderColor: '#287AE5',
                  alignSelf: 'center',
                }}
              />
              <TextRegular text={'Ya'} style={{marginHorizontal: 5}} />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              {/* Buletan Biru Kosong */}
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  width: 20,
                  height: 20,
                  borderWidth: 1,
                  borderColor: '#287AE5',
                  alignSelf: 'center',
                }}
              />
              <TextRegular text={'Tidak'} style={{marginHorizontal: 5}} />
            </View>
          </View>

          {/* Apakah Customer .. selesai di sini*/}

          {/*Alsan Tidak Deal.. dimulai di sini*/}
          <TextRegular text={'Alasan Tidak Deal'} style={{marginTop: 20}} />

          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: 50,
              borderWidth: 1,
              borderColor: '#eee',
              marginVertical: 10,
              alignItems: 'center',
              borderRadius: 5,
            }}>
            <TextRegular text={'Pilih Alasan'} style={{marginHorizontal: 10}} />

            <Image
              source={require('../../../../asset/icon-expand-down.png')}
              style={{
                width: 15,
                height: 15,
                position: 'absolute',
                right: 0,
                margin: 10,
              }}
            />
          </TouchableOpacity>
          {/* Alasan Tidak Deal .. selesai di sini*/}
        </View>

        {/*  Notes dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <TextRegular
              text={'Notes'}
              color={Colors.BLACK}
              style={{
                fontWeight: '600',
              }}
            />
            <TextRegular
              text={'0/100'}
              style={{
                fontWeight: '100',
              }}
            />
          </View>

          <View style={{marginTop: 10}}>
            <TextInput
              placeholder="Masukkan Catatan"
              value={notes}
              onChangeText={text => setNotes(text)}
              multiline
              style={{
                width: '100%',
                height: 100,
                paddingHorizontal: 10,
                borderColor: '#dadada',
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="default"
            />
          </View>
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        <TouchableOpacity
          style={{
            marginVertical: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: '#287AE5',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          // onPress={() => filter()}
          >
          <TextRegular
            text={'KIRIM'}
            color={Colors.WHITE}
            style={{fontWeight: '600'}}
          />
        </TouchableOpacity>
      </View>
    </View>
    </Modal>
    
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#E4E7EB',
    borderBottomWidth: 1,
  },
  body: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});
export default ModalAlasanPenolakan;
