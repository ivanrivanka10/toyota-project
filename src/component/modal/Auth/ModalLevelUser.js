import React, {useEffect, useState} from 'react';
import {View, Modal, StyleSheet, TouchableOpacity} from 'react-native';
import {TextBold, TextMedium} from '../../global/Text';
import {Colors} from '../../../styles';
import Icon from 'react-native-vector-icons/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import apiProvider from '../../../utils/service/apiProvider';
import {log} from 'console';
import {useDispatch, useSelector} from 'react-redux';

const ModalLevelUser = ({show, title, onClose, data, navigation}) => {
  const [selectedOption, setSelectedOption] = useState(null);
  const [dataUser, setDataUser] = useState('');
  const dispatch = useDispatch();
  const {loginData, isLoggedIn} = useSelector(state => state.login);

  const handleOptionSelect = option => {
    setSelectedOption(option);
  };
  const isFocused = useIsFocused();
  const [dataBranch, setDataBranch] = useState([]);
  const [visible, setVisible] = useState(show);

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);
  const getDataMobil = async () => {
    const response = await apiProvider.getDataBranch();

    if (response) {
      return setDataBranch(response);
    }
  };

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.header}>
            <TextBold text={title} color={Colors.DEEPBLUE} size={18} />
            <TouchableOpacity onPress={onClose}>
              <Icon name="close" size={18} />
            </TouchableOpacity>  
          </View>
          {dataBranch.map(option => (
            <TouchableOpacity
              key={option.id}
              style={[
                styles.option,
                selectedOption?.id === option.id && styles.selectedOption,
              ]}
              onPress={() => handleOptionSelect(option)}>
              <TextMedium
                text={option.branch}
                color={
                  selectedOption?.id === option.id
                    ? Colors.PRIMARY
                    : Colors.BLACK
                }
              />

              <View
                style={{
                  width: 16,
                  height: 16,
                  borderRadius: 20,
                  borderWidth: 1,
                  borderColor: Colors.PRIMARY,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={
                    selectedOption?.id === option.id && {
                      width: 8,
                      height: 8,
                      borderRadius: 20,

                      backgroundColor: Colors.PRIMARY,
                      alignItems: 'center',
                    }
                  }></View>
              </View>
            </TouchableOpacity>
          ))}
          <TouchableOpacity
            onPress={async () => {
              setVisible(false);
              const params = {
                email: data.user.email,
                role: data.user.role,
                id: data.user.id,
                token: data.token,
                branchid: selectedOption.id,
                branch: selectedOption.branch,
                nrp: data.user.nrp,
              };
              dispatch({type: 'ADD_DATA_LOGIN', data: params});

              navigation.replace('BottomNavigation');
            }}>
            <View
              style={{
                width: '90%',
                backgroundColor: Colors.PRIMARY,
                alignSelf: 'center',
                marginHorizontal: 16,
                marginVertical: 16,
                paddingVertical: 16,
                alignItems: 'center',
                borderRadius: 5,
              }}>
              <TextBold text={'LANJUTKAN'} color={Colors.WHITE} size={16} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#E4E7EB',
    borderBottomWidth: 1,
  },
  body: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 12,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  option: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectedOption: {
    backgroundColor: '#E4E7EB',
  },
});

export default ModalLevelUser;
