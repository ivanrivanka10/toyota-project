import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';

const ModalChangeDiscount = ({show,title,onClose,data,navigation, route}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingTop: 15,
          backgroundColor: Colors.WHITE,
          width: '90%',
          borderRadius: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'Diskon Kacab'} color={Colors.BLACK} size={16} />

          <TouchableOpacity
            onPress={onClose}
            style={{position: 'absolute', right: 0, marginRight: 20}}>
            <Image
              onPress={() => goBack()}
              source={require('../../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/*  Masukkan Discount dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Masukkan Discount'}
              style={{fontWeight: '600', marginVertical: 10}}
              color={Colors.BLACK}
            />
          </View>

          <View style={{flexDirection: 'row', marginTop: 10}}>
            <View
              style={{
                width: '15%',
                height: 50,
                justifyContent: 'center',
                alignSelf: 'center',
                borderTopColor: Colors.BORDERGREY,
                borderBottomColor: Colors.BORDERGREY,
                borderRightColor: Colors.WHITE,
                borderLeftColor: Colors.BORDERGREY,
                borderWidth: 1,
                borderTopLeftRadius: 6,
                borderBottomLeftRadius: 6,
              }}>
              <TextRegular text={'Rp.'} style={{marginLeft: 10}} />
            </View>

            <TextInput
              value={nominalRequestDiscount}
              onChangeText={text => setNominalRequestDiscount(text)}
              placeholder="15.000.000"
              style={{
                width: '85%',
                height: 50,
                borderTopColor: Colors.BORDERGREY,
                borderBottomColor: Colors.BORDERGREY,
                borderRightColor: Colors.BORDERGREY,
                borderLeftColor: Colors.WHITE,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="phone-pad"
            />
          </View>
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        <TouchableOpacity
          style={{
            marginVertical: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.PRIMARY,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          // onPress={() => goBack()}
        >
          <TextRegular
            text={'KIRIM'}
            color={Colors.WHITE}
            style={{fontWeight: '600'}}
          />
        </TouchableOpacity>
      </View>
    </View>

    </Modal>
  );
};

export default ModalChangeDiscount;
