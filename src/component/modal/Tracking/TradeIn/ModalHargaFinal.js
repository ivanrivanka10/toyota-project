import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Modal,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';

const ModalHargaFinal = ({show,title,onClose,data,navigation, route}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');
  const [notes, setNotes] = useState('');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: '#eee',
        marginVertical: 5,
      }}
    />
  );

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>

    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingTop: 15,
          backgroundColor: 'white',
          width: '90%',

          borderRadius: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'Harga Final'} color={Colors.BLACK} size={16} />

          <TouchableOpacity
            style={{position: 'absolute', right: 0, marginRight: 20}} onPress={onClose}>
            <Image
              
              source={require('../../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/*  Masukkan Disocunt dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Masukkan Nominal'}
              style={{fontWeight: '600', marginVertical: 10}}
              color={Colors.BLACK}
            />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 12,
              }}>
              <TextRegular
                text={'Harga Final'}
                style={{fontWeight: '100', marginHorizontal: 5}}
                color={Colors.BLACK}
                size={12}
              />

              <TextRegular
                text={'Rp. 155.000.000'}
                style={{fontWeight: '600'}}
                color={Colors.BLACK}
                size={12}
              />
            </View>
          </View>

          <View style={{flexDirection: 'row', marginTop: 10}}>
            <View
              style={{
                width: '15%',
                height: 50,
                justifyContent: 'center',
                alignSelf: 'center',
                borderTopColor: '#dedede',
                borderBottomColor: '#dedede',
                borderRightColor: 'white',
                borderLeftColor: '#dedede',
                borderWidth: 1,
                borderTopLeftRadius: 6,
                borderBottomLeftRadius: 6,
              }}>
              <TextRegular text={'Rp.'} style={{marginLeft: 10}} />
            </View>

            <TextInput
              value={nominalRequestDiscount}
              onChangeText={text => setNominalRequestDiscount(text)}
              style={{
                width: '85%',
                height: 50,
                borderTopColor: '#dedede',
                borderBottomColor: '#dedede',
                borderRightColor: '#dedede',
                borderLeftColor: 'white',
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="phone-pad"
            />
          </View>

          <TextRegular
            text={' * Request Pengajuan Discount'}
            style={{marginVertical: 5, fontSize: 12}}
          />
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        {/*  Notes dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Notes'}
              color={Colors.BLACK}
              style={{
                fontSize: 14,
                color: '#000',
                fontWeight: '600',
                marginVertical: 10,
              }}
            />

            <TextRegular
              text={'0/100'}
              color={Colors.BLACK}
              style={{
                fontSize: 14,
                color: '#000',
                fontWeight: '100',
                marginVertical: 10,
              }}
            />
          </View>

          <View style={{marginTop: 10}}>
            <TextInput
              placeholder="Masukkan Catatan"
              value={notes}
              onChangeText={text => setNotes(text)}
              multiline
              style={{
                width: '100%',
                height: 100,
                paddingHorizontal: 10,
                borderColor: '#dadada',
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="default"
            />
          </View>
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        <TouchableOpacity
          style={{
            marginVertical: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: '#287AE5',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          // onPress={() => goBack()}
        >
          <TextRegular
            text={'KIRIM'}
            color={Colors.WHITE}
            style={{fontWeight: '600'}}
          />
        </TouchableOpacity>
      </View>
    </View>
    </Modal>
  );
};

export default ModalHargaFinal;
