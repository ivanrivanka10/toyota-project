import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';
import KolomPencarianComponent from '../../../section/Home/KolomPencarianComponent';

const ModalChangeCar = ({show,title,onClose,data,navigation, route}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');
  const [notes, setNotes] = useState('');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: '#eee',
        marginVertical: 5,
      }}
    />
  );

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingTop: 15,
          backgroundColor: Colors.WHITE,
          width: '90%',

          borderRadius: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'NEW CAR'} color={Colors.BLACK} size={16} />

          <TouchableOpacity
            onPress={onClose}
            style={{position: 'absolute', right: 0, marginRight: 20}}>
            <Image
              source={require('../../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/* Kolom pencarian dimulai dari sini  */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '90%',
            alignSelf: 'center',
            margin: 15,
            backgroundColor: Colors.WHITE,
            borderRadius: 5,
            borderWidth: 1,
            borderColor: Colors.PRIMARY,
          }}>
          <TextInput
            style={{
              backgroundColor: Colors.WHITE,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              width: '85%',
            }}
            placeholder="   Telusuri mobil..."
            keyboardType="default"
          />
          <View
            style={{
              width: 50,
              height: 50,
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../../asset/icon-cari.png')}
              style={{
                height: 20,
                marginVertical: 15,
                width: 20,
                tintColor: Colors.PRIMARY,
              }}
            />
          </View>
        </View>
        {/* Kolom pencarian dan iconnya selesai di sini */}

        {/* List Mobil dimulai dari sini  */}
        {/* List Mobil 1 */}
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
              padding: 15,
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
              borderWidth: 1,
              borderTopColor: Colors.BORDERGREY,
              borderLeftColor: Colors.BORDERGREY,
              borderRightColor: Colors.BORDERGREY,
              borderBottomColor: Colors.WHITE,
            }}>
            <TextRegular
              text={'AVANZA VELOZ A/T 2020'}
              color={Colors.DARKBLUETOYOTA}
              size={12}
            />
            <TextRegular
              text={'Rp 250.000.00'}
              color={Colors.PRIMARY}
              size={12}
            />
          </View>
        </TouchableOpacity>
        {/* List Mobil 2 */}
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
              padding: 15,
              borderWidth: 1,
              borderTopColor: Colors.BORDERGREY,
              borderLeftColor: Colors.BORDERGREY,
              borderRightColor: Colors.BORDERGREY,
              borderBottomColor: Colors.WHITE,
            }}>
            <TextRegular
              text={'GRAND NEW VELOZ A/T 2020'}
              color={Colors.DARKBLUETOYOTA}
              size={12}
            />
            <TextRegular
              text={'Rp 320.000.00'}
              color={Colors.PRIMARY}
              size={12}
            />
          </View>
        </TouchableOpacity>
        {/* List Mobil 3 */}
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
              padding: 15,
              borderWidth: 1,
              borderTopColor: Colors.BORDERGREY,
              borderLeftColor: Colors.BORDERGREY,
              borderRightColor: Colors.BORDERGREY,
              borderBottomColor: Colors.WHITE,
            }}>
            <TextRegular
              text={'ALL NEW KIJANG INNOVA'}
              color={Colors.DARKBLUETOYOTA}
              size={12}
            />
            <TextRegular
              text={'Rp 420.000.00'}
              color={Colors.PRIMARY}
              size={12}
            />
          </View>
        </TouchableOpacity>
        {/* List Mobil 4 */}
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
              padding: 15,
              borderWidth: 1,
              borderTopColor: Colors.BORDERGREY,
              borderLeftColor: Colors.BORDERGREY,
              borderRightColor: Colors.BORDERGREY,
              borderBottomColor: Colors.WHITE,
            }}>
            <TextRegular
              text={'GRAND NEW VELOZ A/T 2020'}
              color={Colors.DARKBLUETOYOTA}
              size={12}
            />
            <TextRegular
              text={'Rp 475.000.00'}
              color={Colors.PRIMARY}
              size={12}
            />
          </View>
        </TouchableOpacity>
        {/* List Mobil 5 */}
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
              padding: 15,
              borderWidth: 1,
              borderTopColor: Colors.BORDERGREY,
              borderLeftColor: Colors.BORDERGREY,
              borderRightColor: Colors.BORDERGREY,
              borderBottomColor: Colors.WHITE,
            }}>
            <TextRegular
              text={'TOYOTA ALL NEW ALPHARD'}
              color={Colors.DARKBLUETOYOTA}
              size={12}
            />
            <TextRegular
              text={'Rp1.250.000.00'}
              color={Colors.PRIMARY}
              size={12}
            />
          </View>
        </TouchableOpacity>
        {/* List Mobil 6 */}
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
              padding: 15,
              borderWidth: 1,
              borderTopColor: Colors.BORDERGREY,
              borderLeftColor: Colors.BORDERGREY,
              borderRightColor: Colors.BORDERGREY,
              borderBottomColor: Colors.BORDERGREY,
              marginBottom: 15,
            }}>
            <TextRegular
              text={'TOYOTA FORTUNER'}
              color={Colors.DARKBLUETOYOTA}
              size={12}
            />
            <TextRegular
              text={'Rp 507.000.00'}
              color={Colors.PRIMARY}
              size={12}
            />
          </View>
        </TouchableOpacity>
        {/* List Mobil selesai di sini */}
      </View>
    </View>

    </Modal>
  );
};

export default ModalChangeCar;
