import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../../global';

const ModalRequestDiscount = ({show,title,onClose,data,navigation, route}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');
  const [notes, setNotes] = useState('');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  return (
    <Modal transparent visible={show} onRequestClose={onClose} >
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingTop: 15,
          backgroundColor: Colors.WHITE,
          width: '90%',

          borderRadius: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'Request Discount'} color={Colors.BLACK} size={16} />

          <TouchableOpacity
            onPress={onClose}
            style={{position: 'absolute', right: 0, marginRight: 20}}>
            <Image
              source={require('../../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/*  Masukkan Disocunt dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Masukkan Discount'}
              style={{fontWeight: '600', marginVertical: 10}}
              color={Colors.BLACK}
            />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 12,
              }}>
              <TextRegular
                text={'OTR'}
                style={{fontWeight: '100', marginHorizontal: 5}}
                color={Colors.BLACK}
                size={12}
              />

              <TextRegular
                text={'Rp 276.600.000'}
                style={{fontWeight: '600'}}
                color={Colors.BLACK}
                size={12}
              />
            </View>
          </View>

          <View style={{flexDirection: 'row', marginTop: 10}}>
            <View
              style={{
                width: '15%',
                height: 50,
                justifyContent: 'center',
                alignSelf: 'center',
                borderTopColor: Colors.BORDERGREY,
                borderBottomColor: Colors.BORDERGREY,
                borderRightColor: Colors.WHITE,
                borderLeftColor: Colors.BORDERGREY,
                borderWidth: 1,
                borderTopLeftRadius: 6,
                borderBottomLeftRadius: 6,
              }}>
              <TextRegular text={'Rp.'} style={{marginLeft: 10}} />
            </View>

            <TextInput
              value={nominalRequestDiscount}
              onChangeText={text => setNominalRequestDiscount(text)}
              placeholder="15.000.000"
              style={{
                width: '85%',
                height: 50,
                borderTopColor: Colors.BORDERGREY,
                borderBottomColor: Colors.BORDERGREY,
                borderRightColor: Colors.BORDERGREY,
                borderLeftColor: Colors.WHITE,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="phone-pad"
            />
          </View>

          <TextRegular
            text={' * Request Pengajuan Discount'}
            style={{marginVertical: 5, fontSize: 12}}
          />
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        {/*  Notes dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Notes'}
              color={Colors.BLACK}
              style={{
                fontSize: 14,
                fontWeight: '600',
                marginVertical: 10,
              }}
            />

            <View
              style={{
                position: 'absolute',
                right: 0,
                flexDirection: 'row',
                marginVertical: 10,
              }}>
              <TextRegular
                text={'0/100'}
                color={Colors.BLACK}
                style={{
                  fontSize: 14,
                  color: '#000',
                  fontWeight: '100',
                  marginVertical: 10,
                }}
              />
            </View>
          </View>

          <View style={{marginTop: 10}}>
            <TextInput
              placeholder="Masukkan Catatan"
              value={notes}
              onChangeText={text => setNotes(text)}
              multiline
              style={{
                width: '100%',
                height: 100,
                paddingHorizontal: 10,
                borderColor: Colors.BORDERGREY,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="default"
            />
          </View>
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        <TouchableOpacity
          style={{
            marginVertical: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.PRIMARY,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          // onPress={() => goBack()}
        >
          <TextRegular
            text={'SEND REQUEST'}
            color={Colors.WHITE}
            style={{fontWeight: '600'}}
          />
        </TouchableOpacity>
      </View>
    </View>
    </Modal>
  );
};

export default ModalRequestDiscount;
