import React, {useState} from 'react';
import {
  View,
  Modal,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {TextBold, TextMedium, TextRegular} from '../../global/Text';
import {Colors} from '../../../styles';
import Icon from 'react-native-vector-icons/AntDesign';

const ModalFilter = ({show, title, onClose, data, navigation}) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleOptionSelect = chooseDate => {
    setSelectedOption(chooseDate);
  };

  const chooseDate = [
    {
      id: 50,
      detail: 'Januari 2021',
      harga: 'Rp. 3000.000',
    },
    {
      id: 53,
      detail: 'Februari 2022',
      harga: 'Rp. 3000.000',
    },
    {
      id: 54,
      detail: 'Maret 2021',
      harga: 'Rp. 3000.000',
    },
    {
      id: 55,
      detail: 'April 2022',
      harga: 'Rp. 3000.000',
    },
    {
      id: 56,
      detail: 'Mei 2021',
      harga: 'Rp. 3000.000',
    },
    {
      id: 57,
      detail: 'Juni 2022',
      harga: 'Rp. 3000.000',
    },
    
  ];

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.header}>
            <TextBold text={title} color={Colors.DEEPBLUE} size={18} />
            <TouchableOpacity onPress={onClose}>
              <Icon name="close" size={18} />
            </TouchableOpacity>
          </View>

          <View
            style={{
              paddingHorizontal: 16,
            
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              borderWidth:1,
              borderColor:Colors.BORDERGREY,
              margin: 12,
              borderRadius:10,
            }}>
            <TextInput
              onChangeText={text => onChangeText(text)}
              placeholder="Cari"
            />
            <Icon name="search1" size={14} color={Colors.DEEPBLUE} />
          </View>

          {chooseDate.map(chooseDate => (
            <TouchableOpacity
              key={chooseDate.id}
              style={[
                styles.option,
                selectedOption?.id === chooseDate.id && styles.selectedOption,
              ]}
              onPress={() => handleOptionSelect(chooseDate)}>
              <TextRegular
                text={chooseDate.detail}
                color={
                  selectedOption?.id === chooseDate.id
                    ? Colors.PRIMARY
                    : Colors.BLACK
                }
              />

              <View
                style={{
                  borderColor: Colors.PRIMARY,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TextRegular
                  text={chooseDate.harga}
                  color={
                    selectedOption?.harga === chooseDate.harga
                      ? Colors.PRIMARY
                      : Colors.BLACK
                  }
                />
              </View>
            </TouchableOpacity>
          ))}
          <TouchableOpacity
            onPress={() => navigation.navigate('BottomNavigation')}>
            <View
              style={{
                width: '90%',
                backgroundColor: Colors.PRIMARY,
                alignSelf: 'center',
                marginHorizontal: 16,
                marginVertical: 16,
                paddingVertical: 16,
                alignItems: 'center',
                borderRadius: 5,
              }}>
              <TextRegular text={'Simpan'} color={Colors.WHITE} size={16} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#E4E7EB',
    borderBottomWidth: 1,
  },
  body: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 4,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  option: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectedOption: {
    backgroundColor: '#E4E7EB',
  },
});

export default ModalFilter;
