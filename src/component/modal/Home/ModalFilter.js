import React, {useState} from 'react';
import {View, Modal, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {TextBold, TextMedium, TextRegular} from '../../global/Text';
import {Colors} from '../../../styles';
import Icon from 'react-native-vector-icons/AntDesign';

const ModalFilter = ({
  show,
  title,
  onClose,
  filter,
  setFilter,
  onSaveFilter,
}) => {
  const close = () => {
    onClose = false;
  };

  const handleOptionSelect = chooseFilter => {
    if (filter?.id === chooseFilter.id) {
      return setFilter(null);
    }
    setFilter(chooseFilter);
  };
  const chooseFilter = [
    {
      id: 39,
      detail: 'Terbaru',
      url: `&date=DESC`,
    },
    {
      id: 40,
      detail: 'Terlama',
      url: `&date=ASC`,
    },
    {
      id: 41,
      detail: 'A - Z(Nama Mobil)',
      url: `&carName=DESC`,
    },
    {
      id: 42,
      detail: 'Z - A(Nama Mobil)',
      url: `&carName=ASC`,
    },
    {
      id: 43,
      detail: 'A - Z(Nama Salesman)',
      url: `&kacab=DESC`,
    },
    {
      id: 44,
      detail: 'Z - A(Nama Salesman)',
      url: `&kacab=ASC`,
    },
  ];

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.header}>
            <TextBold text={title} color={Colors.DEEPBLUE} size={18} />
            <TouchableOpacity onPress={onClose}>
              <Icon name="close" size={18} />
            </TouchableOpacity>
          </View>
          {chooseFilter.map(chooseFilter => (
            <TouchableOpacity
              key={chooseFilter.id}
              style={[
                styles.option,
                filter?.id === chooseFilter.id && styles.filter,
              ]}
              onPress={() => handleOptionSelect(chooseFilter)}>
              <TextBold
                text={chooseFilter.detail}
                color={
                  filter?.id === chooseFilter.id ? Colors.PRIMARY : Colors.BLACK
                }
              />

              <View
                style={{
                  width: 16,
                  height: 16,
                  borderRadius: 20,
                  borderWidth: 1,
                  borderColor: Colors.PRIMARY,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={
                    filter?.id === chooseFilter.id && {
                      width: 8,
                      height: 8,
                      borderRadius: 20,
                      backgroundColor: Colors.PRIMARY,
                      alignItems: 'center',
                    }
                  }></View>
              </View>
            </TouchableOpacity>
          ))}
          <TouchableOpacity
            onPress={() => {
              onSaveFilter();
              onClose();
            }}>
            <View
              style={{
                width: '90%',
                backgroundColor: Colors.PRIMARY,
                alignSelf: 'center',
                marginHorizontal: 16,
                marginVertical: 16,
                paddingVertical: 16,
                alignItems: 'center',
                borderRadius: 5,
              }}>
              <TextBold text={'Simpan'} color={Colors.WHITE} size={16} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#E4E7EB',
    borderBottomWidth: 1,
  },
  body: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  option: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: '#E4E7EB',
    marginHorizontal: 5,
  },
  selectedOption: {
    backgroundColor: '#E4E7EB',
  },
});

export default ModalFilter;
