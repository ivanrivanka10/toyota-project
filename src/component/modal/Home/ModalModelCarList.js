import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import KolomPencarianComponent from '../../section/Home/KolomPencarianComponent';
import {FlatList} from 'react-native-gesture-handler';

const ModalModelCarList = ({listCar, onRequestClose, setModelCar}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: '#eee',
        marginVertical: 5,
      }}
    />
  );
  const RenderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        onRequestClose();
        setModelCar(item);
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '90%',
          alignSelf: 'center',
          padding: 15,
          borderWidth: 1,
          borderColor: Colors.BORDERGREY,
        }}>
        <TextRegular
          text={item.carName}
          color={Colors.DARKBLUETOYOTA}
          size={10}
        />
        <TextRegular text={item.otr} color={Colors.PRIMARY} size={10} />
      </View>
    </TouchableOpacity>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingVertical: 15,
          backgroundColor: Colors.WHITE,
          width: '90%',
          borderRadius: 5,
          paddingHorizontal: 5,
          height: '90%',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'NEW CAR'} color={Colors.BLACK} size={16} />

          <TouchableOpacity onPress={onRequestClose}>
            <Image
              onPress={() => goBack()}
              source={require('../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/* Kolom pencarian dimulai dari sini  */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '90%',
            alignSelf: 'center',
            margin: 15,
            backgroundColor: Colors.WHITE,
            borderRadius: 5,
            borderWidth: 1,
            borderColor: Colors.PRIMARY,
          }}>
          <TextInput
            style={{
              backgroundColor: Colors.WHITE,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              width: '85%',
            }}
            placeholder="   Telusuri mobil..."
            keyboardType="default"
          />
          <View
            style={{
              width: 50,
              height: 50,
              alignItems: 'center',
            }}>
            <Image
              source={require('../../../asset/icon-cari.png')}
              style={{
                height: 20,
                marginVertical: 15,
                width: 20,
                tintColor: Colors.PRIMARY,
              }}
            />
          </View>
        </View>
        {/* Kolom pencarian dan iconnya selesai di sini */}
        <FlatList
          data={listCar}
          renderItem={({item}) => <RenderItem item={item} />}
        />

        {/* List Mobil selesai di sini */}
      </View>
    </View>
  );
};

export default ModalModelCarList;
