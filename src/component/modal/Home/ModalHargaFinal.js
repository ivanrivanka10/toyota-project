import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';

const ModalHargaFinal = ({navigation, route, onRequestClose}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');
  const [notes, setNotes] = useState('');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: '#eee',
        marginVertical: 5,
      }}
    />
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingVertical: 15,
          backgroundColor: Colors.WHITE,
          width: '90%',
          borderRadius: 5,
          paddingHorizontal: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'Harga Final'} color={Colors.BLACK} size={16} />

          <TouchableOpacity onPress={onRequestClose}>
            <Image
              onPress={() => goBack()}
              source={require('../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/*  Masukkan Disocunt dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Masukkan Nominal'}
              style={{fontWeight: '600', marginVertical: 10}}
              color={Colors.BLACK}
              size={12}
            />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 12,
              }}>
              <TextRegular
                text={'Harga Final'}
                style={{fontWeight: '100', marginHorizontal: 5}}
                color={Colors.BLACK}
                size={10}
              />

              <TextRegular
                text={'Rp. 155.000.000'}
                style={{fontWeight: '600'}}
                color={Colors.BLACK}
                size={10}
              />
            </View>
          </View>

          <View style={{flexDirection: 'row', marginTop: 10}}>
            <View
              style={{
                width: '15%',
                height: 50,
                justifyContent: 'center',
                alignSelf: 'center',
                borderTopColor: Colors.BORDERGREY,
                borderBottomColor: Colors.BORDERGREY,
                borderRightColor: Colors.WHITE,
                borderLeftColor: Colors.BORDERGREY,
                borderWidth: 1,
                borderTopLeftRadius: 6,
                borderBottomLeftRadius: 6,
              }}>
              <TextRegular
                text={'Rp.'}
                style={{marginLeft: 10}}
                color={Colors.BLACK}
              />
            </View>

            <TextInput
              value={nominalRequestDiscount}
              onChangeText={text => setNominalRequestDiscount(text)}
              style={{
                width: '85%',
                height: 50,
                borderTopColor: Colors.BORDERGREY,
                borderBottomColor: Colors.BORDERGREY,
                borderRightColor: Colors.BORDERGREY,
                borderLeftColor: Colors.WHITE,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="phone-pad"
            />
          </View>

          <TextRegular
            text={' * Request Pengajuan Discount'}
            style={{marginVertical: 5, fontSize: 12}}
          />
        </View>
        {/* Masukkan Discount Selesai Di Sini  */}

        {/*  Notes dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            <TextRegular
              text={'Notes'}
              color={Colors.BLACK}
              style={{
                fontSize: 12,
                color: '#000',
                fontWeight: '600',
              }}
            />

            <TextRegular
              text={'0/100'}
              color={Colors.BLACK}
              style={{
                fontSize: 12,
                fontWeight: '100',
              }}
            />
          </View>

          <View style={{marginTop: 10}}>
            <TextInput
              placeholder="Masukkan Catatan"
              value={notes}
              onChangeText={text => setNotes(text)}
              multiline
              style={{
                width: '100%',
                height: 100,
                paddingHorizontal: 10,
                borderColor: Colors.BORDERGREY,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="default"
            />
          </View>
        </View>
        {/* Notes Selesai Di Sini  */}

        <TouchableOpacity
          style={{
            marginVertical: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.PRIMARY,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          // onPress={() => goBack()}
        >
          <TextRegular
            text={'KIRIM'}
            color={Colors.WHITE}
            style={{fontWeight: '600', marginVertical: 3}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ModalHargaFinal;
