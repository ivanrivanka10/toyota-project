import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  Modal,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
import ModalModelCarList from './ModalModelCarList';
import AntDesign from 'react-native-vector-icons/AntDesign';

const ModalTambahDataCustomer = ({
  dataListCar,
  onRequestClose,
  setModelCar,
  modelCar,
  phone,
  name,
  setPhone,
  setName,
  addNewCar,
}) => {
  const [ModalChangeCarVisible, setModalChangeCarVisible] = useState(false);

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'flex-end',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingVertical: 15,
          backgroundColor: Colors.WHITE,
          width: '100%',
          paddingHorizontal: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 5,
            paddingHorizontal: 15,
          }}>
          <TextBold
            text={'Tambah Pembeli Mobil Baru'}
            color={Colors.BLACK}
            size={16}
          />

          <TouchableOpacity
            onPress={() => {
              setModelCar('');
              setName('');
              setPhone('');
              onRequestClose();
            }}>
            <Image
              onPress={() => goBack()}
              source={require('../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/* Text Input */}
        <View style={{marginHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Nama Customer'}
              size={12}
              style={{fontWeight: '600', marginVertical: 5}}
              color={Colors.BLACK}
            />

            <TextBold
              text={'* Wajib'}
              size={12}
              style={{marginVertical: 5}}
              color={Colors.RED}
            />
          </View>

          <TextInput
            value={name}
            onChangeText={text => setName(text)}
            placeholder="Masukkan nama customer"
            style={styles.txtInput}
          />
        </View>

        {/* Text Input */}
        <View style={{marginHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'No. HP'}
              size={12}
              style={{fontWeight: '600', marginVertical: 5}}
              color={Colors.BLACK}
            />

            <TextBold
              text={'* Wajib'}
              size={12}
              style={{marginVertical: 5}}
              color={Colors.RED}
            />
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <View
              style={{
                width: '15%',
                height: 50,
                justifyContent: 'center',
                alignSelf: 'center',
                borderTopColor: '#dedede',
                borderBottomColor: '#dedede',
                borderRightColor: Colors.WHITE,
                borderLeftColor: '#dedede',
                borderWidth: 1,
                borderTopLeftRadius: 6,
                borderBottomLeftRadius: 6,
              }}>
              <TextRegular text={'+62'} style={{marginLeft: 10}} />
            </View>

            <TextInput
              value={phone}
              onChangeText={text => setPhone(text)}
              style={{
                width: '85%',
                height: 50,
                paddingHorizontal: 10,
                borderTopColor: '#dedede',
                borderBottomColor: '#dedede',
                borderRightColor: '#dedede',
                borderLeftColor: Colors.WHITE,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="phone-pad"
            />
          </View>
        </View>

        <View style={{marginHorizontal: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextRegular
              text={'Model'}
              size={12}
              style={{fontWeight: '600', marginVertical: 5}}
              color={Colors.BLACK}
            />

            <TextBold
              text={'* Wajib'}
              size={12}
              style={{marginVertical: 5}}
              color={Colors.RED}
            />
          </View>
          <TouchableOpacity onPress={() => setModalChangeCarVisible(true)}>
            <View
              style={[
                styles.txtInput,
                {
                  padding: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <TextRegular
                text={modelCar ? modelCar.carName : 'Pilih Model'}
                size={16}
                color={Colors.DEEPBLUE}
              />
              <AntDesign name="circledowno" size={15} color={Colors.DEEPBLUE} />
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => {
            addNewCar();
            onRequestClose();
          }}
          style={{
            marginTop: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.PRIMARY,
            justifyContent: 'center',
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <TextRegular
            text={'TAMBAHKAN'}
            color={Colors.WHITE}
            style={{fontWeight: '600', marginVertical: 3}}
          />
        </TouchableOpacity>
      </View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={ModalChangeCarVisible}
        onRequestClose={() => {
          setModalChangeCarVisible(false);
        }}>
        <ModalModelCarList
          listCar={dataListCar}
          onRequestClose={() => {
            setModalChangeCarVisible(false);
          }}
          setModelCar={setModelCar}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  txtInput: {
    marginVertical: 5,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default ModalTambahDataCustomer;
