import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';

const ModalKonfirmasiDeal = ({navigation, route, onRequestClose}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: '#eee',
        marginVertical: 5,
      }}
    />
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          width: '100%',

          backgroundColor: Colors.WHITE,
          width: '90%',
          borderRadius: 5,
          paddingVertical: 20,
        }}>
        <TextRegular
          text={'Apakah data yang akan anda kirim Sudah dipastikan benar?'}
          style={{
            marginVertical: 10,
            fontSize: 16,
            justifyContent: 'center',
            textAlign: 'center',
            width: 250,
            alignSelf: 'center',
          }}
        />

        <TouchableOpacity
          style={{
            marginVertical: 10,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.PRIMARY,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          //   onPress={() => filter()}
        >
          <TextRegular
            text={'KIRIM'}
            color={Colors.WHITE}
            style={{fontWeight: '600', marginVertical: 3}}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={onRequestClose}
          style={{
            marginVertical: 10,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.WHITE,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            borderColor: Colors.BORDERGREY,
            borderWidth: 1,
          }}
          //   onPress={() => filter()}
        >
          <TextRegular
            text={'KEMBALI'}
            style={{fontWeight: '600', marginVertical: 3}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ModalKonfirmasiDeal;
