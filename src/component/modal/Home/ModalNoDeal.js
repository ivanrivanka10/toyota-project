import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';

import {Colors} from '../../../styles';
import {TextBold, TextRegular, TextMedium} from '../../global';
// Halo

const ModalNoDeal = ({navigation, route, onRequestClose}) => {
  const [nominalRequestDiscount, setNominalRequestDiscount] = useState('');
  const [notes, setNotes] = useState('');

  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 2,
        backgroundColor: Colors.BORDERGREY,
        marginVertical: 5,
      }}
    />
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* Header  */}
      <View
        style={{
          paddingVertical: 15,
          backgroundColor: Colors.WHITE,
          width: '90%',
          borderRadius: 5,
          paddingHorizontal: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            paddingHorizontal: 15,
          }}>
          <TextBold text={'Alasan Penolakan'} color={Colors.BLACK} size={16} />

          <TouchableOpacity onPress={onRequestClose}>
            <Image
              onPress={() => goBack()}
              source={require('../../../asset/icon-close.png')}
              style={{
                width: 15,
                height: 15,
              }}
            />
          </TouchableOpacity>
        </View>

        <Separator />

        {/* Request Pengajuan Discount dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <TextRegular
            text={'* Request Pengajuan Discount'}
            size={10}
            style={{
              marginVertical: 10,
            }}
          />

          {/* Apakah Customer .. dimulai di AUTO2000*/}
          <TextRegular
            text={'Apakah customer tetap membeli mobil AUTO2000?'}
            size={11}
          />

          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              {/* Buletan Biru Kosong */}
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  width: 18,
                  height: 18,
                  borderWidth: 1,
                  borderColor: Colors.PRIMARY,
                  alignSelf: 'center',
                }}
              />
              <TextRegular
                text={'Ya'}
                style={{marginHorizontal: 5}}
                size={12}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft: 50,
              }}>
              {/* Buletan Biru Kosong */}
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  width: 18,
                  height: 18,
                  borderWidth: 1,
                  borderColor: Colors.PRIMARY,
                  alignSelf: 'center',
                }}
              />
              <TextRegular
                text={'Tidak'}
                style={{marginHorizontal: 5}}
                size={12}
              />
            </View>
          </View>

          {/* Apakah Customer .. selesai di sini*/}

          {/*Alasan Tidak Deal.. dimulai di sini*/}
          <TextRegular
            text={'Alasan Tidak Deal'}
            style={{marginTop: 20}}
            size={12}
          />

          <TouchableOpacity
            style={{
              flexDirection: 'row',
              width: '100%',
              height: 50,
              borderWidth: 1,
              borderColor: Colors.BORDERGREY,
              marginVertical: 10,
              alignItems: 'center',
              borderRadius: 5,
            }}>
            <TextRegular text={'Pilih Alasan'} style={{marginHorizontal: 10}} />

            <Image
              source={require('../../../asset/icon-expand-down.png')}
              style={{
                width: 15,
                height: 15,
                position: 'absolute',
                right: 0,
                margin: 10,
              }}
            />
          </TouchableOpacity>
          {/* Alasan Tidak Deal .. selesai di sini*/}
        </View>

        {/*  Notes dimulai di sini*/}
        <View style={{paddingHorizontal: 15}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            <TextRegular
              text={'Notes'}
              color={Colors.BLACK}
              style={{
                fontSize: 12,
                color: '#000',
                fontWeight: '600',
              }}
            />

            <TextRegular
              text={'0/100'}
              color={Colors.BLACK}
              style={{
                fontSize: 12,
                fontWeight: '100',
              }}
            />
          </View>

          <View style={{marginTop: 10}}>
            <TextInput
              placeholder="Masukkan Catatan"
              value={notes}
              onChangeText={text => setNotes(text)}
              multiline
              style={{
                width: '100%',
                height: 100,
                paddingHorizontal: 10,
                borderColor: Colors.BORDERGREY,
                borderWidth: 1,
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
              }}
              keyboardType="default"
            />
          </View>
        </View>
        {/* Notes Selesai Di Sini  */}

        <TouchableOpacity
          style={{
            marginVertical: 20,
            width: '90%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: Colors.PRIMARY,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          onPress={() => filter()}>
          <TextRegular
            text={'KIRIM'}
            color={Colors.WHITE}
            style={{fontWeight: '600', marginVertical: 4}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ModalNoDeal;
