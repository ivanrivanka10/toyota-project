import React, {useEffect, useState} from 'react';
import HomeComponent from '../../component/section/Home/HomeComponent';
import {useIsFocused} from '@react-navigation/native';
import apiProvider from '../../utils/service/apiProvider';
import {useSelector} from 'react-redux';

const Home = ({navigation, route}) => {
  const [dataTotalDashboard, setDataTotalDashboard] = useState([]);
  const [dataTradein, setDataTradein] = useState([]);
  const [dataNewCar, setDataNewCar] = useState([]);
  const [dataListCar, setDataListCar] = useState([]);
  const [keyword, setKeyword] = useState('');
  const isFocused = useIsFocused();
  const [filter, setFilter] = useState('');
  const [modelCar, setModelCar] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const {loginData} = useSelector(state => state.login);
  useEffect(() => {
    getDataDashboardTotal();
    getDataTradein();
    getDataNewCar();
    getDataListCar();
  }, [isFocused]);

  const getDataDashboardTotal = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDashboardTotal(token);
    if (response) {
      setDataTotalDashboard(response);
    }
  };
  const getDataTradein = async (value = '') => {
    const search = `&search=${value}`;
    const token = loginData.token;
    const filtering = filter?.url ?? ``;
    const idBranch = loginData.branchid;
    const response = await apiProvider.GetDataTradein(
      search,
      filtering,
      token,
      idBranch,
    );
    if (response) {
      return setDataTradein(response);
    }
  };
  const getDataNewCar = async (value = '') => {
    const search = `&search=${value}`;
    const token = loginData.token;
    const filtering = filter?.url ?? ``;
    const idBranch = loginData.branchid;
    const response = await apiProvider.GetDataNewCar(
      search,
      filtering,
      token,
      idBranch,
    );
    if (response) {
      return setDataNewCar(response);
    }
  };
  const getDataListCar = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataListCar(token);
    if (response) {
      setDataListCar(response);
    }
  };

  const addNewCar = async () => {
    let phoneNumber = phone[0] != 0 ? `0${phone.toString()}` : phone;
    const body = {
      customerName: name,
      phone: phoneNumber,
      newCarId: modelCar.id,
    };
    const response = await apiProvider.addNewCar(
      body,
      loginData.token,
      loginData.branchid,
    );
    if (response.success) {
      getDataNewCar();
      setModelCar('');
      setName('');
      setModelCar('');
    }
  };
  return (
    <HomeComponent
      navigation={navigation}
      dataTotalDashboard={dataTotalDashboard}
      dataTradein={dataTradein}
      dataNewCar={dataNewCar}
      loginData={loginData}
      onSearch={value => {
        getDataTradein(value);
        getDataNewCar(value);
        setKeyword(value);
      }}
      keyword={keyword}
      filter={filter}
      setFilter={setFilter}
      onSaveFilter={() => {
        getDataNewCar();
        getDataTradein();
      }}
      dataListCar={dataListCar}
      modelCar={modelCar}
      phone={phone}
      name={name}
      setModelCar={value => {
        setModelCar(value);
      }}
      setPhone={value => {
        setPhone(value);
      }}
      setName={value => {
        setName(value);
      }}
      addNewCar={() => addNewCar()}
    />
  );
};

export default Home;
