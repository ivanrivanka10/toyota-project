import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import TotalAppraisalComponent from '../../component/section/Home/TotalAppraisalComponent';

const TotalAppraisal = ({navigation}) => {
  return <TotalAppraisalComponent navigation={navigation} />;
};

export default TotalAppraisal;
