import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import POComponent from '../../component/section/Home/POComponent';

const PO = ({navigation}) => {
  return <POComponent navigation={navigation} />;
};

export default PO;
