import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import BeliMobilComponent from '../../component/section/Home/BeliMobilComponent';
import {useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import apiProvider from '../../utils/service/apiProvider';

const BeliMobil = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idBeliMobil = route.params.item.id;
  const [dataDetailBeliMobil, setDataDetailBeliMobil] = useState('');
  useEffect(() => {
    getDataDetailBeliMobil();
  }, [isFocused]);
  const item = route.params;
  const getDataDetailBeliMobil = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDetailBeliMobil(
      idBeliMobil,
      token,
    );
    if (response) {
      return setDataDetailBeliMobil(response);
    }
  };
  return (
    <BeliMobilComponent navigation={navigation} item={dataDetailBeliMobil} />
  );
};

export default BeliMobil;
