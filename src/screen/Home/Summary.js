import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import SummaryComponent from '../../component/section/Home/SummaryComponent';
import {useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import apiProvider from '../../utils/service/apiProvider';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const Summary = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idDataAppraisal = route.params.item.approvalTradeIn.id;
  const [dataAppraisal, setDataAppraisal] = useState('');
  useEffect(() => {
    GetDataAppraisal();
  }, [isFocused]);
  const item = route.params;
  const GetDataAppraisal = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDetailToolTradeIn(
      idDataAppraisal,
      token,
    );
    if (response) {
      return setDataAppraisal(response);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      {dataAppraisal ? (
        <SummaryComponent navigation={navigation} item={dataAppraisal} />
      ) : null}
    </View>
  );
};

export default Summary;
