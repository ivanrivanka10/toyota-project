// import moment from 'moment';
import React, {useEffect} from 'react';
import {View, Text, Image} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('AuthNavigation');
    }, 2000);
  }, []);
  // const originalDate = '2021-01-18T09:37:53.207Z';
  // const formattedDate = moment(originalDate).format('YYYY-MM-DD HH:mm');

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      {/* <Text>Halooooo</Text> */}
      {/* <Text> {formattedDate} </Text> */}
      <Image
        style={{width: 200, height: 200}}
        source={require('../../asset/img-splash.png')}
      />
    </View>
  );
};

export default SplashScreen;
