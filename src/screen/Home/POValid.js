import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import POValidComponent from '../../component/section/Home/POValidComponent';

const POValid = ({navigation}) => {
  return <POValidComponent navigation={navigation} />;
};

export default POValid;
