import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import ToolTradeInComponent from '../../component/section/Home/ToolTradeInComponent';
import {useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import apiProvider from '../../utils/service/apiProvider';

const ToolTradeIn = ({navigation, route, onChange}) => {
  const [dataListCar, setDataListCar] = useState([]);
  const [dataTradeIn, setDataTradeIn] = useState('');
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const [dataDetailToolTradeIn, setDataDetailToolTradeIn] = useState('');
  const [idNewCar, setIdNewCar] = useState('');
  useEffect(() => {
    getDataListCar();
    GetDataDetailTradeIn();
  }, [isFocused]);

  const getDataListCar = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataListCar(token);
    console.log('ini respon get list', response);
    if (response) {
      setDataListCar(response);
    }
  };
  const GetDataDetailTradeIn = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDetailTradeInCar(
      route.params.item.id,
      token,
    );
    console.log('ini respon get data', response);
    if (response) {
      setDataTradeIn(response);
    }
  };
  const updateDataNewCar = async id => {
    const item = idNewCar ? idNewCar : ``;

    const token = loginData.token;
    const body = {
      newCarId: id,
      branchId: loginData.branchid,
    };
    const response = await apiProvider.updateDataNewCar(
      route.params.item.id,
      token,
      body,
    );
    if (response) {
    }
    GetDataDetailTradeIn();
  };

  return (
    <View style={{flex: 1}}>
      {dataTradeIn ? (
        <ToolTradeInComponent
          navigation={navigation}
          listCar={dataListCar}
          setIdNewCar={setIdNewCar}
          dataDetailTradeIn={dataTradeIn}
          item={dataTradeIn}
          onChange={updateDataNewCar}
        />
      ) : (
        <Text>hallo</Text>
      )}
    </View>
  );
};

export default ToolTradeIn;
