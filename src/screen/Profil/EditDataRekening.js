import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';

import {Colors} from '../../styles';
import EditDataRekeningComponent from '../../component/section/Profil/EditDataRekeningComponent';
import apiProvider from '../../utils/service/apiProvider';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
const EditDataRekening = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const [bank, setBank] = useState('');
  const [name, setName] = useState('');
  const [noRekening, setNoRekening] = useState('');

  useEffect(() => {
    getDataProfile();
  }, [isFocused]);
  const getDataProfile = async () => {
    const token = loginData.token;
    const response = await apiProvider.getDataProfil(
      token,
      loginData.id,
      loginData.branchid,
    );
    console.log(response);
    if (response) {
      setBank(response.profile.bankName);
      setName(response.profile.bankBeneficiary);
      setNoRekening(response.profile.bankAccNo);
    }
  };
  const onSave = async value => {
    let formData = new FormData();
    for (let key in value) {
      formData.append(key, value[key]);
    }
    const response = await apiProvider.pathDataProfile(
      loginData.token,
      formData,
    );
    if (response && response.success) {
      navigation.pop();
    }
  };

  const item = route.params;
  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      {bank && name && noRekening ? (
        <EditDataRekeningComponent
          navigation={navigation}
          item={item}
          bank={bank}
          name={name}
          noRekening={noRekening}
          setBank={text => setBank(text)}
          setName={text => setName(text)}
          setNoRekening={text => setNoRekening(text)}
          onSave={onSave}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default EditDataRekening;
