import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';

import {Colors} from '../../styles';
import EditDataDiriComponent from '../../component/section/Profil/EditDataDiriComponent';
import {CommonActions, useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import apiProvider from '../../utils/service/apiProvider';

const EditDataDiri = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const [namaKacab, setNamaKacab] = useState('');
  const [cabang, setCabang] = useState('');
  const [nrp, setNrp] = useState('');
  const [phone, setPhone] = useState('');
  const [birth, setBirth] = useState('');

  useEffect(() => {
    getDataProfile();
  }, [isFocused]);
  const getDataProfile = async () => {
    const token = loginData.token;
    const response = await apiProvider.getDataProfil(
      token,
      loginData.id,
      loginData.branchid,
    );
    if (response) {
      setNamaKacab(response.profile.name);
      setCabang(response.profile.SalesBranch.branch);
      setNrp(response.profile.nrp);
      setPhone(response.profile.phone);
      setBirth(response.profile.dateOfBirth);
    }
  };
  const onSave = async value => {
    let formData = new FormData();
    for (let key in value) {
      formData.append(key, value[key]);
    }
    const response = await apiProvider.pathDataProfile(
      loginData.token,
      formData,
    );
    if (response && response.success) {
      navigation.pop();
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      {birth && namaKacab && cabang && nrp && phone ? (
        <EditDataDiriComponent
          // item={item}
          navigation={navigation}
          birth={birth}
          nrp={nrp}
          namaKacab={namaKacab}
          cabang={cabang}
          phone={phone}
          setBirth={text => setBirth(text)}
          setPhone={text => setPhone(text)}
          setCabang={text => setCabang(text)}
          setNamaKacab={text => setNamaKacab(text)}
          setNrp={text => setNrp(text)}
          onSave={onSave}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default EditDataDiri;
