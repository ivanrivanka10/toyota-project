import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';

import {Colors} from '../../styles';
import EditDokumenComponent from '../../component/section/Profil/EditDokumenComponent';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import apiProvider from '../../utils/service/apiProvider';

const EditDokumen = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const [noktp, setNoKtp] = useState('');
  const [ktp, setKtp] = useState('');
  const [noNpwp, setNoNpwp] = useState('');
  const [npwp, setNpwp] = useState('');

  useEffect(() => {
    getDataProfile();
  }, [isFocused]);
  const getDataProfile = async () => {
    const token = loginData.token;
    const response = await apiProvider.getDataProfil(
      token,
      loginData.id,
      loginData.branchid,
    );
    console.log(response);
    if (response) {
      setKtp(response.profile.ktp);
      setNoNpwp(response.profile.noNPWP);
      setNoKtp(response.profile.noKTP);
      setNpwp(response.profile.npwp);
    }
  };
  const onSave = async value => {
    let formData = new FormData();
    for (let key in value) {
      formData.append(key, value[key]);
    }
    const response = await apiProvider.pathDataProfile(
      loginData.token,
      formData,
    );
    if (response && response.success) {
      navigation.pop();
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      {npwp && noNpwp && ktp && noktp ? (
        <EditDokumenComponent
          navigation={navigation}
          npwp={npwp}
          noNpwp={noNpwp}
          ktp={ktp}
          noktp={noktp}
          setKtp={text => setKtp(text)}
          setNoKtp={text => setNoKtp(text)}
          setNoNpwp={text => setNoNpwp(text)}
          setNpwp={text => setNpwp(text)}
          onSave={onSave}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default EditDokumen;
