import React, {useEffect, useState} from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
  ActivityIndicator,
} from 'react-native';

import {Colors} from '../../styles';
import ProfilComponent from '../../component/section/Profil/ProfilComponent';
import {useSelector} from 'react-redux';
import apiProvider from '../../utils/service/apiProvider';
import {useIsFocused} from '@react-navigation/native';

const Profil = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const [dataProfile, setDataProfile] = useState('');
  useEffect(() => {
    getDataProfile();
  }, [isFocused]);
  const getDataProfile = async () => {
    const token = loginData.token;
    const response = await apiProvider.getDataProfil(
      token,
      loginData.id,
      loginData.branchid,
    );
    if (response) {
      setDataProfile(response);
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.WHITE}}>
      {dataProfile ? (
        <ProfilComponent navigation={navigation} item={dataProfile} />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default Profil;
