import React, {useEffect, useState} from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
  ActivityIndicator,
} from 'react-native';

import {Colors} from '../../styles';
import apiProvider from '../../utils/service/apiProvider';
import {useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import SumaryNewCarComponent from '../../component/section/Riwayat/SumaryNewCarComponent';
import SumaryTradeInComponent from '../../component/section/Riwayat/SumaryTradeInComponent';

const SumaryNewCar = ({navigation, route}) => {
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idDetailNewcar = route.params.id;
  const [dataNewCar, setDataNewCar] = useState('');
  useEffect(() => {
    getDataDetailNewCar();
  }, [isFocused]);
  const getDataDetailNewCar = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDetailNewCar(
      idDetailNewcar,
      token,
    );
    if (response) {
      return setDataNewCar(response);
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.RED}}>
      {dataNewCar ? (
        <SumaryNewCarComponent navigation={navigation} item={dataNewCar} />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default SumaryNewCar;
