import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Colors} from '../../styles';
import RiwayatComponent from '../../component/section/Riwayat/RiwayatComponent';
import apiProvider from '../../utils/service/apiProvider';
import {useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';

const Riwayat = ({navigation, route}) => {
  const [dataTradein, setDataTradein] = useState([]);
  const [dataNewCar, setDataNewCar] = useState([]);
  const [keyword, setKeyword] = useState('');
  const isFocused = useIsFocused();
  const {loginData} = useSelector(state => state.login);
  const [filter, setFilter] = useState('');

  useEffect(() => {
    getDataTradein();
    getDataNewCar();
  }, [isFocused]);

  const getDataTradein = async (value = '') => {
    const search = `&search=${value}`;
    const filtering = filter?.url ?? ``;
    const token = loginData.token;
    const idBranch = loginData.branchid;
    const response = await apiProvider.GetDataTradeinRiwayat(
      search,
      filtering,
      token,
      idBranch,
    );
    if (response) {
      return setDataTradein(response);
    }
  };
  const getDataNewCar = async (value = '') => {
    const search = `&search=${value}`;
    const filtering = filter?.url ?? ``;
    const token = loginData.token;
    const idBranch = loginData.branchid;
    const response = await apiProvider.GetDataNewCarRiwayat(
      search,
      filtering,
      token,
      idBranch,
    );
    if (response) {
      return setDataNewCar(response);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      <RiwayatComponent
        navigation={navigation}
        dataTradein={dataTradein}
        dataNewCar={dataNewCar}
        onSearch={value => {
          getDataTradein(value);
          getDataNewCar(value);
          setKeyword(value);
        }}
        keyword={keyword}
        filter={filter}
        setFilter={setFilter}
        onSaveFilter={() => {
          getDataNewCar();
          getDataTradein();
        }}
      />
    </View>
  );
};

export default Riwayat;
