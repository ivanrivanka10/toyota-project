import React, {useEffect, useState} from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
  ActivityIndicator,
} from 'react-native';

import {Colors} from '../../styles';
import SumaryTradeInComponent from '../../component/section/Riwayat/SumaryTradeInComponent';
import apiProvider from '../../utils/service/apiProvider';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import {WaitingLoading} from '../../utils/helpers/loading';

const SumaryTradeIn = ({navigation, route}) => {
  const item = route.params;
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idDetaiTradeIn = route.params.item.id;
  const [dataTradeIn, setDataTradeIn] = useState('');
  useEffect(() => {
    GetDataDetailTradeInCar();
  }, [isFocused]);
  const GetDataDetailTradeInCar = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDetailTradeInCar(
      idDetaiTradeIn,
      token,
    );
    if (response) {
      return setDataTradeIn(response);
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.WHITE}}>
      {dataTradeIn ? (
        <SumaryTradeInComponent navigation={navigation} item={dataTradeIn} />
      ) : (
        <WaitingLoading />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default SumaryTradeIn;
