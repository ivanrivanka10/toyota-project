import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import {
  TextRegular,
  TextBold,
  TextMedium,
  Header,
  InputText,
} from '../../component/global';
import {Colors} from '../../styles';
import TradeInComponent from '../../component/section/Tracking/TrackingTradeInComponent';
import apiProvider from '../../utils/service/apiProvider';
import {useIsFocused} from '@react-navigation/native';
import NewCarComponent from '../../component/section/Tracking/TrackingNewCarComponent';

const NewCar = ({navigation, route}) => {
  const [data, setData] = useState([]);
  const isFocused = useIsFocused();

  useEffect(() => {
    GetDataDashboardTotal();
  }, [isFocused]);
  const GetDataDashboardTotal = async () => {
    const response = await apiProvider.GetDataNewCarTracking();
    if (response) {
      return setData(response);
    }
  };

  return (
    <View>
      {data ? <NewCarComponent navigation={navigation} routing={data} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default NewCar;
