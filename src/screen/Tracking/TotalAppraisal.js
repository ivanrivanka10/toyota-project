import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import TotalAppraisalComponent from '../../component/section/Home/TotalAppraisalComponent';

const TotalAppraisal = ({navigation, route}) => {
  const item = route.params;
  return <TotalAppraisalComponent navigation={navigation} item={item}/>;
};

export default TotalAppraisal;
