import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import {
  TextRegular,
  TextBold,
  TextMedium,
  Header,
  InputText,
} from '../../component/global';
import {Colors} from '../../styles';
import DetailAppraisalTrackingComponent from '../../component/section/Tracking/DetailAppraisalTrackingComponent';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import apiProvider from '../../utils/service/apiProvider';

const DetailAppraisalTracking = ({navigation, route}) => {
  const item = route.params;
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idDataAppraisal = route.params.item.approvalTradeIn.id;
  const [dataAppraisal, setDataAppraisal] = useState('');
  
  useEffect(() => {
    GetDataAppraisal();
  }, [isFocused]);
  // const item = route.params;
  const GetDataAppraisal = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataDetailToolTradeIn(idDataAppraisal, token);
    if (response) {
      return setDataAppraisal(response);
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.AQUA}}>
      {dataAppraisal ? (
        <DetailAppraisalTrackingComponent
          navigation={navigation}
          item={dataAppraisal}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default DetailAppraisalTracking;
