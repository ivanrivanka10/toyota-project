import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import BeliMobilComponent from '../../component/section/Tracking/BeliMobilComponent';
import {Colors} from '../../styles';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import apiProvider from '../../utils/service/apiProvider';
const TrackingBeliMobil = ({navigation, route}) => {
  const item = route.params;

  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idDetailNewcar = item.item.ApprovalNewCar.id;

  const [dataNewCar, setDataNewCar] = useState('');
  useEffect(() => {
    getDataDetailNewCar();
  }, [isFocused]);
  const getDataDetailNewCar = async () => {
    const token = loginData.token; 
    const response = await apiProvider.GetDataDetailNewCar(
      idDetailNewcar,
      token,
    );
    if (response) {
      return setDataNewCar(response);
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.AQUA}}>
      {dataNewCar ? (
        <BeliMobilComponent navigation={navigation} item={dataNewCar} />
      ) : null}
    </View>
  );
};

export default TrackingBeliMobil;
