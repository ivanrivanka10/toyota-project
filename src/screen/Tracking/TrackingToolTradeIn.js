import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import ToolTradeInComponent from '../../component/section/Tracking/ToolTradeInComponent';
import {Colors} from '../../styles';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import apiProvider from '../../utils/service/apiProvider';

const TrackingToolTradeIn = ({navigation, route}) => {
  const item = route.params;
  const {loginData} = useSelector(state => state.login);
  const isFocused = useIsFocused();
  const idDetaiTradeIn = route.params.item.ApprovalTradeIn.id; //
  const [dataTradeIn, setDataTradeIn] = useState('');
  const [dataListCar, setDataListCar] = useState([]);
  const [idNewCar, setIdNewCar] = useState('');
  useEffect(() => {
    GetDataDetailTradeInCar();
    getDataListCar();
  }, [isFocused]);
  const getDataListCar = async () => {
    const token = loginData.token;
    const response = await apiProvider.GetDataListCar(token);
    if (response) {
      return await setDataListCar(response);
    }
  };
  const GetDataDetailTradeInCar = async () => {
    const token = loginData.token;

    const response = await apiProvider.GetDataDetailTradeInCar(
      idDetaiTradeIn,
      token,
    );
    if (response) {
      return setDataTradeIn(response);
    }
  };
  const updateDataNewCar = async id => {
    const item = idNewCar ? idNewCar : ``;
    const token = loginData.token;
    const body = {
      newCarId: id,
      branchId: loginData.branchid,
    };
    const response = await apiProvider.updateDataNewCar(
      route.params.item.ApprovalTradeIn.id,
      token,
      body,
    );
    if (response) {
    }
    GetDataDetailTradeInCar();
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.GREY}}>
      {dataTradeIn ? (
        <ToolTradeInComponent
          navigation={navigation}
          item={dataTradeIn}
          listCar={dataListCar}
          setIdNewCar={setIdNewCar}
          onChange={updateDataNewCar}
        />
      ) : // <View
      //   style={{
      //     width: '100%',
      //     height: '100%',
      //     justifyContent: 'center',
      //     alignItems: 'center',
      //     alignContent: 'center',
      //   }}>
      //   <ActivityIndicator size="large" color={Colors.PRIMARY} />
      // </View>
      null}
    </View>
  );
};

export default TrackingToolTradeIn;
