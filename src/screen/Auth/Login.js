import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {
  TextRegular,
  TextBold,
  TextMedium,
  Header,
  InputText,
} from '../../component/global';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/AntDesign';
import ModalBottom from '../../component/modal/ModalBottom';
import ModalCenter from '../../component/modal/ModalCenter';
import LoginComponent from '../../component/section/Auth/LoginComponent';
import {useIsFocused} from '@react-navigation/native';
import Axios from 'axios';
import apiProvider from '../../utils/service/apiProvider';
import {useSelector} from 'react-redux';
const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const isFocused = useIsFocused();
  const [password, setPassword] = useState('');
  const [hidePassword, setHidePassword] = useState(false);
  const [dataBranch, setDataBranch] = useState([]);
  const {loginData, isLoggedIn} = useSelector(state => state.login);
  const onLogin = () => {
    navigation.navigate('BottomNavigation');
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  useEffect(() => {
    getDataMobil();
    if (isLoggedIn != false) {
      navigation.replace('BottomNavigation');
    } else {
    }
  }, [isFocused]);
  const getDataMobil = async () => {
    const response = await apiProvider.getDataBranch();
    if (response) {
      setDataBranch(response);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <LoginComponent
        navigation={navigation}
        email={email}
        setEmail={text => setEmail(text)}
        password={password}
        setPassword={text => setPassword(text)}
        hidePassword={hidePassword}
        setShowPassword={() => setHidePassword(prevState => !prevState)}
        onPressLogin={() => onLogin()}
        data={dataBranch}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default Login;
