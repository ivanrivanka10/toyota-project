import React, {useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import {
  TextRegular,
  TextBold,
  TextMedium,
  Header,
  InputText,
} from '../../component/global';
import {Colors} from '../../styles';
import InsentifComponent from '../../component/section/Insentif/InsentifComponent';

const Insentif = ({navigation, route}) => {
  return <InsentifComponent navigation={navigation} />;
};

export default Insentif;
