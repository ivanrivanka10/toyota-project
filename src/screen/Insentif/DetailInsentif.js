import React, {useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';

import {Colors} from '../../styles';
import DetailInsentifComponent from '../../component/section/Insentif/DetailInsentifComponent';

const DetailIntensif = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: Colors.AQUA}}>
      <DetailInsentifComponent navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default DetailIntensif;
