import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Login from '../screen/Auth/Login';
import Home from '../screen/Home/Home';
import Riwayat from '../screen/Riwayat/Riwayat';
import Trasaction from '../screen/Insentif/Insentif';
import Insentif from '../screen/Insentif/Insentif';
import {Colors} from '../styles';
import Profil from '../screen/Profil/Profil';
import Tracking from '../screen/Tracking/Tracking';

const Tab = createBottomTabNavigator();

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: 56,
        borderTopEndRadius: 10,
        alignContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const LabelIcon = {
          Home: require('../../src/asset/Category.png'),
          Transaction: require('../../src/asset/tracking.png'),
          Riwayat: require('../../src/asset/Paper.png'),
          Insentif: require('../../src/asset/Insentif.png'),
          Profil: require('../../src/asset/user.png'),
          Tracking: require('../../src/asset/tracking.png'),
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              alignSelf: 'center',
              height: 40,
              width: 40,
              alignItems: 'center',
            }}>
            <Image
              source={LabelIcon[label]}
              style={{
                width: 20,
                height: 20,
                tintColor: isFocused ? Colors.DEEPBLUE : '#D8D8D8',
              }}
            />
            <Text style={{color: isFocused ? Colors.DEEPBLUE : '#D8D8D8'}}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

function MyTabs(params) {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false, tabBarHideOnKeyboard: true}}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Tracking" component={Tracking} />
      <Tab.Screen name="Riwayat" component={Riwayat} />
      <Tab.Screen name="Insentif" component={Insentif} />
      <Tab.Screen name="Profil" component={Profil} />
    </Tab.Navigator>
  );
}
export default MyTabs;
