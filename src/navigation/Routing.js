import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {NavigationContainer} from '@react-navigation/native';
import AuthNavigation from './AuthNavigation';
import MyTabs from './BottomNavigation';
import Home from '../screen/Home/Home';
import Insentif from '../screen/Insentif/Insentif';
import Profil from '../screen/Profil/Profil';
import Riwayat from '../screen/Riwayat/Riwayat';
import Tracking from '../screen/Tracking/Tracking';
import EditDataDiri from '../screen/Profil/EditDataDiri';
import EditDataRekening from '../screen/Profil/EditDataRekening';
import BeliMobil from '../screen/Home/BeliMobil';
import CekHarga from '../screen/Home/CekHarga';
import PO from '../screen/Home/PO';
import POValid from '../screen/Home/POValid';
import Summary from '../screen/Home/Summary';
import ToolTradeIn from '../screen/Home/ToolTradeIn';
import TotalAppraisal from '../screen/Home/TotalAppraisal';
import RiwayatComponent from '../component/section/Riwayat/RiwayatComponent';
import SumaryTradeIn from '../screen/Riwayat/SumaryTradeIn';
import SumaryNewCar from '../screen/Riwayat/SumaryNewCar';
import DetailAparsial from '../screen/Riwayat/DetailAparsial';
import DetailIntensif from '../screen/Insentif/DetailInsentif';
import EditDokumen from '../screen/Profil/EditDokumen';
import TrackingToolTradeIn from '../screen/Tracking/TrackingToolTradeIn';
import TrackingBeliMobil from '../screen/Tracking/TrackingBeliMobil';
import ModalTambahDataCustomer from '../component/modal/Home/ModalTambahDataCustomer';
import BeliMobilComponent from '../component/section/Home/BeliMobilComponent';
import DetailAppraisalTracking from '../screen/Tracking/DetailAppraisalTracking';
import SplashScreen from '../screen/Home/SplashScreen';
const Stack = createStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, tabBarHideOnKeyboard: true}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
        <Stack.Screen name="BottomNavigation" component={MyTabs} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Insentif" component={Insentif} />
        <Stack.Screen name="Profil" component={Profil} />
        <Stack.Screen name="Riwayat" component={Riwayat} />
        <Stack.Screen name="Tracking" component={Tracking} />
        <Stack.Screen name="EditDataDiri" component={EditDataDiri} />
        <Stack.Screen name="EditDataRekening" component={EditDataRekening} />
        <Stack.Screen name="RiwayatComponent" component={RiwayatComponent} />
        <Stack.Screen name="SumaryTradeIn" component={SumaryTradeIn} />
        <Stack.Screen name="SumaryNewCar" component={SumaryNewCar} />
        <Stack.Screen name="DetailAparsial" component={DetailAparsial} />
        <Stack.Screen name="EditDokumen" component={EditDokumen} />
        <Stack.Screen name="BeliMobil" component={BeliMobil} />
        <Stack.Screen name="CekHarga" component={CekHarga} />
        <Stack.Screen name="PO" component={PO} />
        <Stack.Screen name="POValid" component={POValid} />
        <Stack.Screen name="Summary" component={Summary} />
        <Stack.Screen name="ToolTradeIn" component={ToolTradeIn} />
        <Stack.Screen name="TotalAppraisal" component={TotalAppraisal} />
        <Stack.Screen name="DetailInsentif" component={DetailIntensif} />
        <Stack.Screen
          name="TrackingToolTradeIn"
          component={TrackingToolTradeIn}
        />
        <Stack.Screen name="TrackingBeliMobil" component={TrackingBeliMobil} />
        <Stack.Screen
          name="BeliMobilComponent"
          component={BeliMobilComponent}
        />
        <Stack.Screen
          name="ModalTambahDataCustomer"
          component={ModalTambahDataCustomer}
        />
        <Stack.Screen
          name="DetailAppraisalTracking"
          component={DetailAppraisalTracking}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
