import axios from 'axios';
import {BASE_URL, TOKEN} from './url';

const API = async (
  url,
  options = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'PATCH' ||
    request.method === 'DELETE'
  )
    request.data = options.body;

  const res = await axios(request);

  if (res.status === 200) {
    return res.data;
  } else {
    return res;
  }
};

export default {
  getDataBranch: async () => {
    return API(`approval/branch-list`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: TOKEN},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataTradeinHome: async () => {
    return API(`approval/trade-in`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: TOKEN},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataDashboardTotal: async token => {
    return API(`approval/dashboards`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataListCar: async token => {
    return API(`approval/new-car-list`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  GetDataTradeinTracking: async (search, filter, token, idBranch) => {
    return API(
      `approval/trade-in/tracking?branchId=${idBranch}${search}${filter}`,
      {
        method: 'GET',
        head: {'Content-Type': 'application/json', Authorization: token},
      },
    )
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataNewCarTracking: async (search, filter, token, idBranch) => {
    return API(
      `approval/new-car/tracking?branchId=${idBranch}${search}${filter}`,
      {
        method: 'GET',
        head: {'Content-Type': 'application/json', Authorization: token},
      },
    )
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataTradeinRiwayat: async (search, filter, token, idBranch) => {
    return API(
      `approval/trade-in/history?branchId=${idBranch}${search}${filter}`,
      {
        method: 'GET',
        head: {'Content-Type': 'application/json', Authorization: token},
      },
    )
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataNewCarRiwayat: async (search, filter, token, idBranch) => {
    return API(
      `approval/new-car/history?branchId=${idBranch}${search}${filter}`,
      {
        method: 'GET',
        head: {'Content-Type': 'application/json', Authorization: token},
      },
    )
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataTradein: async (search, filter, token, idBranch) => {
    return API(`approval/trade-in?branchId=${idBranch}${search}${filter}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataNewCar: async (search, filter, token, idBranch) => {
    return API(`approval/new-car?branchId=${idBranch}${search}${filter}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataDetailNewCar: async (id, token) => {
    return API(`approval/new-car/${id}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataDetailTradeInCar: async (id, token) => {
    return API(`approval/trade-in/${id}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  PostDataLogin: async params => {
    return API(`user/login`, {
      method: 'POST',
      head: {'Content-Type': 'application/json'},
      body: params,
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataDetailToolTradeIn: async (id, token) => {
    return API(`approval/trade-in/${id}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataDetailBeliMobil: async (id, token) => {
    return API(`approval/new-car/${id}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  getDataProfil: async (token, id, branchid) => {
    return API(`user/${id}?branchId=${branchid}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  updateDataNewCar: async (id, token, params) => {
    return API(`approval/trade-in/new-car/${id}`, {
      method: 'PUT',
      head: {'Content-Type': 'application/json', Authorization: token},
      body: {
        ...params,
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  updateDataNewCar: async (id, token, params) => {
    return API(`approval/trade-in/new-car/${id}`, {
      method: 'PUT',
      head: {'Content-Type': 'application/json', Authorization: token},
      body: {
        ...params,
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  GetDataAppraisal: async (id, token) => {
    return API(`approval/trade-in/appraisal-final/${id}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  GetDataAppraisal2: async (id, token) => {
    return API(`approval/trade-in/${id}`, {
      method: 'GET',
      head: {'Content-Type': 'application/json', Authorization: token},
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  addNewCar: async (params, token, idBranch) => {
    return API(`approval/new-car/?branchId=${idBranch}`, {
      method: 'POST',
      head: {'Content-Type': 'application/json', Authorization: token},
      body: {
        ...params,
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  pathDataProfile: async (token, params) => {
    return API(`user/kacab/profile`, {
      method: 'PATCH',
      head: {'Content-Type': 'multipart/form-data', Authorization: token},
      body: params,
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
};
