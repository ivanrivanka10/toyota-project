import {combineReducers} from 'redux';
import authReducer from './authReducers';
import loginReducers from './authReducers';

const rootReducer = combineReducers({
  login: loginReducers,
});

export default rootReducer;
