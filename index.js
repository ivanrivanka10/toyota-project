// /**
//  * @format
//  */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';
// import Routing from './src/navigation/Routing';

// AppRegistry.registerComponent(appName, () => Routing);

/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Routing from './src/navigation/Routing';
import App from './App';

AppRegistry.registerComponent(appName, () => App);
